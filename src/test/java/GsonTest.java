import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.me.vmware.GroupReader;
import com.me.vmware.config.ConnectionsConfig;
import com.me.vmware.service.builder.DiscoveredTree;
import org.junit.jupiter.api.Test;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.io.File;
import java.io.InputStreamReader;
import java.io.SequenceInputStream;
import java.lang.reflect.Type;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class GsonTest {
    Gson gson = new GsonBuilder()
            .disableHtmlEscaping()
            .setPrettyPrinting()
            .serializeNulls()
            .create();

    @Test
    public void statusTest() {
        Map<String, Boolean> statusMap = new HashMap<>();

        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            statusMap.put(RandomDataUtils.randomString(), random.nextBoolean());
        }

        System.out.println(gson.toJson(statusMap));
    }

    @Test
    public void cnctcfg() {
        ConnectionsConfig connectionsConfig = new ConnectionsConfig(new ArrayList<>(), new ArrayList<>());

        for (int i = 0; i < 3; i++) {
            connectionsConfig.connections.add(new ConnectionsConfig.ConnectionInfo(
                    RandomDataUtils.randomString(),
                    RandomDataUtils.randomString(),
                    RandomDataUtils.randomString(),
                    RandomDataUtils.randomString(),
                    RandomDataUtils.randomString(),
                    RandomDataUtils.randomString()
            ));
        }

        String x = gson.toJson(connectionsConfig);
        System.out.println(x);

        gson.fromJson(x, ConnectionsConfig.class);
    }

    /*@Test
    public void aaaaa() {
        DiscoveredTree discoveredTree = new DiscoveredTree();
        discoveredTree.push("aaaa", new DiscoveredTree.SimpleAbstractEntity("test", "0",null, 1,"test", "1"), Arrays.asList("Datacenter-1", "Cluster-1"));
        discoveredTree.push("aaaa", new DiscoveredTree.SimpleAbstractEntity("test2", "0",null, 1,"test2", "1"), Arrays.asList("Datacenter-2", "Cluster-1"));

        System.out.println(gson.toJson(discoveredTree));
    }*/

    @Test
    public void bb() throws MalformedObjectNameException {
        ObjectName name = new ObjectName("aaa:`TC3Xdkt3`=v1,`f8KJb8BT`=v2,`4EGVvPFm`=v3,`4PAJQE6p`=v4,name=n");
        String str = name.toString().replaceFirst("aaa" + ":", "");


        LinkedHashMap<String, String> collect = new LinkedHashMap<>();

        Arrays.stream(str.split(",")).map(s -> s.split("=")).forEach(s -> collect.put(s[0], s[1]));

        collect.forEach((k, v) -> {
            System.out.println(k + ":" + v);
        });
    }

    @Test
    public void ff() throws MalformedObjectNameException {
        String group = "Datacenter - ha-datacenter|Standalone Host - esxi.|Hosts";
        System.out.println(Arrays.toString(GroupReader.extractPathFromGroup(group, "domain", "name").toArray()));

        System.out.println(GroupReader.toFlat(group));
    }

    @Test
    public void dd() {
        String a = "Hard disk 1 (92000)";
        Matcher m = Pattern.compile("\\((.+)\\)").matcher(a);

        String s = null;
        while (m.find()) {
            s = m.group(1);
        }
        System.out.println(s);


        System.out.println(a.replaceAll("\\((.+)\\)", ""));
    }

    @Test
    public void analyze() {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStreamReader d = new InputStreamReader(classLoader.getResourceAsStream("flat.json"));

        List<DiscoveredTree.SimpleAbstractEntity> entities = gson.fromJson(d, new TypeToken<List<DiscoveredTree.SimpleAbstractEntity>>() {
        }.getType());

        for (DiscoveredTree.SimpleAbstractEntity entity : entities) {
            System.out.println("\n" + "[" + entity.getObjectType() + "]" + entity.getName());
            Deque<String> path = new ArrayDeque<>();
            if (entity.getParentId() != null) {
                StringBuilder edge = new StringBuilder("-");
                DiscoveredTree.SimpleAbstractEntity parent = find(entities, entity);
                while (parent != null) {
                    //names.add(parent.getName());
                    path.addFirst(edge + parent.getName());
                    parent = find(entities, parent);
                    //edge.append("-");
                }
            }
            String v = "-";
            for (String s : path) {
                System.out.println(v + s);
                v = v + "-";
            }
            //System.out.println(entity.getName()+": "+names);
        }


    }

    @Test
    public void asd() {
        BiFunction<Integer, Integer, Double> div = (a, b) -> a * 1.0 / b * 1.0;
        BiFunction<Integer, Integer, Double> div2 = (a, b) -> a * 1.0 / b;
        System.out.println(div.apply(5, 225));
        System.out.println(div2.apply(5, 225));
    }

    private DiscoveredTree.SimpleAbstractEntity find(List<DiscoveredTree.SimpleAbstractEntity> entities, DiscoveredTree.SimpleAbstractEntity entity) {
        return entities.stream().filter(x -> x.getId().equals(entity.getParentId())).findFirst().orElse(null);
    }

/*    @Test
    public void aa() {
        DiscoveredTree.SimpleAbstractEntity entity = new DiscoveredTree.SimpleAbstractEntity("test", "0",null, 1,"test", "<aa>1");
        entity.withConnectionStatus("dd<aaf>");

        String ssss = "1452<connection>";
        System.out.println(ssss.replaceFirst("<.+>", ""));
    }*/
}