package com.me.vmware.service;

import com.me.vmware.VMWareJMX;
import com.me.vmware.service.builder.DataCollectorBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CollectorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CollectorService.class);
    private final List<DataCollector> dataCollectors = new ArrayList<>();

    public synchronized boolean collect() {
        if(dataCollectors.isEmpty()) {
            return false;
        }
        for (DataCollector dataCollector : dataCollectors) {
            dataCollector.start();
        }
        return true;
    }

    public void stopCollect() {
        if(!dataCollectors.isEmpty())
            LOGGER.info("Stopping all collectors");
        else
            LOGGER.info("No collectors to stop");

        for (DataCollector dataCollector : dataCollectors) {
            dataCollector.stop();
        }
    }

    public void hardReload() {
        LOGGER.info("Hard reload ...");

        List<DataCollector> fromConfig = DataCollectorBuilder.buildFromConfig(VMWareJMX.getAppConfig().getConnectionsConfig());

        stopCollect();
        this.dataCollectors.clear();
        this.dataCollectors.addAll(fromConfig);
        collect();
    }

    public boolean addCollector(DataCollector dataCollector) {
        if(dataCollectors.contains(dataCollector))
            return false;

        dataCollector.start();
        dataCollectors.add(dataCollector);

        return true;
    }

    public List<DataCollector> getDataCollectors() {
        return dataCollectors;
    }

    public void softReload() {
        LOGGER.info("Soft reload ...");

        List<DataCollector> fromConfig = DataCollectorBuilder.buildFromConfig(VMWareJMX.getAppConfig().getConnectionsConfig());

        for (DataCollector dataCollector : fromConfig) {
            if (!addCollector(dataCollector)) {
                LOGGER.info("Failed to add connection '{}'. Connection already exists!", dataCollector.getDomainName());
            }
        }

        for (Iterator<DataCollector> iter = dataCollectors.listIterator(); iter.hasNext();) {
            DataCollector dataCollector = iter.next();
            if (!fromConfig.contains(dataCollector)) {
                iter.remove();

                dataCollector.stop();
                LOGGER.info("Connection removed: '{}'", dataCollector.getDomainName());
            }
        }
    }
}
