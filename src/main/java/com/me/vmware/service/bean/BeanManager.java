package com.me.vmware.service.bean;

import com.me.vmware.service.CollectorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.*;
import java.lang.management.ManagementFactory;

public class BeanManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(BeanManager.class);
    final MBeanServer server = ManagementFactory.getPlatformMBeanServer();

    public MBeanServer getServer() {
        return server;
    }

    public void registerBean(Object o, String mBeanType, String mBeanName) {
        try {
            ObjectName objectName = new ObjectName("com.me.vmware.service.bean" + ":type=" + mBeanType + ",name=" + mBeanName);
            server.registerMBean(o, objectName);
        } catch (Exception e) {
            LOGGER.error("Error during MBean" + mBeanName + " registration operation. ", e);
        }
    }

    public void registerBean(Object o, String mBeanName) {
        try {
            ObjectName objectName = new ObjectName(mBeanName);
            server.registerMBean(o, objectName);
        } catch (Exception e) {
            LOGGER.error("Error during MBean" + mBeanName + " registration operation. ", e);
        }
    }
}
