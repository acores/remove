package com.me.vmware.service.bean;

public class DynamicMXBean implements IEmptyMXBean {
    String group;

    public DynamicMXBean(String group) {
        this.group = group;
    }

    @Override
    public String getGroup() {
        return group;
    }

    @Override
    public void setGroup(String group) {
        this.group = group;
    }
}
