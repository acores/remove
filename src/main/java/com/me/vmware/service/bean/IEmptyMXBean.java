package com.me.vmware.service.bean;

public interface IEmptyMXBean {
    String getGroup();
    void setGroup(String group);
}
