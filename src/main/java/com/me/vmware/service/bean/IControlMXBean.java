package com.me.vmware.service.bean;

public interface IControlMXBean {
    /**
     * Читает статус всех подключений и возвращает состояние
     *
     * @return текущий статус подключений в формате json.
     * key - domainName, value - isConnected (состояние подключения)
     * {
     * "c0102a": true,
     * "e7eadd": true,
     * "94a4aa": true,
     * "efaaa0": true,
     * "ad2e48": false,
     * "f8a038": true
     * ...
     * }
     */
    String readStatus();

    /**
     * Выключает приложение System.exit()
     */
    void reload();

    /**
     * @return текущий конфиг в формате json из файла config.json
     */
    String readConfig();

    /**
     * Обновляет конфиг подключений приложения
     *
     * @param config в формате json
     * @return 1 если конфиг обновлен,
     * 2 если не удалось прочесть конфиг
     * 3 если произошла ошибка и ничего не поменялось
     * 4 если не удалось сохранить конфиг в файл
     */
    int updateConfig(String config);

    /**
     * читает данные объектов подключения в json
     *
     * @param domain домен
     * @return строка json или {} если такого домена нет
     */
    String discoveredTree(String domain);

    /**
     * читает данные объектов всех подключений в json
     *
     * @return строка json
     */
    String discoveredTree();

    String flatTree();
}
