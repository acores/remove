package com.me.vmware.service.bean;


import com.me.vmware.Utils;
import com.me.vmware.VMWareJMX;
import com.me.vmware.config.ConnectionsConfig;
import com.me.vmware.config.reader.JsonConfigReader;
import com.me.vmware.service.CollectorService;
import com.me.vmware.service.DataCollector;
import com.me.vmware.service.builder.DataCollectorBuilder;
import com.me.vmware.service.builder.DiscoveredTree;
import com.me.vmware.service.discover.Discoverer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

import static com.me.vmware.AppConfig.APP_SETTINGS_PATH;

public class ControlBean implements IControlMXBean {

    CollectorService collectorService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ControlBean.class);

    public ControlBean(CollectorService collectorService) {
        this.collectorService = collectorService;
    }

    @Override
    public String readStatus() {
        Map<String, Boolean> statusMap = new HashMap<>();

        for (DataCollector dataCollector : this.collectorService.getDataCollectors()) {
            statusMap.put(dataCollector.getDomainName(), dataCollector.getServerConnection().getConnectionUp());
        }

        return VMWareJMX.getAppConfig().getGson().toJson(statusMap);
    }

    @Override
    public void reload() {
        System.exit(0);
    }

    @Override
    public String readConfig() {
        return new JsonConfigReader().toJsonString(VMWareJMX.getAppConfig().getConnectionsConfig());
    }

    @Override
    public int updateConfig(String config) {
        try {
            ConnectionsConfig connectionsConfig = new JsonConfigReader().fromString(config);
            if (connectionsConfig == null)
                return 2;

            boolean needHardReload = connectionsConfig.equalByIntervals(connectionsConfig);

            VMWareJMX.getAppConfig().setConnectionsConfig(connectionsConfig);
            boolean result = JsonConfigReader.saveConfig(
                    APP_SETTINGS_PATH.resolve("config.json").toFile(),
                    connectionsConfig
            );

            if (needHardReload) {
                collectorService.hardReload();
            } else {
                collectorService.softReload();
            }

            if (!result)
                return 4;

        } catch (Exception e) {
            LOGGER.error("Error while update config", e);
            return 3;
        }
        return 1;
    }

    @Override
    public String discoveredTree(String domain) {
        for (DataCollector dataCollector : collectorService.getDataCollectors()) {
            if (dataCollector.getDomainName().equals(domain)) {
                DiscoveredTree tree = dataCollector.readTree();
                return VMWareJMX.getAppConfig().getGson().toJson(tree);
            }
        }

        return "{}";
    }

    @Override
    public String discoveredTree() {
        DiscoveredTree discoveredTree = new DiscoveredTree();
        Deque<Integer> ids = new ArrayDeque<>();

        for (DataCollector dataCollector : collectorService.getDataCollectors()) {
            dataCollector.readTree(discoveredTree, ids);
        }

        return VMWareJMX.getAppConfig().getGson().toJson(discoveredTree);
    }

    @Override
    public String flatTree() {
        DiscoveredTree discoveredTree = new DiscoveredTree();
        Deque<Integer> ids = new ArrayDeque<>();
        for (DataCollector dataCollector : collectorService.getDataCollectors()) {
            dataCollector.readTree(discoveredTree, ids);
        }

        List<DiscoveredTree.SimpleAbstractEntity> src = discoveredTree.toFlat();

        List<Integer> roots = new ArrayList<>();

        //выписывает корни
        src.forEach(x -> {
            if (x.getParentId() == null) {
                roots.add(x.getId());
            }
        });

        //если у сущности ссылка на корень - ставим null т.е. делаем корневой
        for (DiscoveredTree.SimpleAbstractEntity entity : src) {
            if (roots.contains(entity.getParentId())) {
                entity.setParentId(null);
            }
        }

        //удаляет корни
        src.removeIf(x -> roots.contains(x.getId()));

        return VMWareJMX.getAppConfig().getGson().toJson(src);
    }
}
