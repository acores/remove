package com.me.vmware.service.builder;

import com.me.vmware.ServerConnection;
import com.me.vmware.config.ConnectionsConfig;
import com.me.vmware.service.DataCollector;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class DataCollectorBuilder {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataCollectorBuilder.class);

    public static List<DataCollector> buildFromConfig(@NotNull ConnectionsConfig connectionsConfig) {
        List<DataCollector> dataCollectors = new ArrayList<>();

        for (String activeConnection : connectionsConfig.activeConnections) {
            Optional<ConnectionsConfig.ConnectionInfo> infoOptional = connectionsConfig.connections.stream()
                    .filter(x -> x.connection.equals(activeConnection))
                    .findFirst();

            if (!infoOptional.isPresent()) {
                LOGGER.error("connection not found {}", activeConnection);
            }

            infoOptional.ifPresent(connectionInfo -> {
                try {
                    dataCollectors.add(new DataCollector(
                            connectionInfo.domainName,
                            connectionInfo,
                            buildCollectorSettings(connectionsConfig, activeConnection, connectionInfo)
                    ));
                } catch (IllegalStateException e) {
                    LOGGER.error("Connection '{}' not added", connectionInfo.connection);
                }
            });
        }

        return dataCollectors;
    }

    private static DataCollector.DataCollectorSettings buildCollectorSettings(
            ConnectionsConfig connectionsConfig,
            String connection,
            ConnectionsConfig.ConnectionInfo connectionInfo) throws IllegalStateException {
        DataCollector.DataCollectorSettings.ON_ERROR error;
        try {
            error = DataCollector.DataCollectorSettings.ON_ERROR.valueOf(connectionInfo.onError);
        } catch (IllegalArgumentException e) {
            throw new NullPointerException(MessageFormat.format("Failed to recognize 'onError' property for connection '{0}'.\nValue: '{1}'.\nValid values: '{2}'"
                            .replace("'", "''"),
                    connection,
                    connectionInfo.onError,
                    Arrays.toString(DataCollector.DataCollectorSettings.ON_ERROR.values())));
        }

        return new DataCollector.DataCollectorSettings(
                error,
                Duration.ofSeconds(connectionsConfig.clearIntervalSec),
                Duration.ofSeconds(connectionsConfig.updateIntervalSec),
                Duration.ofSeconds(connectionsConfig.updateBeforeExitIntervalSec),
                connectionsConfig.numberRetries
        );
    }
}
