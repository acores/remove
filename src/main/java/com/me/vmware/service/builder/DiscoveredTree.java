package com.me.vmware.service.builder;

import com.me.vmware.AppUtils;
import com.me.vmware.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

public class DiscoveredTree {
    private static final Logger LOGGER = LoggerFactory.getLogger(DiscoveredTree.class);
    public Set<Folder> datacenters = new HashSet<>();
    static Map<String, Integer> types = new HashMap<String, Integer>() {{
        put("Folder", 1);
        put("Datacenter", 2);
        put("Datastore", 3);
        put("Compute Resource", 4);
        put("Host", 5);
        put("Sensor", 6);
        put("Virtual Machine", 7);
        put("Virtual Disk", 8);
        put("Resource Pool", 9);
        put("Cluster Compute Resources", 10);
    }};

    static Map<Class<?>, String> entityTypes = new HashMap<Class<?>, String>() {{
        put(DatacenterEntity.class, "Datacenter");
        put(DatastoreEntity.class, "Datastore");
        put(ComputeResourceEntity.class, "Compute Resource");
        put(HostSystemEntity.class, "Host");
        put(HostSensorEntity.class, "Sensor");
        put(VirtualMachineEntity.class, "Virtual Machine");
        put(VirtualDiskEntity.class, "Virtual Disk");
        put(ResourcePoolEntity.class, "Resource Pool");
        put(ClusterComputeResourceEntity.class, "Cluster Compute Resources");
    }};


    public void push(String datacenter, SimpleAbstractEntity entity, Collection<String> paths, Deque<Integer> ids) {
        if (datacenters.stream().noneMatch(x -> x.name.equals(datacenter)))
            datacenters.add(new Folder(datacenter, AppUtils.generateId(ids)));

        Optional<Folder> folder = datacenters.stream().filter(x -> x.name.equals(datacenter)).findFirst();
        if (!folder.isPresent()) {
            return;
        }

        Folder current = folder.get();
        for (String path : paths) {
            current = current.getFolder(new Folder(path, AppUtils.generateId(ids)));
        }

        /*if (entity.object_type == 5) {
            LOGGER.info("current= " + current.getName());
            LOGGER.info(Arrays.toString(paths.toArray()));
        }*/

        current.withContent(x -> x.add(entity));
    }

    public List<SimpleAbstractEntity> toFlat() {
        List<SimpleAbstractEntity> entities = new ArrayList<>();

        walk(entities);

        return entities;
    }

    public void walk(List<SimpleAbstractEntity> entities) {
        for (Folder domain : datacenters) {
            SimpleAbstractEntity folder = SimpleAbstractEntity.folder(domain.name, domain.id, null, types.get("Folder"));
            entities.add(folder);

            walk(domain, entities);
        }
    }

    public void walk(Folder from, List<SimpleAbstractEntity> entities) {

        //Function<SimpleAbstractEntity, Boolean> contains = entity -> entities.stream().anyMatch(x -> x.id == entity.id);

        //if (from.getChilds().isEmpty())
        for (SimpleAbstractEntity entity : from.getContent()) {
            entity.parent_id = from.id;
            entities.add(entity);
        }


        for (Folder child : from.getChilds()) {
            SimpleAbstractEntity folder = SimpleAbstractEntity.folder(child.name, child.id, from.id, types.get("Folder"));

            entities.add(folder);
            walk(child, entities);
        }
    }

    //depr
    public void walk(BiConsumer<LinkedFolder, Set<SimpleAbstractEntity>> consumer) {
        for (Folder datacenter : datacenters) {
            walk(datacenter, consumer);
        }
    }

    private void walk(Folder from, BiConsumer<LinkedFolder, Set<SimpleAbstractEntity>> consumer) {
        for (Folder child : from.getChilds()) {
            consumer.accept(new LinkedFolder(child, from), child.getContent());
            walk(child, consumer);
        }
    }

    public void simplify(Folder parent, Folder child) {
        if (child.getContent().size() == 1 && child.getChilds().isEmpty()) {
            Set<SimpleAbstractEntity> content = child.getContent();

            parent.withChilds(x -> x.removeIf(f -> f.equals(child)));
            parent.withContent(x -> x.addAll(content));
        }
    }

    public static class Folder {
        String name;
        Set<SimpleAbstractEntity> objects = new HashSet<>();
        Set<Folder> folders = new HashSet<>();
        Integer id;

        public Folder(String name, int id) {
            this.name = name;
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public Folder getFolder(Folder folder) {
            folders.add(folder);

            //noinspection OptionalGetWithoutIsPresent
            return folders.stream().filter(x -> x.equals(folder)).findFirst().get();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Folder folder = (Folder) o;

            return name.equals(folder.name);
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }

        public Folder withContent(Consumer<Set<SimpleAbstractEntity>> content) {
            content.accept(this.objects);
            return this;
        }

        public Set<SimpleAbstractEntity> getContent() {
            return objects;
        }

        public Set<Folder> getChilds() {
            return folders;
        }

        public Folder withChilds(Consumer<Set<Folder>> childs) {
            childs.accept(this.folders);
            return this;
        }

        public Folder with(BiConsumer<Set<Folder>, Set<SimpleAbstractEntity>> childs) {
            childs.accept(this.folders, this.objects);
            return this;
        }
    }

    public static class SimpleAbstractEntity {
        Integer id;
        Integer parent_id;
        String name;
        String alt_name;
        int object_type;
        final String original_id;
        Integer overallStatus;
        Integer powerStatus;
        Integer connectionStatus;


        public SimpleAbstractEntity(String name, int id, String alt_name, int object_type, String original_id,
                                    String overallStatus, Integer parent_id) {
            this.id = id;
            this.name = name;
            this.alt_name = alt_name;
            this.object_type = object_type;
            this.original_id = original_id;
            if (overallStatus != null)
                this.overallStatus = Integer.parseInt(overallStatus.replaceFirst("<.+>", ""));
            this.parent_id = parent_id;
        }

        public Integer getId() {
            return id;
        }

        public Integer getParentId() {
            return parent_id;
        }

        public void setParentId(Integer parentId) {
            this.parent_id = parentId;
        }

        public SimpleAbstractEntity(String name, int id, String alt_name, int object_type, String original_id, String overallStatus, AbstractEntity entity) {
            this.name = name;
            this.id = id;
            this.alt_name = alt_name;
            this.object_type = object_type;
            this.original_id = original_id;
            if (overallStatus != null)
                this.overallStatus = Integer.parseInt(overallStatus.replaceFirst("<.+>", ""));

            String s = entityTypes.get(entity.getClass());
            if (s != null) {
                if (types.get(s) != null)
                    this.object_type = types.get(s);
            }
        }

        public static SimpleAbstractEntity folder(String name, int id, Integer parentId, int objectType) {
            return new SimpleAbstractEntity(name, id, name, objectType, null, null, parentId);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SimpleAbstractEntity that = (SimpleAbstractEntity) o;

            return name.equals(that.name);
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }

        public String getName() {
            return name;
        }

        public int getObjectType() {
            return object_type;
        }

        public void withConnectionStatus(String status) {
            connectionStatus = Integer.parseInt(status.replaceFirst("<.+>", ""));
        }

        public void withPowerStatus(String status) {
            powerStatus = Integer.parseInt(status.replaceFirst("<.+>", ""));
        }
    }

    public static class LinkedFolder {
        Folder current;
        Folder parent;

        public LinkedFolder(Folder current, Folder parent) {
            this.current = current;
            this.parent = parent;
        }

        public Folder getCurrent() {
            return current;
        }

        public Folder getParent() {
            return parent;
        }
    }
}
