package com.me.vmware.service.discover;

import com.me.vmware.entities.*;
import java.util.ArrayList;
import java.util.List;

public class DiscoverEntitiesStorage {
    private List<DatacenterEntity> datacenterEntities = new ArrayList<>();
    private List<ComputeResourceEntity> computeResourceEntities = new ArrayList<>();
    private List<ClusterComputeResourceEntity> clusterComputeResourceEntities = new ArrayList<>();
    private List<DatastoreEntity> datastoreEntities = new ArrayList<>();
    private List<ResourcePoolEntity> resourcePoolEntities = new ArrayList<>();
    private List<HostSystemEntity> hostSystemEntities = new ArrayList<>();
    private List<HostSensorEntity> hostSensorEntities = new ArrayList<>();
    private List<VirtualMachineEntity> virtualMachineEntities = new ArrayList<>();
    private List<VirtualDiskEntity> virtualDiskEntities = new ArrayList<>();
    private List<VirtualAppEntity> virtualAppEntities = new ArrayList<>();


    public List<AbstractEntity> getAllEntities() {
        List<AbstractEntity> allEntities = new ArrayList<>();

        allEntities.addAll(datacenterEntities);
        allEntities.addAll(computeResourceEntities);
        allEntities.addAll(clusterComputeResourceEntities);
        allEntities.addAll(datastoreEntities);
        allEntities.addAll(resourcePoolEntities);
        allEntities.addAll(hostSystemEntities);
        allEntities.addAll(hostSensorEntities);
        allEntities.addAll(virtualMachineEntities);
        allEntities.addAll(virtualDiskEntities);
        allEntities.addAll(virtualAppEntities);

        return allEntities;
    }

    public List<DatacenterEntity> getDatacenterEntities() {
        return datacenterEntities;
    }

    public List<ComputeResourceEntity> getComputeResourceEntities() { return computeResourceEntities; }

    public List<ClusterComputeResourceEntity> getClusterComputeResourceEntities() { return clusterComputeResourceEntities; }

    public List<DatastoreEntity> getDatastoreEntities() {
        return datastoreEntities;
    }

    public List<ResourcePoolEntity> getResourcePoolEntities() { return resourcePoolEntities; }

    public List<HostSystemEntity> getHostSystemEntities() {
        return hostSystemEntities;
    }

    public List<HostSensorEntity> getHostSensorEntities() { return hostSensorEntities; }

    public List<VirtualMachineEntity> getVirtualMachineEntities() { return virtualMachineEntities; }

    public List<VirtualDiskEntity> getVirtualDiskEntities() { return virtualDiskEntities; }

    public List<VirtualAppEntity> getVirtualAppEntities() { return virtualAppEntities; }

    public void clearStorage() {
        datacenterEntities.clear();
        computeResourceEntities.clear();
        clusterComputeResourceEntities.clear();
        datastoreEntities.clear();
        resourcePoolEntities.clear();
        hostSystemEntities.clear();
        hostSensorEntities.clear();
        virtualMachineEntities.clear();
        virtualDiskEntities.clear();
        virtualAppEntities.clear();
    }

}
