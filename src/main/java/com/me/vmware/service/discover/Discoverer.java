package com.me.vmware.service.discover;


import com.me.vmware.ServerConnection;
import com.me.vmware.Utils;
import com.vmware.vim25.*;
import com.me.vmware.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class Discoverer {
    private static final Logger LOGGER = LoggerFactory.getLogger(Discoverer.class);
    private ServerConnection connection;
    private VimPortType methods;
    private ServiceContent serviceContent;
    private ManagedObjectReference rootFolder;
    private ManagedObjectReference viewManager;
    private ManagedObjectReference propColl;
    DiscoverEntitiesStorage discoverEntitiesStorage;

    public Discoverer(ServerConnection connection) {
        this.connection = connection;
        this.methods = this.connection.getMethods();
        this.serviceContent = this.connection.getServiceContent();
        this.rootFolder = this.serviceContent.getRootFolder();
        this.viewManager = this.serviceContent.getViewManager();
        this.propColl = this.serviceContent.getPropertyCollector();

        this.discoverEntitiesStorage = new DiscoverEntitiesStorage();
    }

    public List<ManagedObjectReference> getChilds(String entityType, ManagedObjectReference from) throws RuntimeFaultFaultMsg, InvalidPropertyFaultMsg {
        Map<ManagedObjectReference, String> entriesMap = new HashMap<>();
        entriesMap.put(from, "");

        List<ManagedObjectReference> references = new ArrayList<>();

        for (Map.Entry<ManagedObjectReference, String> entry : entriesMap.entrySet()) {
            ManagedObjectReference e = entry.getKey();

            RetrieveResult props = this.getResults(entityType, Arrays.asList("name"), e, true);

            if (props == null)
                continue;

            references.addAll(props.getObjects().stream().map(ObjectContent::getObj).collect(Collectors.toList()));
        }

        return references;
    }

    public DiscoverEntitiesStorage getDiscoverEntitiesStorage() {
        return discoverEntitiesStorage;
    }

    public void run() {
        if (this.connection.getConnectionUp()) {
            try {
                discoverDatacenters();
                discoverResources();
                discoverDatastores();
                discoverHosts();

                List<AbstractEntity> computeResourceEntities = new ArrayList<>();
                computeResourceEntities.addAll(discoverEntitiesStorage.getComputeResourceEntities());
                computeResourceEntities.addAll(discoverEntitiesStorage.getClusterComputeResourceEntities());
                if (!computeResourceEntities.isEmpty()) {
                    for (AbstractEntity computeResourceEntity : computeResourceEntities) {
                        discoverResourcePools(computeResourceEntity);
                    }
                }

                discoverVirtualMachines();
            } catch (Exception e) {
                System.out.println("Error during discover operation.");
                e.printStackTrace();
            }
        }
    }

    private void discoverDatacenters() throws RuntimeFaultFaultMsg, InvalidPropertyFaultMsg {
        RetrieveResult props = this.getResults("Datacenter",
                Arrays.asList("name"),
                this.rootFolder,
                true);

        if (props != null) {
            for (ObjectContent oc : props.getObjects()) {
                String name;
                String path;
                List<DynamicProperty> dps = oc.getPropSet();
                if (dps != null) {
                    DatacenterEntity entity = new DatacenterEntity();
                    entity.setObjectReference(oc.getObj());
                    entity.setId(oc.getObj().getValue());
                    for (DynamicProperty dp : dps) {
                        path = dp.getName();
                        if (path.equals("name")) {
                            name = (String) dp.getVal();
                            entity.setName(name + " (" + entity.getId() + ")");
                            entity.setGroup("Datacenter - " + name);
                        }
                    }
                    discoverEntitiesStorage.getDatacenterEntities().add(entity);
                }
            }
        }
    }

    private void discoverResources() throws RuntimeFaultFaultMsg, InvalidPropertyFaultMsg {
        List<DatacenterEntity> datacenterEntities = discoverEntitiesStorage.getDatacenterEntities();
        if (!datacenterEntities.isEmpty()) {
            for (DatacenterEntity datacenterEntity : datacenterEntities) {
                RetrieveResult props = this.getResults("ComputeResource",
                        Arrays.asList("name", "resourcePool"),
                        datacenterEntity.getObjectReference(),
                        true);
                if (props != null) {
                    for (ObjectContent oc : props.getObjects()) {
                        String name;
                        String path;
                        ManagedObjectReference rootResourcePool;
                        List<DynamicProperty> dps = oc.getPropSet();
                        if (dps != null) {
                            if (oc.getObj().getType().equals("ClusterComputeResource")) {
                                ClusterComputeResourceEntity entity = new ClusterComputeResourceEntity();
                                entity.setObjectReference(oc.getObj());
                                entity.setId(oc.getObj().getValue());
                                for (DynamicProperty dp : dps) {
                                    path = dp.getName();
                                    if (path.equals("name")) {
                                        name = (String) dp.getVal();
                                        entity.setName(name + " (" + entity.getId() + ")");
                                        //todo here
                                        entity.setGroup(datacenterEntity.getGroup() + "|Cluster - " + name);
                                        //entity.setGroup(datacenterEntity.getGroup());
                                    }
                                    if (path.equals("resourcePool")) {
                                        rootResourcePool = (ManagedObjectReference) dp.getVal();
                                        entity.setRootResourcePool(rootResourcePool);
                                    }
                                }
                                discoverEntitiesStorage.getClusterComputeResourceEntities().add(entity);
                            } else {
                                ComputeResourceEntity entity = new ComputeResourceEntity();
                                entity.setObjectReference(oc.getObj());
                                entity.setId(oc.getObj().getValue());
                                for (DynamicProperty dp : dps) {
                                    path = dp.getName();
                                    if (path.equals("name")) {
                                        name = (String) dp.getVal();
                                        entity.setName(name);
                                        //todo
                                        entity.setGroup(datacenterEntity.getGroup() + "|Standalone Host - " + name);
                                        //entity.setGroup(datacenterEntity.getGroup());
                                    }
                                    if (path.equals("resourcePool")) {
                                        rootResourcePool = (ManagedObjectReference) dp.getVal();
                                        entity.setRootResourcePool(rootResourcePool);
                                    }
                                }
                                discoverEntitiesStorage.getComputeResourceEntities().add(entity);
                            }
                        }
                    }
                }
            }
        }
    }

    private void discoverDatastores() throws RuntimeFaultFaultMsg, InvalidPropertyFaultMsg {
        List<DatacenterEntity> datacenterEntities = discoverEntitiesStorage.getDatacenterEntities();
        if (!datacenterEntities.isEmpty()) {
            for (DatacenterEntity datacenterEntity : datacenterEntities) {
                RetrieveResult props = this.getResults("Datastore",
                        Arrays.asList("name"),
                        datacenterEntity.getObjectReference(),
                        true);
                if (props != null) {
                    for (ObjectContent oc : props.getObjects()) {
                        String name;
                        String path;
                        List<DynamicProperty> dps = oc.getPropSet();
                        if (dps != null) {
                            DatastoreEntity entity = new DatastoreEntity();
                            entity.setObjectReference(oc.getObj());
                            entity.setId(oc.getObj().getValue());
                            for (DynamicProperty dp : dps) {
                                path = dp.getName();
                                if (path.equals("name")) {
                                    name = (String) dp.getVal();
                                    entity.setName(name + " (" + entity.getId() + ")");
                                    entity.setGroup(datacenterEntity.getGroup() + "|Datastores");
                                }
                            }
                            discoverEntitiesStorage.getDatastoreEntities().add(entity);
                        }
                    }
                }
            }
        }
    }

    private void discoverHosts() throws RuntimeFaultFaultMsg, InvalidPropertyFaultMsg {
        List<AbstractEntity> computeResourceEntities = new ArrayList<>();
        computeResourceEntities.addAll(discoverEntitiesStorage.getComputeResourceEntities());
        computeResourceEntities.addAll(discoverEntitiesStorage.getClusterComputeResourceEntities());
        if (!computeResourceEntities.isEmpty()) {
            for (AbstractEntity computeResourceEntity : computeResourceEntities) {
                RetrieveResult props = this.getResults("HostSystem",
                        Arrays.asList("name", "runtime.healthSystemRuntime.systemHealthInfo.numericSensorInfo"),
                        computeResourceEntity.getObjectReference(),
                        true);
                if (props != null) {
                    for (ObjectContent oc : props.getObjects()) {
                        String name;
                        String path;
                        ArrayOfHostNumericSensorInfo hostSensors;
                        List<DynamicProperty> dps = oc.getPropSet();
                        if (dps != null) {
                            HostSystemEntity entity = new HostSystemEntity();
                            entity.setObjectReference(oc.getObj());
                            entity.setId(oc.getObj().getValue());
                            for (DynamicProperty dp : dps) {
                                path = dp.getName();
                                if (path.equals("name")) {
                                    name = (String) dp.getVal();
                                    entity.setName(name);
                                    //todo
                                    //entity.setGroup(computeResourceEntity.getGroup() + "|Hosts|Host - " + name);
                                    entity.setGroup(computeResourceEntity.getGroup() + "|Hosts");
                                }
                            }
                            discoverEntitiesStorage.getHostSystemEntities().add(entity);

                            for (DynamicProperty dp : dps) {
                                path = dp.getName();
                                if (path.equals("runtime.healthSystemRuntime.systemHealthInfo.numericSensorInfo")) {
                                    hostSensors = (ArrayOfHostNumericSensorInfo) dp.getVal();

                                    List<HostNumericSensorInfo> hostNumericSensorInfo = hostSensors.getHostNumericSensorInfo();
                                    //LOGGER.info("discover size: " + entity.getName() + "=" + hostNumericSensorInfo.size());

                                    hostNumericSensorInfo.forEach(e -> {
                                        entity.setGroup(computeResourceEntity.getGroup() + "|Hosts|Host - " + entity.getName());
                                        HostSensorEntity hostSensorEntity = new HostSensorEntity();
                                        String hostSensorId = e.getId();
                                        String hostSensorName = e.getName();
                                        String sensorType = e.getSensorType();

                                        hostSensorEntity.setName(hostSensorName);
                                        hostSensorEntity.setGroup(entity.getGroup() + "|Sensors");
                                        hostSensorEntity.setId(entity.getId());
                                        hostSensorEntity.setAdditionalId(hostSensorId);
                                        hostSensorEntity.setSensorType(sensorType);

                                        //LOGGER.info("sensor: " + hostSensorEntity.getGroup() + ":" + hostSensorName);
                                        discoverEntitiesStorage.getHostSensorEntities().add(hostSensorEntity);
                                    });

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void discoverResourcePools(AbstractEntity startPoint) throws RuntimeFaultFaultMsg, InvalidPropertyFaultMsg {
        RetrieveResult props = this.getResults("ResourcePool",
                Arrays.asList("parent", "resourcePool", "name"),
                startPoint.getObjectReference(),
                false);
        if (props != null) {
            for (ObjectContent oc : props.getObjects()) {
                ManagedObjectReference parent = new ManagedObjectReference();
                ArrayOfManagedObjectReference resourcePool = new ArrayOfManagedObjectReference();
                String name;
                String path;
                List<DynamicProperty> dps = oc.getPropSet();
                if (dps != null) {
                    for (DynamicProperty dp : dps) {
                        path = dp.getName();
                        if (path.equals("parent")) {
                            parent = (ManagedObjectReference) dp.getVal();
                        }
                        if (path.equals("resourcePool")) {
                            resourcePool = (ArrayOfManagedObjectReference) dp.getVal();
                        }
                    }

                    /*if (!parent.getType().equals("ResourcePool") && resourcePool.getManagedObjectReference().size() <= 0) {
                        LOGGER.info("if(1)");
                        for (DynamicProperty dp : dps) {
                            path = dp.getName();
                            if (path.equals("name"))
                                LOGGER.info("if(1)" + dp.getVal());
                        }
                        return;
                    }*/

                    ResourcePoolEntity entity = new ResourcePoolEntity();
                    entity.setObjectReference(oc.getObj());
                    entity.setId(oc.getObj().getValue());
                    if (parent.getType().equals("ResourcePool") || parent.getType().equals("ClusterComputeResource") || parent.getType().equals("ComputeResource")) {
                        for (DynamicProperty dp : dps) {
                            path = dp.getName();
                            if (path.equals("name")) {
                                name = (String) dp.getVal();
                                entity.setName(name + " (" + entity.getId() + ")");
                                //todo
                                entity.setGroup(startPoint.getGroup() + "|ResourcePool - " + name);
                                //entity.setGroup(startPoint.getGroup());
                            }
                        }
                        discoverEntitiesStorage.getResourcePoolEntities().add(entity);
                        if (resourcePool.getManagedObjectReference().size() > 0)
                            this.discoverResourcePools(entity);
                    } else {
                        LOGGER.info("if(2)" + entity.getId());
                        entity.setGroup(startPoint.getGroup());
                        this.discoverResourcePools(entity);
                    }
                }
            }
        }
    }

    private void discoverVirtualMachines() throws RuntimeFaultFaultMsg, InvalidPropertyFaultMsg {
        Map<ManagedObjectReference, String> entriesMap = new HashMap<>();
        discoverEntitiesStorage.getComputeResourceEntities().forEach(e -> entriesMap.put(e.getRootResourcePool(), e.getGroup()));
        discoverEntitiesStorage.getClusterComputeResourceEntities().forEach(e -> entriesMap.put(e.getRootResourcePool(), e.getGroup()));
        discoverEntitiesStorage.getResourcePoolEntities().forEach(e -> entriesMap.put(e.getObjectReference(), e.getGroup()));

        if (!entriesMap.isEmpty()) {
            for (Map.Entry<ManagedObjectReference, String> entry : entriesMap.entrySet()) {
                RetrieveResult props = this.getResults("VirtualMachine",
                        Arrays.asList("name", "config.hardware.device", "guest.ipAddress"),
                        entry.getKey(),
                        false);
                if (props != null) {
                    for (ObjectContent oc : props.getObjects()) {
                        String name;
                        String ip;
                        String path;
                        ArrayOfVirtualDevice virtualDevices;
                        List<DynamicProperty> dps = oc.getPropSet();
                        if (dps != null) {
                            VirtualMachineEntity entity = new VirtualMachineEntity();
                            entity.setObjectReference(oc.getObj());
                            entity.setId(oc.getObj().getValue());
                            for (DynamicProperty dp : dps) {
                                path = dp.getName();
                                if (path.equals("name")) {
                                    name = (String) dp.getVal();
                                    entity.setName(name + " (" + entity.getId() + ")");
                                    //todo
                                    entity.setGroup(entry.getValue() + "|Virtual Machines|Virtual Machine - " + name);
                                    //entity.setGroup(entry.getValue() + "|Virtual Machines");
                                }
                                if (path.equals("guest.ipAddress")) {
                                    ip = (String) dp.getVal();
                                    entity.setIpAddress(" [" + ip + "]");
                                }
                            }
                            discoverEntitiesStorage.getVirtualMachineEntities().add(entity);

                            for (DynamicProperty dp : dps) {
                                path = dp.getName();
                                if (path.equals("config.hardware.device")) {
                                    virtualDevices = (ArrayOfVirtualDevice) dp.getVal();
                                    virtualDevices.getVirtualDevice().forEach(e -> {
                                        if (e instanceof VirtualDisk) {
                                            VirtualDiskEntity virtualDiskEntity = new VirtualDiskEntity();
                                            VirtualDisk virtualDisk = (VirtualDisk) e;
                                            int virtualDiskKey = virtualDisk.getKey();
                                            String virtualDiskName = virtualDisk.getDeviceInfo().getLabel();
                                            VirtualDeviceFileBackingInfo backingInfo = (VirtualDeviceFileBackingInfo) virtualDisk.getBacking();
                                            String diskFileName = backingInfo.getFileName();
                                            String virtualDiskDatastoreName = diskFileName.substring(
                                                    diskFileName.indexOf("[") + 1,
                                                    diskFileName.lastIndexOf("]"));

                                            /*virtualDiskEntity.setName(entity.getName() + " " +
                                                    virtualDiskName +
                                                    " (" + virtualDiskDatastoreName + ")");*/
                                            virtualDiskEntity.setName(virtualDiskName + " (" + entity.getId() + "" + virtualDiskKey + ")");
                                            virtualDiskEntity.setGroup(entity.getGroup() + "|Virtual Disks");
                                            virtualDiskEntity.setId(entity.getId());
                                            virtualDiskEntity.setAdditionalId(Integer.toString(virtualDiskKey));

                                            discoverEntitiesStorage.getVirtualDiskEntities().add(virtualDiskEntity);
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private RetrieveResult getResults(String entityType,
                                      List<String> propsList,
                                      ManagedObjectReference startPoint,
                                      boolean recurse) throws RuntimeFaultFaultMsg, InvalidPropertyFaultMsg {

        ManagedObjectReference cViewRef = this.methods.createContainerView(this.viewManager,
                startPoint,
                Collections.singletonList(entityType),
                recurse);

        ObjectSpec oSpec = new ObjectSpec();
        oSpec.setObj(cViewRef);
        oSpec.setSkip(true);

        TraversalSpec tSpec = new TraversalSpec();
        tSpec.setName("traverseEntities");
        tSpec.setPath("view");
        tSpec.setSkip(false);
        tSpec.setType("ContainerView");

        oSpec.getSelectSet().add(tSpec);

        PropertySpec pSpec = new PropertySpec();
        pSpec.setType(entityType);
        pSpec.getPathSet().addAll(propsList);

        PropertyFilterSpec fSpec = new PropertyFilterSpec();
        fSpec.getObjectSet().add(oSpec);
        fSpec.getPropSet().add(pSpec);

        List<PropertyFilterSpec> fSpecList = new ArrayList<>();
        fSpecList.add(fSpec);

        RetrieveOptions ro = new RetrieveOptions();
        RetrieveResult props = this.methods.retrievePropertiesEx(this.propColl, fSpecList, ro);

        if (props != null) {
            Utils.retrieveAllProperties(this.methods, this.propColl, props, props.getToken());
        }

        return props;
    }
}
