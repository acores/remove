package com.me.vmware.service;

import com.me.vmware.*;
import com.me.vmware.config.ConnectionsConfig;
import com.me.vmware.entities.*;
import com.me.vmware.service.builder.DiscoveredTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.MalformedObjectNameException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataCollector {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataCollector.class);
    private String domainName;
    ConnectionsConfig.ConnectionInfo connectionInfo;
    private ServerConnection serverConnection;
    private Measurer measurer;
    private boolean dataCollected = false;
    private DataCollectorSettings settings;
    private ScheduledFuture<?> collectDataTask;
    private ScheduledFuture<?> watchCollectDataTask;
    private ScheduledExecutorService service = new ScheduledThreadPoolExecutor(2);

    public DataCollector(String domainName, ConnectionsConfig.ConnectionInfo connectionInfo, DataCollectorSettings settings) {
        this.serverConnection = buildServerConnection(connectionInfo);
        this.domainName = domainName;
        this.settings = settings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataCollector that = (DataCollector) o;

        return Objects.equals(connectionInfo, that.connectionInfo);
    }

    public DiscoveredTree readTree(DiscoveredTree discoveredTree, Deque<Integer> ids) {

        for (AbstractEntity e : EntitiesStorage.getAllEntities()) {
            if (!e.getObjectName().getDomain().equals(domainName)) {
                continue;
            }

            /*
             * alt_name – оригинальное имя, получаемое из исходного параметра neme путем отрезания id (который в скобках)
             * original_id – оригинальный ид, полученый из атрибута бина id (так же содержится в скобках в оригинальном имени объекта )
             * name = name
             * */


            Matcher m = Pattern.compile("\\((.+)\\)").matcher(e.getName());

            String originalId = null;
            while (m.find()) {
                originalId = m.group(1);
            }

            DiscoveredTree.SimpleAbstractEntity entity = new DiscoveredTree.SimpleAbstractEntity(
                    e.getName(),
                    AppUtils.generateId(ids),
                    e.getName().replaceAll(" \\((.+)\\)", ""), //alt-name
                    -1,
                    originalId, //original
                    String.valueOf(e.getOverallStatus()),
                    e
            );
            if (e instanceof HostSystemEntity) {
                HostSystemEntity hostSystem = (HostSystemEntity) e;
                entity.withConnectionStatus(String.valueOf(hostSystem.getConnectionStatus()));
                entity.withPowerStatus(String.valueOf(hostSystem.getPowerStatus()));
            }
            if (e instanceof DatastoreEntity) {
                entity.withConnectionStatus(String.valueOf(((DatastoreEntity) e).getConnectionStatus()));
            }
            if (e instanceof VirtualMachineEntity) {
                VirtualMachineEntity machine = (VirtualMachineEntity) e;
                entity.withConnectionStatus(String.valueOf(machine.getConnectionStatus()));
                entity.withPowerStatus(String.valueOf(machine.getPowerStatus()));
            }

            try {
                if(e.getGroup() == null) {
                    LOGGER.info("group is null at DataCollector: " + e.getName());
                    continue;
                }
                List<String> paths = GroupReader.extractPathFromGroup(e, domainName);
                //LOGGER.info("extracted= "+paths);

                discoveredTree.push(domainName, entity, paths, ids);
            } catch (MalformedObjectNameException ex) {
                throw new RuntimeException(ex);
            }
        }

        return discoveredTree;
    }


    public DiscoveredTree readTree() {
        return readTree(new DiscoveredTree(), new ArrayDeque<>());
    }

    private ServerConnection buildServerConnection(ConnectionsConfig.ConnectionInfo x) {
        return new ServerConnection(x.serverIp, x.userName, x.password);
    }

    public boolean start() throws IllegalStateException {
        if (collectDataTask != null) {
            LOGGER.info("Trying to start collector '{}'. This collector is running", domainName);
            return true;
        }

        LOGGER.info("Trying to start Collector '{}'", domainName);


        if (!connectWithRetries(1)) {
            switch (settings.error) {
                case SKIP: {
                    LOGGER.info("Collector '{}' was not started: SKIPPED", domainName);
                    return false;
                }
                case BREAK: {
                    LOGGER.info("Collector '{}' was not started: BREAK", domainName);
                    throw new IllegalStateException();
                }
                case RETRY: {
                    if (!connectWithRetries(settings.numberRetries)) {
                        LOGGER.info("Fail to start collector '{}' with retry", domainName);
                        return false;
                    }

                    LOGGER.info("Collector '{}' started", domainName);
                }
            }
        }

        this.collectDataTask = startCollectDataTask();
        this.watchCollectDataTask = watchDataCollectedFlag();

        return true;
    }

    public void stop() {
        measurer.clearMBeanServer();
        if (collectDataTask != null) {
            collectDataTask.cancel(true);
            collectDataTask = null;
            LOGGER.info("collector '{}' stopped", domainName);
        } else {
            LOGGER.info("collectDataTask for collector '{}' is not running", domainName);
        }

        if (watchCollectDataTask != null) {
            watchCollectDataTask.cancel(true);
            watchCollectDataTask = null;
            LOGGER.info("collector '{}' stopped", domainName);
        } else {
            LOGGER.info("watchCollectDataTask for collector '{}' is not running", domainName);
        }
        serverConnection.disconnect();
    }

    /**
     * Запускает задачу сбора данных
     * Если внутри нет соединения то переподключается и пересоздает measurer
     *
     * @return периодичная задача
     */
    public ScheduledFuture<?> startCollectDataTask() {
        LOGGER.info("Start collect data task for '{}'", domainName);
        return service.scheduleWithFixedDelay(() -> {
            if (!serverConnection.getConnectionUp()) {
                if (!connectWithRetries(settings.numberRetries)) {
                    this.measurer = null;
                }
            }
            if (measurer == null)
                this.measurer = new Measurer(serverConnection, settings.clearInterval.getSeconds());

            collectData();
            LOGGER.info("Waiting {} seconds...", settings.updateInterval.getSeconds());
            System.gc();

        }, 0, settings.updateInterval.toMillis(), TimeUnit.MILLISECONDS);

    }

    /**
     * Данные собираются постоянно
     * Каждые @updateBeforeExitInterval (заданное время в менеджере) проверяем
     * что данные были собраны. Если данные не собраны то падаем
     */
    private ScheduledFuture<?> watchDataCollectedFlag() {
        LOGGER.info("Start watch collect data for '{}'", domainName);
        return service.scheduleWithFixedDelay(() -> {
                    if (!this.dataCollected) {
                        LOGGER.error("Data was not updated for {} seconds. Exiting...",
                                settings.updateBeforeExitInterval.getSeconds());
                        System.exit(1);
                    } else {
                        this.dataCollected = false;
                        LOGGER.info("Data collection check passed. Waiting {} seconds...",
                                settings.updateBeforeExitInterval.getSeconds());
                    }
                }, settings.updateBeforeExitInterval.toMillis(),
                settings.updateBeforeExitInterval.toMillis(),
                TimeUnit.MILLISECONDS);
    }

    /**
     * Собирает данные
     */
    public void collectData() {
        LOGGER.info("Collect data from domain '{}' ...", domainName);
        //собрали данные и поменяли флаг
        measurer.run(domainName);
        dataCollected = true;
        LOGGER.info("End of collect data from domain '{}' ...", domainName);
    }

    /**
     * Попытка подключится заданное количество раз
     *
     * @param numberRetries кол-во попыток
     * @return false если не удалось подключиться иначе true
     */
    private boolean connectWithRetries(int numberRetries) {
        for (int i = 0; i < numberRetries; ++i) {
            try {
                LOGGER.info("Trying to connect to the vCenter/ESX server. Attempt {} / {}", (i + 1), numberRetries);
                this.serverConnection.connect();
            } catch (Exception e) {
                LOGGER.warn("Can't connect to the vCenter/ESX server. ", e);
            }
        }

        if (!this.serverConnection.getConnectionUp()) {
            LOGGER.error("The number of unsuccessful attempts to connect to the vCenter/ESX server has reached the maximum value");
            return false;
        }

        LOGGER.info("Connection to the vCenter/ESX server successful");
        return true;
    }

    public String getDomainName() {
        return domainName;
    }

    public ServerConnection getServerConnection() {
        return serverConnection;
    }

    public static class DataCollectorSettings {
        public ON_ERROR error;
        public Duration clearInterval;
        public Duration updateInterval;
        public Duration updateBeforeExitInterval;
        public int numberRetries;

        public enum ON_ERROR {SKIP, RETRY, BREAK}

        public DataCollectorSettings(ON_ERROR error,
                                     Duration clearInterval,
                                     Duration updateInterval,
                                     Duration updateBeforeExitInterval,
                                     int numberRetries) {
            this.error = error;
            this.clearInterval = clearInterval;
            this.updateInterval = updateInterval;
            this.updateBeforeExitInterval = updateBeforeExitInterval;
            this.numberRetries = numberRetries;
        }
    }
}
