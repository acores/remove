package com.me.vmware.service.io;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import java.io.File;
import java.util.function.Consumer;

public abstract class ConfigListener implements FileAlterationListener {
    @Override
    public void onStart(FileAlterationObserver observer) {
    }

    @Override
    public void onDirectoryCreate(File directory) {
    }

    @Override
    public void onDirectoryChange(File directory) {
    }

    @Override
    public void onDirectoryDelete(File directory) {
        System.out.println("delete：" + directory.getAbsolutePath());
    }

    @Override
    public void onStop(FileAlterationObserver observer) {
    }
}