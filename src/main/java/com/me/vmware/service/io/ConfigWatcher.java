package com.me.vmware.service.io;

import com.me.vmware.VMWareJMX;
import com.me.vmware.config.ConnectionsConfig;
import com.me.vmware.service.CollectorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class ConfigWatcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigWatcher.class);
    FileMonitor fileMonitor;
    CollectorService collectorService;

    public ConfigWatcher(CollectorService collectorService) {
        this.collectorService = collectorService;
    }

    public void watch(File config) {
        LOGGER.info("Start config watcher service: " + config.getAbsolutePath());


        this.fileMonitor = new FileMonitor(1000);
        this.fileMonitor.monitor(config.getParentFile(), new ConfigListener() {
            boolean isConfigFile(File file) {
                return file.getAbsolutePath().equals(config.getAbsolutePath());
            }

            @Override
            public void onFileCreate(File file) {
                if (isConfigFile(file)) {
                    if (file.canRead()) {
                        onConfigFileChanged();
                    }
                }
            }

            @Override
            public void onFileChange(File file) {
                if (isConfigFile(file)) {
                    onConfigFileChanged();
                }
            }

            @Override
            public void onFileDelete(File file) {
                if (isConfigFile(file)) {
                    LOGGER.info("Config file deleted!");
                }
            }
        });

        try {
            fileMonitor.start();
        } catch (Exception e) {
            LOGGER.error("Error", e);
        }
    }

    public void onConfigFileChanged() {
        ConnectionsConfig config = VMWareJMX.readConfig();
        if (config == null)
            return;

        boolean needHardReload = VMWareJMX.getAppConfig().getConnectionsConfig().equalByIntervals(config);

        VMWareJMX.getAppConfig().setConnectionsConfig(config);

        if (needHardReload) {
            collectorService.hardReload();
        } else {
            collectorService.softReload();
        }
    }


}