package com.me.vmware;

import com.me.vmware.config.ConnectionsConfig;
import com.me.vmware.config.reader.JsonConfigReader;
import com.me.vmware.entities.AbstractEntity;
import com.me.vmware.service.CollectorService;
import com.me.vmware.service.DataCollector;
import com.me.vmware.service.bean.BeanManager;
import com.me.vmware.service.bean.ControlBean;
import com.me.vmware.service.builder.DiscoveredTree;
import com.me.vmware.service.discover.Discoverer;
import com.me.vmware.service.io.ConfigWatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.*;

import static com.me.vmware.AppConfig.APP_SETTINGS_PATH;

public class VMWareJMX {
    private static final Logger LOGGER = LoggerFactory.getLogger(VMWareJMX.class);
    static CollectorService collectorService;
    static BeanManager beanManager;
    static ConfigWatcher configWatcher;
    static AppConfig appConfig;
    static ControlBean controlBean;
    static boolean generateReport = false;

    public static AppConfig getAppConfig() {
        if (appConfig == null)
            appConfig = new AppConfig();

        return appConfig;
    }

/*
    public static void createTree() {
        beanManager.registerBean(new DynamicMXBean(""), "foo.com:type=a,name=Resource");

        List<String> strings = new VMWareTest().generateRandomBeanTree();
        System.out.println(strings.get(0));

        QueryExp query = new QueryExp() {
            @Override
            public boolean apply(ObjectName name) {
                return name.getDomain().equals("foo.com");
            }

            @Override
            public void setMBeanServer(MBeanServer s) {
            }
        };

        MBeanServer server = beanManager.getServer();


        for (ObjectName name : server.queryNames(null, query)) {
            System.out.println("before: "+name.getCanonicalName());
            try {
                String[] attributes = Arrays.stream(server.getMBeanInfo(name).getAttributes())
                        .map(MBeanFeatureInfo::getName)
                        .collect(Collectors.toList())
                        .toArray(new String[]{});

                AttributeList oldAttributes = server.getAttributes(name, attributes);
                oldAttributes.add(new Attribute("aaa", "bbbb"));
                
                server.setAttributes(name, new AttributeList(oldAttributes));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (ObjectName queryName : server.queryNames(null, query)) {
            System.out.println("before: "+queryName.getKeyPropertyList());
            queryName.getKeyPropertyList().put("name", "bbbb");
            System.out.println("after: "+queryName.getKeyPropertyList());
        }
    }
*/

    public static void main(String... args) throws Exception {
        LOGGER.info("Welcome");

        collectorService = new CollectorService();
        beanManager = new BeanManager();

        //createTree();

        LOGGER.info("Register beans...");
        controlBean = new ControlBean(collectorService);
        beanManager.registerBean(controlBean, "control", "admin");

        LOGGER.info("Read config...");

        //если конфига нет то создаст его
        if (!getAppConfig().getConfigFile().exists()) {
            LOGGER.info("config.json not found. Create with default at: '{}'...", getAppConfig().getConfigFile().getAbsolutePath());

            try {
                JsonConfigReader.saveConfig(
                        getAppConfig().getConfigFile(),
                        new ConnectionsConfig(new ArrayList<>(), new ArrayList<>())
                );
            } catch (Exception e) {
                LOGGER.error("Unable create default config.json", e);
            }
        }

        ConnectionsConfig config = readConfig();
        if (config != null) {
            LOGGER.info("Read config successfully");

            getAppConfig().setConnectionsConfig(config);
            if (Arrays.binarySearch(args, "--tplg_flat") >= 0) {
                LOGGER.info("topology mode: flat");
                getAppConfig().setFlatBeans(true);
            } else {
                LOGGER.info("topology mode: tree");
            }
            if (Arrays.binarySearch(args, "--report") >= 0) {
                generateReport = true;
            }
            LOGGER.info("generate report: "+ generateReport);

            if (Arrays.binarySearch(args, "--discover") >= 0) {
                LOGGER.info("discover mode: true");
                for (ConnectionsConfig.ConnectionInfo connection : getAppConfig().getConnectionsConfig().connections) {
                    ServerConnection connection1 = new ServerConnection(connection.serverIp, connection.userName, connection.password);
                    connection1.connect();
                    Discoverer discoverer = new Discoverer(connection1);

                    discoverer.run();
                    LOGGER.info("discovered...");
                    for (AbstractEntity entity : discoverer.getDiscoverEntitiesStorage().getAllEntities()) {
                        if (entity.getGroup() == null) {
                            LOGGER.info("null for object name: " + entity.getName());
                        } else {
                            String s = GroupReader.toFlat(entity.getGroup());
                            beanManager.registerBean(entity, connection.domainName + ":" + s + ",name=" + entity.getName());
                        }
                    }
                }
            } else {
                LOGGER.info("discover mode: false");
            }
        }


        configWatcher = new ConfigWatcher(collectorService);
        configWatcher.watch(getAppConfig().getConfigFile());

        collectorService.hardReload();

        readInput();
    }

    public static ConnectionsConfig readConfig() {
        try {
            return new JsonConfigReader().fromStream(AppConfig.readJsonConfig());
        } catch (NoSuchFileException e) {
            LOGGER.warn("config.json file not found");
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.warn("Failed to read config.json");
        }
        return null;
    }

    static void readInput() {
        Scanner input = new Scanner(System.in);
        LOGGER.info("Enter command to continue");
        while (true) {
            String line = input.nextLine();
            switch (line) {
                case "exit": {
                    LOGGER.info("Buy");
                    break;
                }
                case "dst-export": {
                    Deque<Integer> ids = new ArrayDeque<>();

                    String rndName = "dst-" + UUID.randomUUID().toString().substring(0, 6) + ".json";
                    File path = APP_SETTINGS_PATH.resolve(rndName).toFile();

                    DiscoveredTree discoveredTree = new DiscoveredTree();
                    for (DataCollector dataCollector : collectorService.getDataCollectors()) {
                        dataCollector.readTree(discoveredTree, ids);
                    }
                    boolean result = JsonConfigReader.saveJson(
                            path,
                            discoveredTree,
                            true
                    );
                    LOGGER.info("dst-exported {} to: {}", result, path.getPath());

                    StringBuilder nodes = new StringBuilder();
                    discoveredTree.walk((f, c) -> {
                        nodes.append("\nfolder: ");
                        nodes.append(f.getCurrent().getName());
                        c.forEach(e -> nodes.append('\n').append(e.getName()));
                    });
                    File dsf = JsonConfigReader.randomJsonFile("dsf-");
                    try {
                        JsonConfigReader.saveString(dsf, nodes.toString());
                        LOGGER.info("dsf-exported to: {}", dsf.getPath());
                    } catch (IOException e) {
                        LOGGER.error("", e);
                    }

/*                    File dse = JsonConfigReader.randomJsonFile("dse-");
                    discoveredTree.reconnectNodes();
                    JsonConfigReader.saveJson(
                            dse,
                            discoveredTree,
                            true
                    );
                    LOGGER.info("dse-exported to: {}", dse.getPath());*/


/*                    File dsc = JsonConfigReader.randomJsonFile("dsc-");
                    discoveredTree.reconnectNodes();
                    JsonConfigReader.saveString(dsc, nodes.toString());
                    LOGGER.info("dsc-exported to: {}", dsf.getPath());*/


                    for (DataCollector dataCollector : collectorService.getDataCollectors()) {
                        dataCollector.readTree(discoveredTree, ids);
                    }


                    File dse = JsonConfigReader.randomJsonFile("dse-");
                    JsonConfigReader.saveJson(dse, discoveredTree.toFlat(), true);
                    LOGGER.info("dse-exported to: {}", dse.getPath());
                    break;
                }
                case "status": {
                    for (DataCollector collector : collectorService.getDataCollectors()) {
                        LOGGER.info("Domain name: {} ", collector.getDomainName());
                        LOGGER.info("Connected: {}", collector.getServerConnection().getConnectionUp());
                        LOGGER.info("");
                    }
                    break;
                }
                case "cfg-common-save": {
                    boolean result = JsonConfigReader.saveConfig(
                            APP_SETTINGS_PATH.resolve("config.json").toFile(),
                            getAppConfig().getConnectionsConfig()
                    );
                    LOGGER.info("Save config result: {}", result);
                    break;
                }
                case "cfg-file-reload": {
                    collectorService.hardReload();
                    LOGGER.info("Collector service reloaded with config.json");
                    break;
                }
                case "test-1": {
                    controlBean.discoveredTree();
                    break;
                }
                default:
                    LOGGER.info("Unknown command. Use 'exit', 'status', 'cfg-common-save', 'cfg-file-reload'");
            }
        }
    }


}
