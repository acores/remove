package com.me.vmware;

import com.me.vmware.entities.*;
import com.vmware.vim25.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.*;
import java.util.*;

public class Utils {
    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    public static AbstractEntity getEntityById(String id, String additionalId, List<? extends AbstractEntity> entityList) {
        if (!entityList.isEmpty()) {
            for (AbstractEntity e : entityList) {
                if (e.getId().equals(id) && e.getAdditionalId().equals(additionalId)) return e;
            }
        }
        return null;
    }

    public static void retrieveAllProperties(VimPortType methods,
                                             ManagedObjectReference propColl,
                                             RetrieveResult props,
                                             String token) throws InvalidPropertyFaultMsg, RuntimeFaultFaultMsg {
        if (token != null) {
            RetrieveResult newProps = methods.continueRetrievePropertiesEx(propColl, token);
            props.getObjects().addAll(newProps.getObjects());
            retrieveAllProperties(methods, propColl, props, newProps.getToken());
        }
    }

    public static void mBeanRegister(MBeanServer mBeanServer, AbstractEntity entity, String mBeanDomain, String typeName) {
        String mBeanType = typeName;//entity.getId() + entity.getAdditionalId();
        String mBeanName = entity.getName();
        try {
            ObjectName objectName = new ObjectName(mBeanDomain + ":type=" + mBeanType + ",name=" + mBeanName);
            entity.setObjectName(objectName);
            mBeanServer.registerMBean(entity, objectName);
        } catch (MalformedObjectNameException | MBeanRegistrationException | NotCompliantMBeanException e) {
            LOGGER.error("Error during MBean " + entity.getName() + " registration operation. ", e);
        }catch (InstanceAlreadyExistsException e) {
            LOGGER.error("Error during MBean " + entity.getName() + " registration operation. " + e.getMessage());
        }
    }

    public static void mBeanRegisterWithGroup(MBeanServer mBeanServer, AbstractEntity entity, String mBeanDomain) {
        String mBeanName = entity.getName();
        String group = GroupReader.toFlat(entity.getGroup());
/*
        if (entity instanceof HostSystemEntity ) {
            LOGGER.info("host_system_added = " + entity.getName() + ":" + entity.getGroup());
        }
*/
        /*if (entity instanceof HostSensorEntity ) {
            LOGGER.info("mBeanRegisterWithGroup: = " + entity.getName() + ":" + entity.getGroup());
        }*/

        try {
            ObjectName objectName = new ObjectName(mBeanDomain + ":" + group + ",name=" + mBeanName);
            entity.setObjectName(objectName);
            //LOGGER.info("register entity: "+ objectName);
            mBeanServer.registerMBean(entity, objectName);

        } catch (MalformedObjectNameException |
                 MBeanRegistrationException | NotCompliantMBeanException e) {
            LOGGER.error("Error during MBean " + entity.getName() + " registration operation. ", e);
        } catch (InstanceAlreadyExistsException e) {
            LOGGER.error("Error during MBean " + entity.getName() + " registration operation. " + e.getMessage());
        }
    }

    public static void removeOldEntities(MBeanServer mBeanServer, List<? extends AbstractEntity> entities, long clearIntervalSec) {
        Iterator<? extends AbstractEntity> iterator = entities.iterator();
        while (iterator.hasNext()) {
            AbstractEntity entity = iterator.next();
            Date currentDate = new Date();
            Date lastEntityUpdate = entity.getLastDataCollectionTime();
            long intervalWithoutUpdateSec = (currentDate.getTime() - lastEntityUpdate.getTime()) / 1000;

            if (intervalWithoutUpdateSec > clearIntervalSec) {
                try {
                    iterator.remove();
                    mBeanServer.unregisterMBean(entity.getObjectName());
                } catch (InstanceNotFoundException | MBeanRegistrationException e) {
                    LOGGER.error("Error during MBean " + entity.getName() + " unregistration operation. " + e.getMessage());
                }
            }
        }
    }

    public static List<PerfEntityMetricBase> getRealTimeStats(VimPortType methods,
                                                              ManagedObjectReference perfMgr,
                                                              HashMap<String, Integer> countersIdMap,
                                                              Map<String, String> counters,
                                                              String format,
                                                              List<ManagedObjectReference> entities) throws RuntimeFaultFaultMsg {

        List<PerfMetricId> perfMetricIds = new ArrayList<>();
        for (Map.Entry<String, String> entry : counters.entrySet()) {
            String counterName = entry.getKey();
            String counterInstance = entry.getValue();

            PerfMetricId metricId = new PerfMetricId();

            metricId.setCounterId(countersIdMap.get(counterName));
            metricId.setInstance(counterInstance);
            perfMetricIds.add(metricId);
        }

        List<PerfQuerySpec> pqsList = new ArrayList<>();

        for (ManagedObjectReference entity : entities) {
            PerfQuerySpec querySpecification = new PerfQuerySpec();
            querySpecification.setEntity(entity);
            querySpecification.setIntervalId(20);
            querySpecification.setFormat(format);
            querySpecification.setMaxSample(1);
            querySpecification.getMetricId().addAll(perfMetricIds);
            pqsList.add(querySpecification);
        }

        return methods.queryPerf(perfMgr, pqsList);
    }

    public static void fillFSpecList(VimPortType methods,
                                     ManagedObjectReference viewManager,
                                     ManagedObjectReference rootFolder,
                                     List<PropertyFilterSpec> fSpecList,
                                     MetricsSingleton metrics) {
        ManagedObjectReference cViewRef = null;
        try {
            cViewRef = methods.createContainerView(viewManager, rootFolder, metrics.getEntitiesList(), true);
        } catch (RuntimeFaultFaultMsg e) {
            LOGGER.error("Error during filling fSpecList operation. " + e.getMessage());
        }

        ObjectSpec oSpec = new ObjectSpec();
        oSpec.setObj(cViewRef);
        oSpec.setSkip(true);

        TraversalSpec tSpec = new TraversalSpec();
        tSpec.setName("traverseEntities");
        tSpec.setPath("view");
        tSpec.setSkip(false);
        tSpec.setType("ContainerView");

        oSpec.getSelectSet().add(tSpec);

        PropertyFilterSpec fSpec = new PropertyFilterSpec();
        fSpec.getObjectSet().add(oSpec);
        fSpec.getPropSet().add(metrics.getPSpecDC());
        fSpec.getPropSet().add(metrics.getPSpecCR());
        fSpec.getPropSet().add(metrics.getPSpecCCR());
        fSpec.getPropSet().add(metrics.getPSpecDS());
        fSpec.getPropSet().add(metrics.getPSpecHS());
        fSpec.getPropSet().add(metrics.getPSpecRP());
        fSpec.getPropSet().add(metrics.getPSpecVM());
        fSpec.getPropSet().add(metrics.getPSpecVA());

        fSpecList.add(fSpec);
    }

    public static void fillCountersMaps(VimPortType methods,
                                        ManagedObjectReference perfMgr,
                                        ManagedObjectReference propColl,
                                        HashMap<String, Integer> countersIdMap,
                                        HashMap<Integer, PerfCounterInfo> countersInfoMap) {

        ObjectSpec oSpec = new ObjectSpec();
        oSpec.setObj(perfMgr);

        PropertySpec pSpec = new PropertySpec();
        pSpec.setType("PerformanceManager");
        pSpec.getPathSet().add("perfCounter");

        PropertyFilterSpec fSpec = new PropertyFilterSpec();
        fSpec.getObjectSet().add(oSpec);
        fSpec.getPropSet().add(pSpec);

        List<PropertyFilterSpec> fSpecList = new ArrayList<>();
        fSpecList.add(fSpec);

        RetrieveOptions ro = new RetrieveOptions();
        RetrieveResult props = null;
        try {
            props = methods.retrievePropertiesEx(propColl, fSpecList, ro);
        } catch (InvalidPropertyFaultMsg | RuntimeFaultFaultMsg e) {
            LOGGER.error("Error during filling counters maps operation. " + e.getMessage());
        }

        List<PerfCounterInfo> perfCounters = new ArrayList<>();
        if (props != null) {
            for (ObjectContent oc : props.getObjects()) {
                List<DynamicProperty> dps = oc.getPropSet();
                if (dps != null) {
                    for (DynamicProperty dp : dps) {
                        perfCounters = ((ArrayOfPerfCounterInfo) dp.getVal()).getPerfCounterInfo();
                    }
                }
            }
        }
        for (PerfCounterInfo perfCounter : perfCounters) {
            Integer counterId = perfCounter.getKey();

            countersInfoMap.put(counterId, perfCounter);

            String counterGroup = perfCounter.getGroupInfo().getKey();
            String counterName = perfCounter.getNameInfo().getKey();
            String counterRollupType = perfCounter.getRollupType().toString();
            String fullCounterName = counterGroup + "." + counterName + "." + counterRollupType;

            countersIdMap.put(fullCounterName, counterId);
        }
    }

    public static List<String> getAvailablePerfMetricList(VimPortType methods,
                                                          ManagedObjectReference perfMgr,
                                                          Map<String, Integer> countersIdMap,
                                                          ManagedObjectReference objectReference,
                                                          int intervalSec) {
        List<String> result = new ArrayList<>();

        try {
            methods.queryAvailablePerfMetric(perfMgr,
                    objectReference,
                    null,
                    null,
                    intervalSec).forEach(e -> {
                countersIdMap.forEach((k, v) -> {
                    if (e.getCounterId() == v) result.add(k);
                });
            });
        } catch (RuntimeFaultFaultMsg runtimeFaultFaultMsg) {
            System.out.println("Error querying available perf metrics.");
            runtimeFaultFaultMsg.printStackTrace();
        }

        return result;
    }

    public static class RedAbstractEntity {
        AbstractEntity abstractEntity;
        String typename;

        public RedAbstractEntity(AbstractEntity abstractEntity, String typename) {
            this.abstractEntity = abstractEntity;
            this.typename = typename;
        }

        public AbstractEntity getAbstractEntity() {
            return abstractEntity;
        }

        public String getTypename() {
            return typename;
        }


    }
}
