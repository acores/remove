package com.me.vmware.interfaces;

public interface VirtualDiskEntityMXBean {
    public String getId();
    public String getAdditionalId();
    public long getCapacityMB();
    public long getTotalReadLatencyMS();
    public long getTotalWriteLatencyMS();
    public long getNumberReadAveraged();
    public long getNumberWriteAveraged();
    public long getVirtualDiskIops();
    public long getReadRateBps();
    public long getWriteRateBps();
}
