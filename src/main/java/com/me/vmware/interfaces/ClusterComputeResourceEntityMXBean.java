package com.me.vmware.interfaces;

public interface ClusterComputeResourceEntityMXBean {
    public String getId();
    public int getOverallStatus();
    public short getNumCpuCores();
    public int getNumEffectiveHosts();
    public int getNumHosts();
    public int getNumVM();
    public int getEffectiveCpuMHz();
    public int getTotalCpuMHz();
    public long getEffectiveMemoryMB();
    public long getTotalMemoryMB();
    public long getOverallCpuUsageMHz();
    public long getGuestMemoryUsageMB();
    public double getCpuReadyMaxPercent();
    public double getCpuReadyAvgPercent();
    public double getOverallCpuUsageByEffectivePercent();
    public double getOverallCpuUsageByTotalPercent();
    public double getGuestMemoryUsageByEffectivePercent();
    public double getGuestMemoryUsageByTotalPercent();

    // TODO: 15.04.2023
    /**
     * Количество включенных ВМ
     */
    int getTotalEnabledVMs();

    /**
     * Количество выключенных ВМ
     */
    int getTotalDisabledVMs();
}
