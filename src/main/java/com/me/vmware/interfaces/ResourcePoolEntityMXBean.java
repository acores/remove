package com.me.vmware.interfaces;

public interface ResourcePoolEntityMXBean {
    public String getId();
    public int getOverallStatus();
    public String getParentResourcePoolId();
    public String getParentResourcePoolName();
    public String getComputeResourceId();
    public String getComputeResourceName();

    public String getText();
    public void setText(String text);

    public int getNumVM();
    public long getCpuAllocationLimitMHz();
    public long getCpuAllocationReservationMHz();
    public long getMemoryAllocationLimitMB();
    public long getMemoryAllocationReservationMB();
    public long getOverallCpuUsageMHz();
    public long getGuestMemoryUsageMB();
    public double getCpuReadyMaxPercent();
    public double getCpuReadyAvgPercent();
    public double getOverallCpuUsageByLimitPercent();
    public double getOverallCpuUsageByReservationPercent();
    public double getGuestMemoryUsageByLimitPercent();
    public double getGuestMemoryUsageByReservationPercent();

    // TODO: 15.04.2023  
    /**
     * Количество включенных ВМ
     */
    int getTotalEnabledVMs();

    /**
     * Количество выключенных ВМ
     */
    int getTotalDisabledVMs();
}
