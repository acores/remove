package com.me.vmware.interfaces;

public interface DatacenterEntityMXBean {
    String getId();
    int getOverallStatus();

    int getNumVM();

    long getTotalMemorySizeMB();

    int getNumCpuCores();

    long getCpuAllocationLimitMHz();

    long getMemoryAllocationLimitMB();

    int getTotalEnabledVMs();

    int getTotalDisabledVMs();

    int getTotalVMAllocationLimitMB();

    int getDatastoresCount();

    int getSensorsCount();

    int getHostsCount();

    int getPoolCount();

    int getClustersCount();

    int getDisksCount();

    int getTotalDiskCapacityMB();

    int getTotalDatastoreCapacityMB();

    int getTotalDatastoreFreeSpaceMB();

    long getOverallCpuUsageMHz();

    long getCpuAllocationReservationMHz();

    int getGuestMemoryUsageMB();

    int getMemoryAllocationReservationMB();
}
