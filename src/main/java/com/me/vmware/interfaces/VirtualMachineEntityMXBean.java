package com.me.vmware.interfaces;

public interface VirtualMachineEntityMXBean {
    public String getId();
    public int getOverallStatus();
    public int getConnectionStatus();
    public int getPowerStatus();
    public String getResourcePoolId();
    public String getResourcePoolName();
    public int getNumCpuCores();
    public int getMemorySizeMB();
    public int getBalloonedMemoryMB();
    public int getSwappedMemoryMB();
    public double getCpuUsagePercent();
    public double getCpuReadyPercent();
    public double getMemoryUsagePercent();
    public double getMemoryUsageAbsoluteMB();
    public double getMemoryConsumedMB();
    public double getMemoryOverheadMB();
    public double getMemoryPrivateMB();
    public long getNetworkReceivedKbps();
    public long getNetworkTransmittedKbps();
    public double getBalloonedMemoryPercent();
    public double getSwappedMemoryPercent();
    int getDisksCount();
}
