package com.me.vmware.interfaces;

public interface HostSystemEntityMXBean {
    public String getId();
    public int getOverallStatus();
    public int getConnectionStatus();
    public int getPowerStatus();
    public short getNumCpuCores();
    public long getMemorySizeMB();
    public double getCpuUsagePercent();
    public double getMemoryUsagePercent();
    public double getMemoryUsageAbsoluteMB();
    public long getNetworkReceivedKbps();
    public long getNetworkTransmittedKbps();
    int getSensorsCount();
}
