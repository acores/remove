package com.me.vmware.interfaces;

public interface HostSensorEntityMXBean {
    public String getId();
    public String getAdditionalId();
    public int getHealthState();
}
