package com.me.vmware.interfaces;

public interface DatastoreEntityMXBean {
    public String getId();
    public int getOverallStatus();
    public int getConnectionStatus();
    public long getCapacityMB();
    public long getFreeSpaceMB();
    public long getSpaceUsageMB();
    public double getSpaceUsagePercent();
    public long getDatastoreIopsMax();
    public long getDatastoreIopsAvg();
    public long getTotalReadLatencyMaxMS();
    public long getTotalReadLatencyAvgMS();
    public long getTotalWriteLatencyMaxMS();
    public long getTotalWriteLatencyAvgMS();
    public long getNumberReadMax();
    public long getNumberReadAvg();
    public long getNumberWriteMax();
    public long getNumberWriteAvg();
    public long getReadRateMaxBps();
    public long getReadRateAvgBps();
    public long getWriteRateMaxBps();
    public long getWriteRateAvgBps();
}
