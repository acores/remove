package com.me.vmware;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.me.vmware.config.ConnectionsConfig;
import com.me.vmware.config.reader.JsonConfigReader;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AppConfig {
   public static final Path APP_SETTINGS_PATH = Paths.get(System.getProperty("user.dir"));
   @NotNull final Gson gson;
   @NotNull final Gson prettyGson;
   private ConnectionsConfig connectionsConfig;
   private boolean flatBeans = false;

   public AppConfig() {
      GsonBuilder gsonBuilder = new GsonBuilder()
              .disableHtmlEscaping()
              .serializeNulls();

              /*.registerTypeAdapter(
                      new TypeToken<List<String>>() {}.getType(),
                      (JsonDeserializer<List<String>>) (jsonElement, type, jsonDeserializationContext) -> null
              );*/

      this.gson = gsonBuilder.create();
      this.prettyGson = gsonBuilder.setPrettyPrinting().create();
   }

    public boolean isFlatBeans() {
        return flatBeans;
    }

   public void setFlatBeans(boolean flatBeans) {
      this.flatBeans = flatBeans;
   }

   public ConnectionsConfig getConnectionsConfig() {
      return connectionsConfig;
   }

   public void setConnectionsConfig(ConnectionsConfig connectionsConfig) {
      this.connectionsConfig = connectionsConfig;
   }

   public @NotNull Gson getGson() {
      return gson;
   }

   public @NotNull Gson getPrettyGson() {
      return prettyGson;
   }
   @NotNull
   public static InputStream readJsonConfig() throws IOException {
      return Files.newInputStream(APP_SETTINGS_PATH.resolve("config.json"));
   }

   @NotNull
   @Deprecated
   public static InputStream readPropertyConfig() throws IOException {
      return Files.newInputStream(APP_SETTINGS_PATH.resolve("config.properties"));
   }

   public File getConfigFile() {
      return APP_SETTINGS_PATH.resolve("config.json").toFile();
   }

}
