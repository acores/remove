package com.me.vmware.entities;

import com.vmware.vim25.ManagedObjectReference;
import com.me.vmware.interfaces.ResourcePoolEntityMXBean;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ResourcePoolEntity extends AbstractEntity implements ResourcePoolEntityMXBean {
    private ManagedObjectReference parentResourcePool = null;
    private ManagedObjectReference computeResource = null;
    private List<VirtualMachineEntity> mappedVMs = new ArrayList<>();
    private List<ResourcePoolEntity> childResourcePools = new ArrayList<>();
    private String parentResourcePoolId = "";
    private String parentResourcePoolName = "";
    private String computeResourceId = "";
    private String computeResourceName = "";

    private String text = "";

    private int numVMs = 0;
    private long cpuAllocationLimitMHz = 0;
    private long cpuAllocationReservationMHz = 0;
    private long memoryAllocationLimitMB = 0;
    private long memoryAllocationReservationMB = 0;
    private long overallCpuUsageMHz = 0;
    private long guestMemoryUsageMB = 0;
    private double cpuReadyMaxPercent = 0.0;
    private double cpuReadyAvgPercent = 0.0;
    private double overallCpuUsageByLimitPercent = 0.0;
    private double overallCpuUsageByReservationPercent = 0.0;
    private double guestMemoryUsageByLimitPercent = 0.0;
    private double guestMemoryUsageByReservationPercent = 0.0;

    int totalEnabledVMs = 0;
    int totalDisabledVMs = 0;

    public void fillMeasureData() {

        if (!this.getChildResourcePools().isEmpty()) {
            List<Integer> allNumVMs = new ArrayList<>();
            List<Double> allCpuReadyMaxPercent = new ArrayList<>();
            List<Double> allCpuReadyAvgPercent = new ArrayList<>();

            allNumVMs.add(this.getNumVM());
            allCpuReadyMaxPercent.add(this.getCpuReadyMaxPercent());
            allCpuReadyAvgPercent.add(this.getCpuReadyAvgPercent());

            List<ResourcePoolEntity> allChildResourcePools = new ArrayList<>();
            this.fillAllChildResourcePools(this, allChildResourcePools);
            allChildResourcePools.forEach((e) -> {
                allNumVMs.add(e.getNumVM());
                allCpuReadyMaxPercent.add(e.getCpuReadyMaxPercent());
                allCpuReadyAvgPercent.add(e.getCpuReadyAvgPercent());
            });

            this.setNumVMs(allNumVMs.stream().mapToInt(e -> e).sum());
            this.setCpuReadyMaxPercent(allCpuReadyMaxPercent.stream().reduce(Double::max).get());
            this.setCpuReadyAvgPercent(allCpuReadyAvgPercent.stream().collect(Collectors.averagingDouble((e) -> e)));
        }
        if (this.getCpuAllocationLimitMHz() > 0) {
            this.setOverallCpuUsageByLimitPercent(((double) this.getOverallCpuUsageMHz() / (double) this.getCpuAllocationLimitMHz()) * 100);
        }
        if (this.getCpuAllocationReservationMHz() > 0) {
            this.setOverallCpuUsageByReservationPercent(((double) this.getOverallCpuUsageMHz() / (double) this.getCpuAllocationReservationMHz()) * 100);
        }
        if (this.getMemoryAllocationLimitMB() > 0) {
            this.setGuestMemoryUsageByLimitPercent(((double) this.getGuestMemoryUsageMB() / (double) this.getMemoryAllocationLimitMB()) * 100);
        }
        if (this.getMemoryAllocationReservationMB() > 0) {
            this.setGuestMemoryUsageByReservationPercent(((double) this.getGuestMemoryUsageMB() / (double) this.getMemoryAllocationReservationMB()) * 100);
        }

    }

    private void fillAllChildResourcePools(ResourcePoolEntity currentResourcePool, List<ResourcePoolEntity> resultList) {
        if (!currentResourcePool.getChildResourcePools().isEmpty()) {
            resultList.addAll(currentResourcePool.getChildResourcePools());
            currentResourcePool.getChildResourcePools().forEach((e) -> this.fillAllChildResourcePools(e, resultList));
        }
    }

    public String getParentResourcePoolId() { return parentResourcePoolId; }

    public void setParentResourcePoolId(String parentResourcePoolId) { this.parentResourcePoolId = parentResourcePoolId; }

    public ManagedObjectReference getComputeResource() { return computeResource; }

    public void setComputeResource(ManagedObjectReference computeResource) { this.computeResource = computeResource; }

    public String getParentResourcePoolName() { return parentResourcePoolName; }

    public void setParentResourcePoolName(String parentResourcePoolName) { this.parentResourcePoolName = parentResourcePoolName; }

    public String getComputeResourceId() { return computeResourceId; }

    public void setComputeResourceId(String computeResourceId) { this.computeResourceId = computeResourceId; }

    public String getComputeResourceName() { return computeResourceName; }

    public void setComputeResourceName(String computeResourceName) { this.computeResourceName = computeResourceName; }



    public String getText() { return text; }
    public void setText(String text) { this.text = text; }


    public long getCpuAllocationLimitMHz() { return cpuAllocationLimitMHz; }

    public void setCpuAllocationLimitMHz(long cpuAllocationLimitMHz) { this.cpuAllocationLimitMHz = cpuAllocationLimitMHz; }

    public long getCpuAllocationReservationMHz() { return cpuAllocationReservationMHz; }

    public void setCpuAllocationReservationMHz(long cpuAllocationReservationMHz) { this.cpuAllocationReservationMHz = cpuAllocationReservationMHz; }

    public long getMemoryAllocationLimitMB() { return memoryAllocationLimitMB; }

    public void setMemoryAllocationLimitMB(long memoryAllocationLimitMB) { this.memoryAllocationLimitMB = memoryAllocationLimitMB; }

    public long getMemoryAllocationReservationMB() { return memoryAllocationReservationMB; }

    public void setMemoryAllocationReservationMB(long memoryAllocationReservationMB) { this.memoryAllocationReservationMB = memoryAllocationReservationMB; }

    public long getOverallCpuUsageMHz() { return overallCpuUsageMHz; }

    public void setOverallCpuUsageMHz(long overallCpuUsageMHz) { this.overallCpuUsageMHz = overallCpuUsageMHz; }

    public long getGuestMemoryUsageMB() { return guestMemoryUsageMB; }

    public void setGuestMemoryUsageMB(long guestMemoryUsageMB) { this.guestMemoryUsageMB = guestMemoryUsageMB; }

    public List<VirtualMachineEntity> getMappedVMs() { return mappedVMs; }

    public ManagedObjectReference getParentResourcePool() { return parentResourcePool; }

    public void setParentResourcePool(ManagedObjectReference parentResourcePool) { this.parentResourcePool = parentResourcePool; }

    public List<ResourcePoolEntity> getChildResourcePools() { return childResourcePools; }

    public int getNumVM() { return numVMs; }

    public void setNumVMs(int numVMs) { this.numVMs = numVMs; }

    public double getCpuReadyMaxPercent() { return cpuReadyMaxPercent; }

    public void setCpuReadyMaxPercent(double cpuReadyMaxPercent) { this.cpuReadyMaxPercent = cpuReadyMaxPercent; }

    public double getCpuReadyAvgPercent() { return cpuReadyAvgPercent; }

    public void setCpuReadyAvgPercent(double cpuReadyAvgPercent) { this.cpuReadyAvgPercent = cpuReadyAvgPercent; }

    public double getOverallCpuUsageByLimitPercent() { return overallCpuUsageByLimitPercent; }

    public void setOverallCpuUsageByLimitPercent(double overallCpuUsageByLimitPercent) { this.overallCpuUsageByLimitPercent = overallCpuUsageByLimitPercent; }

    public double getOverallCpuUsageByReservationPercent() { return overallCpuUsageByReservationPercent; }

    public void setOverallCpuUsageByReservationPercent(double overallCpuUsageByReservationPercent) { this.overallCpuUsageByReservationPercent = overallCpuUsageByReservationPercent; }

    public double getGuestMemoryUsageByLimitPercent() { return guestMemoryUsageByLimitPercent; }

    public void setGuestMemoryUsageByLimitPercent(double guestMemoryUsageByLimitPercent) { this.guestMemoryUsageByLimitPercent = guestMemoryUsageByLimitPercent; }

    public double getGuestMemoryUsageByReservationPercent() { return guestMemoryUsageByReservationPercent; }

    @Override
    public int getTotalEnabledVMs() {
        return totalEnabledVMs;
    }

    @Override
    public int getTotalDisabledVMs() {
        return totalDisabledVMs;
    }

    public void setTotalEnabledVMs(int totalEnabledVMs) {
        this.totalEnabledVMs = totalEnabledVMs;
    }

    public void setTotalDisabledVMs(int totalDisabledVMs) {
        this.totalDisabledVMs = totalDisabledVMs;
    }

    public void setGuestMemoryUsageByReservationPercent(double guestMemoryUsageByReservationPercent) { this.guestMemoryUsageByReservationPercent = guestMemoryUsageByReservationPercent; }
}
