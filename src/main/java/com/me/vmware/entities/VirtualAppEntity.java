package com.me.vmware.entities;

import com.vmware.vim25.ManagedObjectReference;

import java.util.ArrayList;
import java.util.List;

public class VirtualAppEntity extends AbstractEntity {
    private ManagedObjectReference parent = null;
    private ManagedObjectReference ownerResourcePool = null;
    private String ownerResourcePoolId = "";
    private boolean isRoot = false;
    private List<VirtualAppEntity> childVirtualApps = new ArrayList<>();

    public ManagedObjectReference getParent() { return parent; }

    public void setParent(ManagedObjectReference parent) { this.parent = parent; }

    public ManagedObjectReference getOwnerResourcePool() { return ownerResourcePool; }

    public void setOwnerResourcePool(ManagedObjectReference ownerResourcePool) { this.ownerResourcePool = ownerResourcePool; }

    public String getOwnerResourcePoolId() { return ownerResourcePoolId; }

    public void setOwnerResourcePoolId(String ownerResourcePoolId) { this.ownerResourcePoolId = ownerResourcePoolId; }

    public boolean isRoot() { return isRoot; }

    public void setRoot(boolean root) { isRoot = root; }

    public List<VirtualAppEntity> getChildVirtualApps() { return childVirtualApps; }
}
