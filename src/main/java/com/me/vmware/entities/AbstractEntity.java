package com.me.vmware.entities;

import com.vmware.vim25.ManagedObjectReference;

import javax.management.ObjectName;
import java.util.Date;

public abstract class AbstractEntity {
    private Date lastDataCollectionTime = new Date(0);
    private ObjectName objectName = null;
    private String id = "";
    private String additionalId = "";
    private String name = "";
    private ManagedObjectReference objectReference = null;
    private int overallStatus = -1;

    private String group;
    private String ipAddress;

    public Date getLastDataCollectionTime() { return lastDataCollectionTime; }

    public void setLastDataCollectionTime(Date lastDataCollectionTime) { this.lastDataCollectionTime = lastDataCollectionTime; }

    public ObjectName getObjectName() { return objectName; }

    public void setObjectName(ObjectName objectName) { this.objectName = objectName; }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdditionalId() {
        return additionalId;
    }

    public void setAdditionalId(String additionalId) {
        this.additionalId = additionalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ManagedObjectReference getObjectReference() { return objectReference; }

    public void setObjectReference(ManagedObjectReference objectReference) {
        this.objectReference = objectReference;
    }

    public int getOverallStatus() {
        return overallStatus;
    }


    public void setOverallStatus(String overallStatus) {
        this.overallStatus = Integer.parseInt(overallStatus);
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroup() {
        return group;
    }

    public void setIpAddress(String ip) {
        this.ipAddress = ip;
    }

    public String getIpAddress() {
        return ipAddress;
    }
}
