package com.me.vmware.entities;

import com.me.vmware.interfaces.HostSensorEntityMXBean;

public class HostSensorEntity extends AbstractEntity implements HostSensorEntityMXBean {
    private HostSystemEntity mappedHost = null;
    private String sensorType = "";
    private String healthState = "-1";

    public HostSystemEntity getMappedHost() { return mappedHost; }

    public void setMappedHost(HostSystemEntity mappedHost) { this.mappedHost = mappedHost; }

    public String getSensorType() { return sensorType; }

    public void setSensorType(String sensorType) { this.sensorType = sensorType; }

    public int getHealthState() { return Integer.parseInt(healthState); }

    public void setHealthState(String healthState) { this.healthState = healthState; }
}
