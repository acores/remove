package com.me.vmware.entities;

import com.vmware.vim25.ManagedObjectReference;
import com.me.vmware.interfaces.DatastoreEntityMXBean;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DatastoreEntity extends AbstractEntity implements DatastoreEntityMXBean {
    private String url = "";
    private List<ManagedObjectReference> mappedHosts = new ArrayList<>();
    private List<ManagedObjectReference> mappedVMs = new ArrayList<>();
    private String connectionStatus = "-1";
    private long capacityMB = 0;
    private long freeSpaceMB = 0;
    private List<Long> datastoreIops = new ArrayList<>();
    private List<Long> totalReadLatencyMS = new ArrayList<>();
    private List<Long> totalWriteLatencyMS = new ArrayList<>();
    private List<Long> numberRead = new ArrayList<>();
    private List<Long> numberWrite = new ArrayList<>();
    private List<Long> readRateBps = new ArrayList<>();
    private List<Long> writeRateBps = new ArrayList<>();
    private long spaceUsageMB = 0;
    private double spaceUsagePercent = 0.0;
    private long datastoreIopsMax = 0;
    private long datastoreIopsAvg = 0;
    private long totalReadLatencyMaxMS = 0;
    private long totalReadLatencyAvgMS = 0;
    private long totalWriteLatencyMaxMS = 0;
    private long totalWriteLatencyAvgMS = 0;
    private long numberReadMax = 0;
    private long numberReadAvg = 0;
    private long numberWriteMax = 0;
    private long numberWriteAvg = 0;
    private long readRateMaxBps = 0;
    private long readRateAvgBps = 0;
    private long writeRateMaxBps = 0;
    private long writeRateAvgBps = 0;

    public void fillMeasureData() {

        if (this.getCapacityMB() > 0) {
            this.setSpaceUsageMB(this.getCapacityMB() - this.getFreeSpaceMB());
            this.setSpaceUsagePercent(((double) this.getSpaceUsageMB() / (double) this.getCapacityMB()) * 100);
        }
        if (!this.getDatastoreIops().isEmpty()) {
            this.setDatastoreIopsMax(this.getDatastoreIops().stream().reduce(Long::max).get());
            this.setDatastoreIopsAvg((long) Math.ceil(this.getDatastoreIops().stream().collect(Collectors.averagingLong((e) -> e))));
        }
        if (!this.getTotalReadLatencyMS().isEmpty()) {
            this.setTotalReadLatencyMaxMS(this.getTotalReadLatencyMS().stream().reduce(Long::max).get());
            this.setTotalReadLatencyAvgMS((long) Math.ceil(this.getTotalReadLatencyMS().stream().collect(Collectors.averagingLong((e) -> e))));
        }
        if (!this.getTotalWriteLatencyMS().isEmpty()) {
            this.setTotalWriteLatencyMaxMS(this.getTotalWriteLatencyMS().stream().reduce(Long::max).get());
            this.setTotalWriteLatencyAvgMS((long) Math.ceil(this.getTotalWriteLatencyMS().stream().collect(Collectors.averagingLong((e) -> e))));
        }
        if (!this.getNumberRead().isEmpty()) {
            this.setNumberReadMax(this.getNumberRead().stream().reduce(Long::max).get());
            this.setNumberReadAvg((long) Math.ceil(this.getNumberRead().stream().collect(Collectors.averagingLong((e) -> e))));
        }
        if (!this.getNumberWrite().isEmpty()) {
            this.setNumberWriteMax(this.getNumberWrite().stream().reduce(Long::max).get());
            this.setNumberWriteAvg((long) Math.ceil(this.getNumberWrite().stream().collect(Collectors.averagingLong((e) -> e))));
        }
        if (!this.getReadRateBps().isEmpty()) {
            this.setReadRateMaxBps(this.getReadRateBps().stream().reduce(Long::max).get());
            this.setReadRateAvgBps((long) Math.ceil(this.getReadRateBps().stream().collect(Collectors.averagingLong((e) -> e))));
        }
        if (!this.getWriteRateBps().isEmpty()) {
            this.setWriteRateMaxBps(this.getWriteRateBps().stream().reduce(Long::max).get());
            this.setWriteRateAvgBps((long) Math.ceil(this.getWriteRateBps().stream().collect(Collectors.averagingLong((e) -> e))));
        }

    }

    public String getUrl() { return url; }

    public void setUrl(String url) { this.url = url; }

    public int getConnectionStatus() { return Integer.parseInt(connectionStatus); }

    public void setConnectionStatus(String connectionStatus) { this.connectionStatus = connectionStatus; }

    public long getCapacityMB() { return capacityMB; }

    public void setCapacityMB(long capacityMB) { this.capacityMB = capacityMB; }

    public long getFreeSpaceMB() { return freeSpaceMB; }

    public void setFreeSpaceMB(long freeSpaceMB) { this.freeSpaceMB = freeSpaceMB; }

    public List<ManagedObjectReference> getMappedHosts() { return mappedHosts; }

    public List<ManagedObjectReference> getMappedVMs() { return mappedVMs; }

    public List<Long> getDatastoreIops() { return datastoreIops; }

    public List<Long> getTotalReadLatencyMS() { return totalReadLatencyMS; }

    public List<Long> getTotalWriteLatencyMS() { return totalWriteLatencyMS; }

    public List<Long> getNumberRead() { return numberRead; }

    public List<Long> getNumberWrite() { return numberWrite; }

    public List<Long> getReadRateBps() { return readRateBps; }

    public List<Long> getWriteRateBps() { return writeRateBps; }

    public long getSpaceUsageMB() { return spaceUsageMB; }

    public void setSpaceUsageMB(long spaceUsageMB) { this.spaceUsageMB = spaceUsageMB; }

    public double getSpaceUsagePercent() { return spaceUsagePercent; }

    public void setSpaceUsagePercent(double spaceUsagePercent) { this.spaceUsagePercent = spaceUsagePercent; }

    public long getDatastoreIopsMax() { return datastoreIopsMax; }

    public void setDatastoreIopsMax(long datastoreIopsMax) { this.datastoreIopsMax = datastoreIopsMax; }

    public long getDatastoreIopsAvg() { return datastoreIopsAvg; }

    public void setDatastoreIopsAvg(long datastoreIopsAvg) { this.datastoreIopsAvg = datastoreIopsAvg; }

    public long getTotalReadLatencyMaxMS() { return totalReadLatencyMaxMS; }

    public void setTotalReadLatencyMaxMS(long totalReadLatencyMaxMS) { this.totalReadLatencyMaxMS = totalReadLatencyMaxMS; }

    public long getTotalReadLatencyAvgMS() { return totalReadLatencyAvgMS; }

    public void setTotalReadLatencyAvgMS(long totalReadLatencyAvgMS) { this.totalReadLatencyAvgMS = totalReadLatencyAvgMS; }

    public long getTotalWriteLatencyMaxMS() { return totalWriteLatencyMaxMS; }

    public void setTotalWriteLatencyMaxMS(long totalWriteLatencyMaxMS) { this.totalWriteLatencyMaxMS = totalWriteLatencyMaxMS; }

    public long getTotalWriteLatencyAvgMS() { return totalWriteLatencyAvgMS; }

    public void setTotalWriteLatencyAvgMS(long totalWriteLatencyAvgMS) { this.totalWriteLatencyAvgMS = totalWriteLatencyAvgMS; }

    public long getNumberReadMax() { return numberReadMax; }

    public void setNumberReadMax(long numberReadMax) { this.numberReadMax = numberReadMax; }

    public long getNumberReadAvg() { return numberReadAvg; }

    public void setNumberReadAvg(long numberReadAvg) { this.numberReadAvg = numberReadAvg; }

    public long getNumberWriteMax() { return numberWriteMax; }

    public void setNumberWriteMax(long numberWriteMax) { this.numberWriteMax = numberWriteMax; }

    public long getNumberWriteAvg() { return numberWriteAvg; }

    public void setNumberWriteAvg(long numberWriteAvg) { this.numberWriteAvg = numberWriteAvg; }

    public long getReadRateMaxBps() { return readRateMaxBps; }

    public void setReadRateMaxBps(long readRateMaxBps) { this.readRateMaxBps = readRateMaxBps; }

    public long getReadRateAvgBps() { return readRateAvgBps; }

    public void setReadRateAvgBps(long readRateAvgBps) { this.readRateAvgBps = readRateAvgBps; }

    public long getWriteRateMaxBps() { return writeRateMaxBps; }

    public void setWriteRateMaxBps(long writeRateMaxBps) { this.writeRateMaxBps = writeRateMaxBps; }

    public long getWriteRateAvgBps() { return writeRateAvgBps; }

    public void setWriteRateAvgBps(long writeRateAvgBps) { this.writeRateAvgBps = writeRateAvgBps; }
}
