package com.me.vmware.entities;

import com.me.vmware.interfaces.DatacenterEntityMXBean;

public class DatacenterEntity extends AbstractEntity implements DatacenterEntityMXBean {
    int totalVms;
    long totalMemorySizeMB;
    int totalCpuCores;
    long cpuAllocationLimitMHz;
    long memoryAllocationLimitMB;
    int totalEnabledVMs;
    int totalDisabledVMs;
    int totalVMAllocationLimitMB;
    int totalDatastores;
    int totalSensors;
    int totalHosts;
    int totalPools;
    int totalClusters;
    int totalDisks;
    int totalDiskCapacityMB;
    int totalDatastoreCapacityMB;
    int totalDatastoreFreeSpaceMB;
    long overallCpuUsageMHz;
    long cpuAllocationReservationMHz;
    int guestMemoryUsageMB;
    int memoryAllocationReservationMB;






    // TODO: 15.04.2023


    @Override
    public int getNumVM() {
        return totalVms;
    }

    @Override
    public long getTotalMemorySizeMB() {
        return totalMemorySizeMB;
    }

    @Override
    public int getNumCpuCores() {
        return totalCpuCores;
    }

    @Override
    public long getCpuAllocationLimitMHz() {
        return cpuAllocationLimitMHz;
    }

    @Override
    public long getMemoryAllocationLimitMB() {
        return memoryAllocationLimitMB;
    }

    @Override
    public int getTotalEnabledVMs() {
        return totalEnabledVMs;
    }

    @Override
    public int getTotalDisabledVMs() {
        return totalDisabledVMs;
    }

    @Override
    public int getTotalVMAllocationLimitMB() {
        return totalVMAllocationLimitMB;
    }

    @Override
    public int getDatastoresCount() {
        return totalDatastores;
    }

    @Override
    public int getSensorsCount() {
        return totalSensors;
    }

    @Override
    public int getHostsCount() {
        return totalHosts;
    }
    @Override
    public int getPoolCount() {
        return totalPools;
    }
    @Override
    public int getClustersCount() {
        return totalClusters;
    }
    @Override
    public int getDisksCount() {
        return totalDisks;
    }

    public void setTotalVms(int totalVms) {
        this.totalVms = totalVms;
    }

    public void setTotalMemorySizeMB(long totalMemorySizeMB) {
        this.totalMemorySizeMB = totalMemorySizeMB;
    }

    public void setTotalCpuCores(int totalCpuCores) {
        this.totalCpuCores = totalCpuCores;
    }

    public void setCpuAllocationLimitMHz(long cpuAllocationLimitMHz) {
        this.cpuAllocationLimitMHz = cpuAllocationLimitMHz;
    }

    public void setMemoryAllocationLimitMB(long memoryAllocationLimitMB) {
        this.memoryAllocationLimitMB = memoryAllocationLimitMB;
    }

    public void setTotalEnabledVMs(int totalEnabledVMs) {
        this.totalEnabledVMs = totalEnabledVMs;
    }

    public void setTotalDisabledVMs(int totalDisabledVMs) {
        this.totalDisabledVMs = totalDisabledVMs;
    }

    public void setTotalVMAllocationLimitMB(int totalVMAllocationLimitMB) {
        this.totalVMAllocationLimitMB = totalVMAllocationLimitMB;
    }

    public void setTotalDatastores(int totalDatastores) {
        this.totalDatastores = totalDatastores;
    }

    public void setTotalSensors(int totalSensors) {
        this.totalSensors = totalSensors;
    }

    public void setTotalHosts(int totalHosts) {
        this.totalHosts = totalHosts;
    }

    public void setTotalPools(int totalPools) {
        this.totalPools = totalPools;
    }

    public void setTotalClusters(int totalClusters) {
        this.totalClusters = totalClusters;
    }

    public void setTotalDisks(int totalDisks) {
        this.totalDisks = totalDisks;
    }

    public void setTotalDiskCapacityMB(int totalDiskCapacityMB) {
        this.totalDiskCapacityMB = totalDiskCapacityMB;
    }

    public void setTotalDatastoreCapacityMB(int totalDatastoreCapacityMB) {
        this.totalDatastoreCapacityMB = totalDatastoreCapacityMB;
    }

    public void setTotalDatastoreFreeSpaceMB(int totalDatastoreFreeSpaceMB) {
        this.totalDatastoreFreeSpaceMB = totalDatastoreFreeSpaceMB;
    }

    @Override
    public int getTotalDiskCapacityMB() {
        return totalDiskCapacityMB;
    }

    @Override
    public int getTotalDatastoreCapacityMB() {
        return totalDatastoreCapacityMB;
    }

    @Override
    public int getTotalDatastoreFreeSpaceMB() {
        return totalDatastoreFreeSpaceMB;
    }

    @Override
    public long getOverallCpuUsageMHz() {
        return overallCpuUsageMHz;
    }
    public void setOverallCpuUsageMHz(long overallCpuUsageMHz) {
        this.overallCpuUsageMHz = overallCpuUsageMHz;
    }

    @Override
    public long getCpuAllocationReservationMHz() {
        return cpuAllocationReservationMHz;
    }

    @Override
    public int getGuestMemoryUsageMB() {
        return guestMemoryUsageMB;
    }

    @Override
    public int getMemoryAllocationReservationMB() {
        return memoryAllocationReservationMB;
    }

    public void setCpuAllocationReservationMHz(long cpuAllocationReservationMHz) {
        this.cpuAllocationReservationMHz = cpuAllocationReservationMHz;
    }

    public void setGuestMemoryUsageMB(int guestMemoryUsageMB) {
        this.guestMemoryUsageMB = guestMemoryUsageMB;
    }

    public void setMemoryAllocationReservationMB(int memoryAllocationReservationMB) {
        this.memoryAllocationReservationMB = memoryAllocationReservationMB;
    }
}
