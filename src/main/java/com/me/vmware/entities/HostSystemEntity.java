package com.me.vmware.entities;

import com.me.vmware.interfaces.HostSystemEntityMXBean;

import java.util.ArrayList;
import java.util.List;

public class HostSystemEntity extends AbstractEntity implements HostSystemEntityMXBean {
    private List<HostSensorEntity> mappedHostSensors = new ArrayList<>();
    private int connectionStatus = -1;
    private int powerStatus = -1;
    private short numCpu = 0;
    private long memorySizeMB = 0;
    private double cpuUsagePercent = 0.0;
    private double memoryUsagePercent = 0.0;
    private double memoryUsageAbsoluteMB = 0.0;
    private long networkReceivedKbps = 0;
    private long networkTransmittedKbps = 0;

    public double getCpuUsagePercent() {
        return cpuUsagePercent;
    }

    public void setCpuUsagePercent(double cpuUsagePercent) {
        this.cpuUsagePercent = cpuUsagePercent;
    }

    public double getMemoryUsagePercent() {
        return memoryUsagePercent;
    }

    public void setMemoryUsagePercent(double memoryUsagePercent) {
        this.memoryUsagePercent = memoryUsagePercent;
    }

    public int getConnectionStatus() { return connectionStatus; }

    public void setConnectionStatus(String connectionStatus) { this.connectionStatus = Integer.parseInt(connectionStatus); }

    public int getPowerStatus() { return powerStatus; }

    public void setPowerStatus(String powerStatus) { this.powerStatus = Integer.parseInt(powerStatus); }

    public short getNumCpuCores() { return numCpu; }

    public void setNumCpu(short numCpu) { this.numCpu = numCpu; }

    public long getMemorySizeMB() { return memorySizeMB; }

    public void setMemorySizeMB(long memorySizeMB) { this.memorySizeMB = memorySizeMB; }

    public double getMemoryUsageAbsoluteMB() { return memoryUsageAbsoluteMB; }

    public void setMemoryUsageAbsoluteMB(double memoryUsageAbsoluteMB) { this.memoryUsageAbsoluteMB = memoryUsageAbsoluteMB; }

    public long getNetworkReceivedKbps() { return networkReceivedKbps; }

    public void setNetworkReceivedKbps(long networkReceivedKbps) { this.networkReceivedKbps = networkReceivedKbps; }

    public long getNetworkTransmittedKbps() { return networkTransmittedKbps; }

    @Override
    public int getSensorsCount() {
        return getMappedHostSensors().size();
    }

    public void setNetworkTransmittedKbps(long networkTransmittedKbps) { this.networkTransmittedKbps = networkTransmittedKbps; }

    public List<HostSensorEntity> getMappedHostSensors() { return mappedHostSensors; }
}
