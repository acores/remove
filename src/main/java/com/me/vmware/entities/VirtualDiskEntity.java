package com.me.vmware.entities;

import com.me.vmware.interfaces.VirtualDiskEntityMXBean;

public class VirtualDiskEntity extends AbstractEntity implements VirtualDiskEntityMXBean {
    private VirtualMachineEntity mappedVM = null;
    private String instance = "";
    private long capacityMB = 0;
    private long totalReadLatencyMS = 0;
    private long totalWriteLatencyMS = 0;
    private long numberReadAveraged = 0;
    private long numberWriteAveraged = 0;
    private long virtualDiskIops = 0;
    private long readRateBps = 0;
    private long writeRateBps = 0;

    public void fillMeasureData() {

        this.setVirtualDiskIops(this.numberReadAveraged + this.numberWriteAveraged);

    }

    public VirtualMachineEntity getMappedVM() { return mappedVM; }

    public void setMappedVM(VirtualMachineEntity mappedVM) { this.mappedVM = mappedVM; }

    public String getInstance() { return instance; }

    public void setInstance(String instance) { this.instance = instance; }

    public long getCapacityMB() { return capacityMB; }

    public void setCapacityMB(long capacityMB) { this.capacityMB = capacityMB; }

    public long getTotalReadLatencyMS() { return totalReadLatencyMS; }

    public void setTotalReadLatencyMS(long totalReadLatencyMS) { this.totalReadLatencyMS = totalReadLatencyMS; }

    public long getTotalWriteLatencyMS() { return totalWriteLatencyMS; }

    public void setTotalWriteLatencyMS(long totalWriteLatencyMS) { this.totalWriteLatencyMS = totalWriteLatencyMS; }

    public long getNumberReadAveraged() { return numberReadAveraged; }

    public void setNumberReadAveraged(long numberReadAveraged) { this.numberReadAveraged = numberReadAveraged; }

    public long getNumberWriteAveraged() { return numberWriteAveraged; }

    public void setNumberWriteAveraged(long numberWriteAveraged) { this.numberWriteAveraged = numberWriteAveraged; }

    public long getVirtualDiskIops() { return virtualDiskIops; }

    public void setVirtualDiskIops(long virtualDiskIops) { this.virtualDiskIops = virtualDiskIops; }

    public long getReadRateBps() { return readRateBps; }

    public void setReadRateBps(long readRateBps) { this.readRateBps = readRateBps; }

    public long getWriteRateBps() { return writeRateBps; }

    public void setWriteRateBps(long writeRateBps) { this.writeRateBps = writeRateBps; }
}
