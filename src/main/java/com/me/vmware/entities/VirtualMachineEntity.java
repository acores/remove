package com.me.vmware.entities;

import com.vmware.vim25.ManagedObjectReference;
import com.me.vmware.interfaces.VirtualMachineEntityMXBean;

import java.util.ArrayList;
import java.util.List;

public class VirtualMachineEntity extends AbstractEntity implements VirtualMachineEntityMXBean {
    private ManagedObjectReference resourcePool = null;
    private List<VirtualDiskEntity> mappedVirtualDisks = new ArrayList<>();
    private int connectionStatus = -1;
    private int powerStatus = -1;
    private String resourcePoolId = "";
    private String resourcePoolName = "";
    private int numCpu = 0;
    private int memorySizeMB = 0;
    private int balloonedMemoryMB = 0;
    private int swappedMemoryMB = 0;
    private double cpuUsagePercent = 0.0;
    private double cpuReadyPercent = 0.0;
    private double memoryUsagePercent = 0.0;
    private double memoryUsageAbsoluteMB = 0.0;
    private double memoryConsumedMB = 0.0;
    private double memoryOverheadMB = 0.0;
    private double memoryPrivateMB = 0.0;
    private long networkReceivedKbps = 0;
    private long networkTransmittedKbps = 0;
    private double balloonedMemoryPercent = 0.0;
    private double swappedMemoryPercent = 0.0;

    public void fillMeasureData() {

        if (this.getMemorySizeMB() > 0) {
            this.setBalloonedMemoryPercent(((double) this.getBalloonedMemoryMB() / (double) this.getMemorySizeMB()) * 100);
            this.setSwappedMemoryPercent(((double) this.getSwappedMemoryMB() / (double) this.getMemorySizeMB()) * 100);
        }

        this.setMemoryPrivateMB(this.getMemoryConsumedMB() - this.getMemoryOverheadMB());

    }

    public double getCpuUsagePercent() {
        return cpuUsagePercent;
    }

    public void setCpuUsagePercent(double cpuUsagePercent) {
        this.cpuUsagePercent = cpuUsagePercent;
    }

    public double getMemoryUsagePercent() {
        return memoryUsagePercent;
    }

    public void setMemoryUsagePercent(double memoryUsagePercent) {
        this.memoryUsagePercent = memoryUsagePercent;
    }

    public int getConnectionStatus() { return connectionStatus; }

    public void setConnectionStatus(String connectionStatus) { this.connectionStatus = Integer.parseInt(connectionStatus); }

    public int getPowerStatus() { return powerStatus; }

    public void setPowerStatus(String powerStatus) { this.powerStatus = Integer.parseInt(powerStatus); }

    public String getResourcePoolId() { return resourcePoolId; }

    public void setResourcePoolId(String resourcePoolId) { this.resourcePoolId = resourcePoolId; }

    public String getResourcePoolName() { return resourcePoolName; }

    public void setResourcePoolName(String resourcePoolName) { this.resourcePoolName = resourcePoolName; }

    public int getNumCpuCores() { return numCpu; }

    public void setNumCpu(int numCpu) { this.numCpu = numCpu; }

    public int getMemorySizeMB() { return memorySizeMB; }

    public void setMemorySizeMB(int memorySizeMB) { this.memorySizeMB = memorySizeMB; }

    public double getMemoryUsageAbsoluteMB() { return memoryUsageAbsoluteMB; }

    public void setMemoryUsageAbsoluteMB(double memoryUsageAbsoluteMB) { this.memoryUsageAbsoluteMB = memoryUsageAbsoluteMB; }

    public double getMemoryConsumedMB() { return memoryConsumedMB; }

    public void setMemoryConsumedMB(double memoryConsumedMB) { this.memoryConsumedMB = memoryConsumedMB; }

    public double getMemoryOverheadMB() { return memoryOverheadMB; }

    public void setMemoryOverheadMB(double memoryOverheadMB) { this.memoryOverheadMB = memoryOverheadMB; }

    public double getMemoryPrivateMB() { return memoryPrivateMB; }

    public void setMemoryPrivateMB(double memoryPrivateMB) { this.memoryPrivateMB = memoryPrivateMB; }

    public double getCpuReadyPercent() { return cpuReadyPercent; }

    public void setCpuReadyPercent(double cpuReadyPercent) { this.cpuReadyPercent = cpuReadyPercent; }

    public int getBalloonedMemoryMB() { return balloonedMemoryMB; }

    public void setBalloonedMemoryMB(int balloonedMemoryMB) { this.balloonedMemoryMB = balloonedMemoryMB; }

    public int getSwappedMemoryMB() { return swappedMemoryMB; }

    public void setSwappedMemoryMB(int swappedMemoryMB) { this.swappedMemoryMB = swappedMemoryMB; }

    public ManagedObjectReference getResourcePool() { return resourcePool; }

    public void setResourcePool(ManagedObjectReference resourcePool) { this.resourcePool = resourcePool; }

    public long getNetworkReceivedKbps() { return networkReceivedKbps; }

    public void setNetworkReceivedKbps(long networkReceivedKbps) { this.networkReceivedKbps = networkReceivedKbps; }

    public long getNetworkTransmittedKbps() { return networkTransmittedKbps; }

    public void setNetworkTransmittedKbps(long networkTransmittedKbps) { this.networkTransmittedKbps = networkTransmittedKbps; }

    public List<VirtualDiskEntity> getMappedVirtualDisks() { return mappedVirtualDisks; }

    public double getBalloonedMemoryPercent() { return balloonedMemoryPercent; }

    public void setBalloonedMemoryPercent(double balloonedMemoryPercent) { this.balloonedMemoryPercent = balloonedMemoryPercent; }

    public double getSwappedMemoryPercent() { return swappedMemoryPercent; }

    @Override
    public int getDisksCount() {
        return mappedVirtualDisks.size();
    }

    public void setSwappedMemoryPercent(double swappedMemoryPercent) { this.swappedMemoryPercent = swappedMemoryPercent; }
}
