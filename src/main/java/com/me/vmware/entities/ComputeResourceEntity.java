package com.me.vmware.entities;

import com.vmware.vim25.ManagedObjectReference;
import com.me.vmware.interfaces.ComputeResourceEntityMXBean;

public class ComputeResourceEntity extends AbstractEntity implements ComputeResourceEntityMXBean {
    private ManagedObjectReference rootResourcePool = null;
    private short numCpuCores = 0;
    private int numEffectiveHosts = 0;
    private int numHosts = 0;
    private int numVM = 0;
    private int effectiveCpuMHz = 0;
    private int totalCpuMHz = 0;
    private long effectiveMemoryMB = 0;
    private long totalMemoryMB = 0;
    private long overallCpuUsageMHz = 0;
    private long guestMemoryUsageMB = 0;
    private double cpuReadyMaxPercent = 0.0;
    private double cpuReadyAvgPercent = 0.0;
    private double overallCpuUsageByEffectivePercent = 0.0;
    private double overallCpuUsageByTotalPercent = 0.0;
    private double guestMemoryUsageByEffectivePercent = 0.0;
    private double guestMemoryUsageByTotalPercent = 0.0;

    public void fillMeasureData() {

        if (this.getEffectiveCpuMHz() > 0) {
            this.setOverallCpuUsageByEffectivePercent(((double) this.getOverallCpuUsageMHz() / (double) this.getEffectiveCpuMHz()) * 100);
        }
        if (this.getTotalCpuMHz() > 0) {
            this.setOverallCpuUsageByTotalPercent(((double) this.getOverallCpuUsageMHz() / (double) this.getTotalCpuMHz()) * 100);
        }
        if (this.getEffectiveMemoryMB() > 0) {
            this.setGuestMemoryUsageByEffectivePercent(((double) this.getGuestMemoryUsageMB() / (double) this.getEffectiveMemoryMB()) * 100);
        }
        if (this.getTotalMemoryMB() > 0) {
            this.setGuestMemoryUsageByTotalPercent(((double) this.getGuestMemoryUsageMB() / (double) this.getTotalMemoryMB()) * 100);
        }

    }

    public ManagedObjectReference getRootResourcePool() { return rootResourcePool; }

    public void setRootResourcePool(ManagedObjectReference rootResourcePool) { this.rootResourcePool = rootResourcePool; }

    public short getNumCpuCores() { return numCpuCores; }

    public void setNumCpuCores(short numCpuCores) { this.numCpuCores = numCpuCores; }

    public int getNumEffectiveHosts() { return numEffectiveHosts; }

    public void setNumEffectiveHosts(int numEffectiveHosts) { this.numEffectiveHosts = numEffectiveHosts; }

    public int getNumHosts() { return numHosts; }

    public void setNumHosts(int numHosts) { this.numHosts = numHosts; }

    public int getNumVM() { return numVM; }

    public void setNumVM(int numVM) { this.numVM = numVM; }

    public int getEffectiveCpuMHz() { return effectiveCpuMHz; }

    public void setEffectiveCpuMHz(int effectiveCpuMHz) { this.effectiveCpuMHz = effectiveCpuMHz; }

    public int getTotalCpuMHz() { return totalCpuMHz; }

    public void setTotalCpuMHz(int totalCpuMHz) { this.totalCpuMHz = totalCpuMHz; }

    public long getEffectiveMemoryMB() { return effectiveMemoryMB; }

    public void setEffectiveMemoryMB(long effectiveMemoryMB) { this.effectiveMemoryMB = effectiveMemoryMB; }

    public long getTotalMemoryMB() { return totalMemoryMB; }

    public void setTotalMemoryMB(long totalMemoryMB) { this.totalMemoryMB = totalMemoryMB; }

    public long getOverallCpuUsageMHz() { return overallCpuUsageMHz; }

    public void setOverallCpuUsageMHz(long overallCpuUsageMHz) { this.overallCpuUsageMHz = overallCpuUsageMHz; }

    public long getGuestMemoryUsageMB() { return guestMemoryUsageMB; }

    public void setGuestMemoryUsageMB(long guestMemoryUsageMB) { this.guestMemoryUsageMB = guestMemoryUsageMB; }

    public double getCpuReadyMaxPercent() { return cpuReadyMaxPercent; }

    public void setCpuReadyMaxPercent(double cpuReadyMaxPercent) { this.cpuReadyMaxPercent = cpuReadyMaxPercent; }

    public double getCpuReadyAvgPercent() { return cpuReadyAvgPercent; }

    public void setCpuReadyAvgPercent(double cpuReadyAvgPercent) { this.cpuReadyAvgPercent = cpuReadyAvgPercent; }

    public double getOverallCpuUsageByEffectivePercent() { return overallCpuUsageByEffectivePercent; }

    public void setOverallCpuUsageByEffectivePercent(double overallCpuUsageByEffectivePercent) { this.overallCpuUsageByEffectivePercent = overallCpuUsageByEffectivePercent; }

    public double getOverallCpuUsageByTotalPercent() { return overallCpuUsageByTotalPercent; }

    public void setOverallCpuUsageByTotalPercent(double overallCpuUsageByTotalPercent) { this.overallCpuUsageByTotalPercent = overallCpuUsageByTotalPercent; }

    public double getGuestMemoryUsageByEffectivePercent() { return guestMemoryUsageByEffectivePercent; }

    public void setGuestMemoryUsageByEffectivePercent(double guestMemoryUsageByEffectivePercent) { this.guestMemoryUsageByEffectivePercent = guestMemoryUsageByEffectivePercent; }

    public double getGuestMemoryUsageByTotalPercent() { return guestMemoryUsageByTotalPercent; }

    public void setGuestMemoryUsageByTotalPercent(double guestMemoryUsageByTotalPercent) { this.guestMemoryUsageByTotalPercent = guestMemoryUsageByTotalPercent; }
}
