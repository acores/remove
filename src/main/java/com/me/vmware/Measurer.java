package com.me.vmware;

import com.me.vmware.config.reader.JsonConfigReader;
import com.me.vmware.entities.*;
import com.me.vmware.service.discover.Discoverer;
import com.vmware.vim25.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.*;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Measurer {
    private static final Logger LOGGER = LoggerFactory.getLogger(Measurer.class);
    private MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
    public List<ObjectName> registeredBeans = new ArrayList<>();
    private long clearIntervalSec;
    private ServerConnection connection;
    private VimPortType methods;
    private ServiceContent serviceContent;
    private ManagedObjectReference rootFolder;
    private ManagedObjectReference viewManager;
    private ManagedObjectReference propColl;
    private ManagedObjectReference perfMgr;
    private HashMap<String, Integer> countersIdMap = new HashMap<>();
    private HashMap<Integer, PerfCounterInfo> countersInfoMap = new HashMap<>();
    private List<PropertyFilterSpec> fSpecList = new ArrayList<>();
    private RetrieveOptions ro = new RetrieveOptions();
    private MetricsSingleton metrics = MetricsSingleton.getInstance();
    private List<Utils.RedAbstractEntity> registeredEntities = new ArrayList<>();
    Discoverer discoverer;

    public Measurer(ServerConnection connection, long clearIntervalSec) {
        this.connection = connection;
        this.methods = this.connection.getMethods();
        this.serviceContent = this.connection.getServiceContent();
        this.rootFolder = this.serviceContent.getRootFolder();
        this.viewManager = this.serviceContent.getViewManager();
        this.propColl = this.serviceContent.getPropertyCollector();
        this.perfMgr = this.serviceContent.getPerfManager();
        this.clearIntervalSec = clearIntervalSec;
        Utils.fillCountersMaps(this.methods, this.perfMgr, this.propColl, this.countersIdMap, this.countersInfoMap);
        Utils.fillFSpecList(this.methods, this.viewManager, this.rootFolder, this.fSpecList, this.metrics);
    }

    public void clearMBeanServer() {
        for (ObjectName registeredBean : registeredBeans) {
            try {
                mBeanServer.unregisterMBean(registeredBean);
            } catch (InstanceNotFoundException | MBeanRegistrationException e) {
                LOGGER.info("Unregister bean: ", e);
            }
        }
    }

    public List<Utils.RedAbstractEntity> getRegisteredEntities() {
        return registeredEntities;
    }

    public void postMBeanRegister(MBeanServer mBeanServer, AbstractEntity entity, String mBeanDomain, String typeName) {
        String mBeanType = typeName;//entity.getId() + entity.getAdditionalId();
        String mBeanName = entity.getName();
        try {
            ObjectName objectName = new ObjectName(mBeanDomain + ":type=" + mBeanType + ",name=" + mBeanName);
            entity.setObjectName(objectName);
            //mBeanServer.registerMBean(entity, objectName);
            /*if (entity instanceof HostSensorEntity)
                LOGGER.info("postMBeanRegister: " + objectName);*/
            registeredEntities.add(new Utils.RedAbstractEntity(entity, typeName));

        } catch (MalformedObjectNameException e) {
            LOGGER.error("Error during MBean " + entity.getName() + " registration operation. ", e);
        }
    }

    public void calculateGroup(String domain) {
        List<AbstractEntity> discovererEntities = discoverer.getDiscoverEntitiesStorage().getAllEntities();


        List<String> discoveredENames = discovererEntities.stream().map(AbstractEntity::getName).collect(Collectors.toList());
        List<String> measuredENames = registeredEntities.stream().map(x -> x.getAbstractEntity().getName()).collect(Collectors.toList());
        List<String> undiscoveredEntities = new ArrayList<>();
        List<String> unmeasuredENames = new ArrayList<>(discoveredENames);
        unmeasuredENames.removeAll(measuredENames);

        List<String> lostGroupsAtDiscovered = new ArrayList<>();


        //registeredEntities собранные объекты в обычном режиме
        for (Utils.RedAbstractEntity abstractEntity : registeredEntities) {
            String name = abstractEntity.getAbstractEntity().getName();

            List<AbstractEntity> collect = discovererEntities.stream()
                    .filter(x ->
                            name.equals(x.getName()) &&
                            x.getId().equals(abstractEntity.getAbstractEntity().getId())
                            && x.getClass().equals(abstractEntity.getAbstractEntity().getClass())
                    )
                    .collect(Collectors.toList());

            if (collect.isEmpty()) {
                //discover не обнаружил объект с именем
                //LOGGER.error("not found for: " + abstractEntity.getAbstractEntity().getName());
                undiscoveredEntities.add(name);
            } else {
                if (collect.get(0).getGroup() == null) {
                    //LOGGER.error("group not found for: " + abstractEntity.getAbstractEntity().getName());
                    lostGroupsAtDiscovered.add(name);
                } else {
                    abstractEntity.getAbstractEntity().setGroup(collect.get(0).getGroup());
                }
            }
        }


        report(discoveredENames, measuredENames, undiscoveredEntities, unmeasuredENames, lostGroupsAtDiscovered);
    }

    public static void report(List<String> discoveredENames,
                              List<String> measuredENames,
                              List<String> undiscoveredEntities,
                              List<String> unmeasuredENames,
                              List<String> lostGroupsAtDiscovered) {

        if (!VMWareJMX.generateReport) {
            return;
        }

        Function<String, HashMap<String, String>> wrap = s -> new HashMap<String, String>() {{
            put("result", s);
        }};

        String k1 = (String.format("Discovered entity names: [%s]", discoveredENames.size()));
        String v1 = (discoveredENames.stream().sorted().collect(Collectors.joining(",")));

        String k2 = (String.format("Measured entity names: [%s]", measuredENames.size()));
        String v2 = (measuredENames.stream().sorted().collect(Collectors.joining(",")));

        String k3 = (String.format("Un-discovered entity names: [%s]", undiscoveredEntities.size()));
        String v3 = (String.join(",", undiscoveredEntities));

        String k4 = (String.format("Un-measured entity names: [%s]", unmeasuredENames.size()));
        String v4 = (unmeasuredENames.stream().sorted().collect(Collectors.joining(",")));

        String k5 = (String.format("Discovered entity with group==null names: [%s]", lostGroupsAtDiscovered.size()));
        String v5 = (lostGroupsAtDiscovered.stream().sorted().collect(Collectors.joining(",")));

        List<String> duplicatesDiscovered = AppUtils.listDuplicates(discoveredENames);
        String k6 = (String.format("Discovered entities duplicates: [%s]", duplicatesDiscovered.size()));
        String v6 = (duplicatesDiscovered.stream().sorted().collect(Collectors.joining(",")));

        List<String> duplicatesMeasured = AppUtils.listDuplicates(measuredENames);
        String k7 = (String.format("Measured entities duplicates: [%s]", duplicatesMeasured.size()));
        String v7 = (duplicatesMeasured.stream().sorted().collect(Collectors.joining(",")));

        List<String> unDuplicatesMeasured = new ArrayList<>(new HashSet<>(measuredENames));
        String k8 = (String.format("Measured entities without duplicates: [%s]", unDuplicatesMeasured.size()));
        String v8 = (unDuplicatesMeasured.stream().sorted().collect(Collectors.joining(",")));

        List<String> unDuplicatesDiscovered = new ArrayList<>(new HashSet<>(discoveredENames));
        String k9 = (String.format("Discovered entities without duplicates: [%s]", unDuplicatesDiscovered.size()));
        String v9 = (unDuplicatesDiscovered.stream().sorted().collect(Collectors.joining(",")));

        Map<String, HashMap<String, String>> map = new HashMap<String, HashMap<String, String>>() {{
            put(k1, wrap.apply(v1));
            put(k2, wrap.apply(v2));
            put(k3, wrap.apply(v3));
            put(k4, wrap.apply(v4));
            put(k5, wrap.apply(v5));
            put(k6, wrap.apply(v6));
            put(k7, wrap.apply(v7));
            put(k8, wrap.apply(v8));
            put(k9, wrap.apply(v9));
        }};

        File dsf = JsonConfigReader.randomFile("report-", ".json", "reports");
        JsonConfigReader.saveJson(dsf, map, true);

        LOGGER.info("report saved to: {}", dsf.getPath());
    }

    public void run(String domainName) {
        try {
            LOGGER.info("Collecting data from vCenter/ESX server...");

            registeredEntities = new ArrayList<>();

            discoverer = new Discoverer(connection);
            discoverer.run();

            collectProperties(domainName);
            //LOGGER.error("run.registeredEntities.size: " + registeredEntities.size());
            //LOGGER.error("run.discoverer.size: " + discoverer.getDiscoverEntitiesStorage().getAllEntities().size());


            calculateGroup(domainName);


            if (VMWareJMX.getAppConfig().isFlatBeans()) {
                for (Utils.RedAbstractEntity redEntity : registeredEntities) {
                    Utils.mBeanRegister(this.mBeanServer, redEntity.getAbstractEntity(), domainName, redEntity.getTypename());
                }
            } else {
                // перепроверка групп
                for (Utils.RedAbstractEntity redEntity : registeredEntities) {
                    if (redEntity.abstractEntity.getGroup() == null) {
                        LOGGER.info("group(2) not found for: " + redEntity.getAbstractEntity().getName());
                    } else {
                        Utils.mBeanRegisterWithGroup(this.mBeanServer, redEntity.getAbstractEntity(), domainName);
                    }
                }
            }


            setResourcePoolIdToVMsInVApp();

            measureDatastores();
            measureHostSystems();
            measureVirtualMachines();
            measureResourcePools();
            measureComputeResources();
            measureClusterComputeResources();

            measureDatacenterExtras();


            Utils.removeOldEntities(this.mBeanServer, EntitiesStorage.getDatacenterEntities(), this.clearIntervalSec);
            Utils.removeOldEntities(this.mBeanServer, EntitiesStorage.getComputeResourceEntities(), this.clearIntervalSec);
            Utils.removeOldEntities(this.mBeanServer, EntitiesStorage.getClusterComputeResourceEntities(), this.clearIntervalSec);
            Utils.removeOldEntities(this.mBeanServer, EntitiesStorage.getDatastoreEntities(), this.clearIntervalSec);
            Utils.removeOldEntities(this.mBeanServer, EntitiesStorage.getResourcePoolEntities(), this.clearIntervalSec);
            Utils.removeOldEntities(this.mBeanServer, EntitiesStorage.getHostSystemEntities(), this.clearIntervalSec);
            Utils.removeOldEntities(this.mBeanServer, EntitiesStorage.getHostSensorEntities(), this.clearIntervalSec);
            Utils.removeOldEntities(this.mBeanServer, EntitiesStorage.getVirtualMachineEntities(), this.clearIntervalSec);
            Utils.removeOldEntities(this.mBeanServer, EntitiesStorage.getVirtualDiskEntities(), this.clearIntervalSec);

            LOGGER.info("Data collection from vCenter/ESX server completed");
        } catch (Exception e) {
            LOGGER.error("Error during data collection operation. ", e);
            this.connection.disconnect();
        }
    }

    void measureDatacenterExtras() {
        /*
         * Количество ВМ датацентра
         * Общее дисковое пространство виртуальных машин
         * Общее количество ядер процессора
         * Общая утилизация процессора
         * Общая утилизация памяти
         * Количество включенных ВМ
         * Количество выключенных ВМ
         * Общая утилизация дискового пространства виртуальных машин
         * Количество датасторов
         * Количество сенсоров
         * Количество хостов
         * Количество пулов
         * Количество кластеров
         * Количество дисков
         * */
        for (DatacenterEntity datacenter : EntitiesStorage.getDatacenterEntities()) {
            int totalVms = 0;// Количество ВМ датацентра
            long totalMemorySizeMB = 0;// Общее дисковое пространство виртуальных машин
            int totalCpuCores = 0;// Общее количество ядер процессора
            long cpuAllocationLimitMHz = 0;// Общая утилизация процессора
            long memoryAllocationLimitMB = 0;// Общая утилизация памяти
            int totalEnabledVMs = 0;// Количество включенных ВМ
            int totalDisabledVMs = 0;// Количество выключенных ВМ
            int totalVMAllocationLimitMB = 0;// Общая утилизация дискового пространства виртуальных машин (оперативная память)
            int totalDatastores = 0;// Количество датасторов
            int totalSensors = 0;// Количество сенсоров
            int totalHosts = 0;// Количество хостов
            int totalPools = 0;// Количество пулов
            int totalClusters = 0;// Количество кластеров
            int totalDisks = 0;// Количество дисков

            int totalDiskCapacityMB = 0; //дисковое пространство
            int totalDatastoreCapacityMB = 0; //дисковое пространство datastore
            int totalDatastoreFreeSpaceMB = 0; //свободное дисковое пространство datastore
            long overallCpuUsageMHz = 0;

            long cpuAllocationReservationMHz = 0;
            int guestMemoryUsageMB = 0;
            int memoryAllocationReservationMB = 0;



            /*
             * ресурсный пул может содержать компут ресурсы и кластеры
             * сводные свойства кластера содержаться в корневом ресурспулле
             *
             * */

            try {
                ManagedObjectReference dcRef = datacenter.getObjectReference();
                List<ManagedObjectReference> clusters = discoverer.getChilds("ClusterComputeResource", dcRef);
                List<ManagedObjectReference> computeResources = discoverer.getChilds("ComputeResource", dcRef);
                List<ManagedObjectReference> resourcePools = discoverer.getChilds("ResourcePool", dcRef);
                List<ManagedObjectReference> hostSystems = discoverer.getChilds("HostSystem", dcRef);
                List<ManagedObjectReference> vms = discoverer.getChilds("VirtualMachine", dcRef);
                List<ManagedObjectReference> dss = discoverer.getChilds("Datastore", dcRef);

                for (ManagedObjectReference ref : dss) {
                    for (DatastoreEntity x : EntitiesStorage.getDatastoreEntities()) {
                        if (x.getObjectReference().getValue().equals(ref.getValue())) {
                            totalDatastoreCapacityMB += x.getCapacityMB();
                            totalDatastoreFreeSpaceMB += x.getFreeSpaceMB();
                        }
                    }
                }

                for (VirtualMachineEntity vm : EntitiesStorage.getVirtualMachineEntities()) {
                    for (VirtualDiskEntity x : vm.getMappedVirtualDisks()) {
                        totalDiskCapacityMB += x.getCapacityMB();
                    }
                }


                for (ManagedObjectReference ref : hostSystems) {
                    for (HostSystemEntity x : EntitiesStorage.getHostSystemEntities()) {
                        if (x.getObjectReference().getValue().equals(ref.getValue())) {
                            totalSensors += x.getMappedHostSensors().size();
                        }
                    }
                }
                for (ManagedObjectReference ref : vms) {
                    for (VirtualMachineEntity x : EntitiesStorage.getVirtualMachineEntities()) {
                        if (x.getObjectReference().getValue().equals(ref.getValue())) {
                            totalDisks += x.getMappedVirtualDisks().size();
                        }
                    }
                }


                totalDatastores += discoverer.getChilds("Datastore", dcRef).size();
                totalPools += resourcePools.size();
                totalClusters += clusters.size();
                //totalDisks += discoverer.getChilds("VirtualDisk", dcRef).size();

                for (ManagedObjectReference ref : clusters) {
                    for (ClusterComputeResourceEntity x : EntitiesStorage.getClusterComputeResourceEntities()) {
                        if (x.getObjectReference().getValue().equals(ref.getValue())) {
                            totalVms += x.getNumVM();
                            totalMemorySizeMB += x.getTotalMemoryMB();
                            totalCpuCores += x.getNumCpuCores();
                            totalHosts += x.getNumHosts();

                            for (ManagedObjectReference vmRef : discoverer.getChilds("VirtualMachine", ref)) {
                                for (VirtualMachineEntity vm : EntitiesStorage.getVirtualMachineEntities()) {
                                    if (vm.getObjectReference().getValue().equals(vmRef.getValue())) {
                                        totalEnabledVMs += vm.getPowerStatus() == 1 ? 1 : 0;
                                        totalDisabledVMs += vm.getPowerStatus() == 0 ? 1 : 0;
                                        totalVMAllocationLimitMB += vm.getMemorySizeMB() - vm.getMemoryUsageAbsoluteMB();
                                    }
                                }
                            }

                            x.setTotalEnabledVMs(totalEnabledVMs);
                            x.setTotalDisabledVMs(totalDisabledVMs);
                        }
                    }
                }

                for (ManagedObjectReference ref : computeResources) {
                    for (ComputeResourceEntity x : EntitiesStorage.getComputeResourceEntities()) {
                        if (x.getObjectReference().getValue().equals(ref.getValue())) {
                            totalVms += x.getNumVM();
                            totalMemorySizeMB += x.getTotalMemoryMB();
                            totalCpuCores += x.getNumCpuCores();
                            totalHosts += x.getNumHosts();

                            for (ManagedObjectReference vmRef : discoverer.getChilds("VirtualMachine", ref)) {
                                for (VirtualMachineEntity vm : EntitiesStorage.getVirtualMachineEntities()) {
                                    if (vm.getObjectReference().getValue().equals(vmRef.getValue())) {
                                        totalEnabledVMs += vm.getPowerStatus() == 1 ? 1 : 0;
                                        totalDisabledVMs += vm.getPowerStatus() == 0 ? 1 : 0;
                                        totalVMAllocationLimitMB += vm.getMemorySizeMB() - vm.getMemoryUsageAbsoluteMB();
                                    }
                                }
                            }
                        }
                    }
                }

                for (ManagedObjectReference rpRef : resourcePools) {
                    for (ResourcePoolEntity resourcePoolEntity : EntitiesStorage.getResourcePoolEntities()) {
                        if (resourcePoolEntity.getObjectReference().getValue().equals(rpRef.getValue())) {
                            cpuAllocationLimitMHz += resourcePoolEntity.getCpuAllocationLimitMHz();
                            memoryAllocationLimitMB += resourcePoolEntity.getMemoryAllocationLimitMB();
                            overallCpuUsageMHz += resourcePoolEntity.getOverallCpuUsageMHz();
                            cpuAllocationReservationMHz += resourcePoolEntity.getCpuAllocationReservationMHz();
                            guestMemoryUsageMB += resourcePoolEntity.getGuestMemoryUsageMB();
                            memoryAllocationReservationMB += resourcePoolEntity.getMemoryAllocationReservationMB();


                            int enabledVms = 0;
                            int disabledVms = 0;
                            for (ManagedObjectReference vmRef : discoverer.getChilds("VirtualMachine", rpRef)) {
                                for (VirtualMachineEntity vm : EntitiesStorage.getVirtualMachineEntities()) {
                                    if (vm.getObjectReference().getValue().equals(vmRef.getValue())) {
                                        enabledVms += vm.getPowerStatus() == 1 ? 1 : 0;
                                        disabledVms += vm.getPowerStatus() == 0 ? 1 : 0;
                                    }
                                }
                            }
                            resourcePoolEntity.setTotalEnabledVMs(enabledVms);
                            resourcePoolEntity.setTotalDisabledVMs(disabledVms);
                        }
                    }
                }

                datacenter.setTotalVms(totalVms);
                datacenter.setTotalMemorySizeMB(totalMemorySizeMB);
                datacenter.setTotalCpuCores(totalCpuCores);
                datacenter.setCpuAllocationLimitMHz(cpuAllocationLimitMHz);
                datacenter.setMemoryAllocationLimitMB(memoryAllocationLimitMB);
                datacenter.setTotalEnabledVMs(totalEnabledVMs);
                datacenter.setTotalDisabledVMs(totalDisabledVMs);
                datacenter.setTotalVMAllocationLimitMB(totalVMAllocationLimitMB);
                datacenter.setTotalDatastores(totalDatastores);
                datacenter.setTotalSensors(totalSensors);
                datacenter.setTotalHosts(totalHosts);
                datacenter.setTotalPools(totalPools);
                datacenter.setTotalClusters(totalClusters);
                datacenter.setTotalDisks(totalDisks);

                datacenter.setTotalDiskCapacityMB(totalDiskCapacityMB);
                datacenter.setTotalDatastoreCapacityMB(totalDatastoreCapacityMB);
                datacenter.setTotalDatastoreFreeSpaceMB(totalDatastoreFreeSpaceMB);

                datacenter.setOverallCpuUsageMHz(overallCpuUsageMHz);
                datacenter.setCpuAllocationReservationMHz(cpuAllocationReservationMHz);
                datacenter.setGuestMemoryUsageMB(guestMemoryUsageMB);
                datacenter.setMemoryAllocationReservationMB(memoryAllocationReservationMB);


            } catch (RuntimeFaultFaultMsg | InvalidPropertyFaultMsg e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void collectProperties(String domainName) throws InvalidPropertyFaultMsg, RuntimeFaultFaultMsg {
        RetrieveResult props = this.methods.retrievePropertiesEx(this.propColl, this.fSpecList, this.ro);

        if (props != null) {
            Date dataCollectionTime = new Date();
            Utils.retrieveAllProperties(this.methods, this.propColl, props, props.getToken());

            for (ObjectContent oc : props.getObjects()) {
                ManagedObjectReference objectReference;
                ManagedEntityStatus status;
                HostSystemConnectionState hvConnStatus;
                HostSystemPowerState hvPowerStatus;
                VirtualMachineConnectionState vmConnStatus;
                VirtualMachinePowerState vmPowerStatus;
                ArrayOfDatastoreHostMount datastoreHostMounts;
                ArrayOfManagedObjectReference arrayOfManagedObjects;
                ArrayOfVirtualDevice virtualDevices;
                ArrayOfHostNumericSensorInfo hostSensors;
                int num;
                long numLong;
                short numShort;
                boolean statusBoolean;
                String string;
                String name;
                String path;
                List<DynamicProperty> dps = oc.getPropSet();
                if (dps != null && oc.getObj().getType().equals("Datacenter")) {
                    DatacenterEntity entity = null;
                    String entityId = oc.getObj().getValue();
                    boolean entityExist = false;
                    for (DatacenterEntity e : EntitiesStorage.getDatacenterEntities()) {
                        if (e.getId().equals(entityId)) {
                            entityExist = true;
                            entity = e;
                            break;
                        }
                    }
                    if (!entityExist) entity = new DatacenterEntity();
                    entity.setObjectReference(oc.getObj());
                    entity.setId(entityId);
                    entity.setLastDataCollectionTime(dataCollectionTime);
                    for (DynamicProperty dp : dps) {
                        path = dp.getName();
                        if (path.equals("name")) {
                            name = (String) dp.getVal();
                            entity.setName(name + " (" + entityId + ")");
                        }
                        if (path.equals("overallStatus")) {
                            status = (ManagedEntityStatus) dp.getVal();
                            switch (status) {
                                case GREEN:
                                    entity.setOverallStatus("0");
                                    break;
                                case GRAY:
                                    entity.setOverallStatus("1");
                                    break;
                                case YELLOW:
                                    entity.setOverallStatus("2");
                                    break;
                                case RED:
                                    entity.setOverallStatus("3");
                                    break;
                            }
                        }
                    }
                    if (!entityExist) {
                        EntitiesStorage.getDatacenterEntities().add(entity);
                        postMBeanRegister(this.mBeanServer, entity, domainName, "datacenter");
                    }
                }
                if (dps != null && oc.getObj().getType().equals("Datastore")) {
                    DatastoreEntity entity = null;
                    String entityId = oc.getObj().getValue();
                    boolean entityExist = false;
                    for (DatastoreEntity e : EntitiesStorage.getDatastoreEntities()) {
                        if (e.getId().equals(entityId)) {
                            entityExist = true;
                            entity = e;
                            entity.getMappedHosts().clear();
                            entity.getMappedVMs().clear();
                            entity.getDatastoreIops().clear();
                            entity.getTotalReadLatencyMS().clear();
                            entity.getTotalWriteLatencyMS().clear();
                            break;
                        }
                    }
                    if (!entityExist) entity = new DatastoreEntity();
                    entity.setObjectReference(oc.getObj());
                    entity.setId(entityId);
                    entity.setLastDataCollectionTime(dataCollectionTime);
                    for (DynamicProperty dp : dps) {
                        path = dp.getName();
                        if (path.equals("name")) {
                            name = (String) dp.getVal();
                            entity.setName(name + " (" + entityId + ")");
                        }
                        if (path.equals("host")) {
                            datastoreHostMounts = (ArrayOfDatastoreHostMount) dp.getVal();
                            for (DatastoreHostMount host : datastoreHostMounts.getDatastoreHostMount()) {
                                if (host.getMountInfo().isAccessible()) {
                                    entity.getMappedHosts().add(host.getKey());
                                }
                            }
                        }
                        if (path.equals("vm")) {
                            arrayOfManagedObjects = (ArrayOfManagedObjectReference) dp.getVal();
                            for (ManagedObjectReference vm : arrayOfManagedObjects.getManagedObjectReference()) {
                                entity.getMappedVMs().add(vm);
                            }
                        }
                        if (path.equals("summary.url")) {
                            string = (String) dp.getVal();
                            entity.setUrl(string);
                        }
                        if (path.equals("overallStatus")) {
                            status = (ManagedEntityStatus) dp.getVal();
                            switch (status) {
                                case GREEN:
                                    entity.setOverallStatus("0");
                                    break;
                                case GRAY:
                                    entity.setOverallStatus("1");
                                    break;
                                case YELLOW:
                                    entity.setOverallStatus("2");
                                    break;
                                case RED:
                                    entity.setOverallStatus("3");
                                    break;
                            }
                        }
                        if (path.equals("summary.accessible")) {
                            statusBoolean = (boolean) dp.getVal();
                            if (statusBoolean) {
                                entity.setConnectionStatus("0");
                            } else {
                                entity.setConnectionStatus("1");
                            }
                        }
                        if (path.equals("summary.capacity")) {
                            numLong = (long) dp.getVal() / 1024 / 1024;
                            entity.setCapacityMB(numLong);
                        }
                        if (path.equals("summary.freeSpace")) {
                            numLong = (long) dp.getVal() / 1024 / 1024;
                            entity.setFreeSpaceMB(numLong);
                        }
                    }
                    if (!entityExist) {
                        EntitiesStorage.getDatastoreEntities().add(entity);
                        postMBeanRegister(this.mBeanServer, entity, domainName, "datastore");
                    }
                }
                if (dps != null && oc.getObj().getType().equals("HostSystem")) {
                    HostSystemEntity entity = null;
                    String entityId = oc.getObj().getValue();
                    boolean entityExist = false;
                    for (HostSystemEntity e : EntitiesStorage.getHostSystemEntities()) {
                        if (e.getId().equals(entityId)) {
                            entityExist = true;
                            entity = e;
                            break;
                        }
                    }
                    if (!entityExist) entity = new HostSystemEntity();
                    entity.setObjectReference(oc.getObj());
                    entity.setId(entityId);
                    entity.setLastDataCollectionTime(dataCollectionTime);
                    for (DynamicProperty dp : dps) {
                        path = dp.getName();
                        if (path.equals("name")) {
                            name = (String) dp.getVal();
                            entity.setName(name);
                        }
                        if (path.equals("overallStatus")) {
                            status = (ManagedEntityStatus) dp.getVal();
                            switch (status) {
                                case GREEN:
                                    entity.setOverallStatus("0");
                                    break;
                                case GRAY:
                                    entity.setOverallStatus("1");
                                    break;
                                case YELLOW:
                                    entity.setOverallStatus("2");
                                    break;
                                case RED:
                                    entity.setOverallStatus("3");
                                    break;
                            }
                        }
                        if (path.equals("runtime.connectionState")) {
                            hvConnStatus = (HostSystemConnectionState) dp.getVal();
                            switch (hvConnStatus) {
                                case CONNECTED:
                                    entity.setConnectionStatus("0");
                                    break;
                                case DISCONNECTED:
                                    entity.setConnectionStatus("1");
                                    break;
                                case NOT_RESPONDING:
                                    entity.setConnectionStatus("2");
                                    break;
                            }
                        }
                        if (path.equals("runtime.powerState")) {
                            hvPowerStatus = (HostSystemPowerState) dp.getVal();
                            switch (hvPowerStatus) {
                                case POWERED_OFF:
                                    entity.setPowerStatus("0");
                                    break;
                                case POWERED_ON:
                                    entity.setPowerStatus("1");
                                    break;
                                case STAND_BY:
                                    entity.setPowerStatus("2");
                                    break;
                                case UNKNOWN:
                                    entity.setPowerStatus("3");
                                    break;
                            }
                        }
                        if (path.equals("summary.hardware.numCpuCores")) {
                            numShort = (short) dp.getVal();
                            entity.setNumCpu(numShort);
                        }
                        if (path.equals("summary.hardware.memorySize")) {
                            numLong = (long) dp.getVal() / 1024 / 1024;
                            entity.setMemorySizeMB(numLong);
                        }
                    }
                    if (!entityExist) {
                        EntitiesStorage.getHostSystemEntities().add(entity);
                        postMBeanRegister(this.mBeanServer, entity, domainName, "host");
                    }

                    for (DynamicProperty dp : dps) {
                        path = dp.getName();
                        if (path.equals("runtime.healthSystemRuntime.systemHealthInfo.numericSensorInfo")) {
                            hostSensors = (ArrayOfHostNumericSensorInfo) dp.getVal();
                            //LOGGER.info("mesurer size: " + entity.getName() + ":" + hostSensors.getHostNumericSensorInfo().size());


                            for (HostNumericSensorInfo e : hostSensors.getHostNumericSensorInfo()) {
                                HostSensorEntity hostSensorEntity = null;
                                String hostSensorId = e.getId();
                                String hostSensorName = e.getName();
                                String sensorType = e.getSensorType();
                                String healthState = e.getHealthState().getKey();
                                boolean hostSensorEntityExist = false;

                                if (entityExist) {
                                    for (HostSensorEntity sensor : entity.getMappedHostSensors()) {
                                        if (sensor.getAdditionalId().equals(hostSensorId)) {
                                            hostSensorEntityExist = true;
                                            hostSensorEntity = sensor;
                                            break;
                                        }
                                    }
                                }
                                if (!hostSensorEntityExist) {
                                    //LOGGER.info(hostSensorName + " not exists!");
                                    hostSensorEntity = new HostSensorEntity();
                                }

                                hostSensorEntity.setName(hostSensorName);
                                hostSensorEntity.setId(entity.getId());
                                hostSensorEntity.setAdditionalId(hostSensorId);
                                hostSensorEntity.setMappedHost(entity);
                                hostSensorEntity.setSensorType(sensorType);
                                hostSensorEntity.setLastDataCollectionTime(dataCollectionTime);
                                switch (healthState) {
                                    case "green":
                                        hostSensorEntity.setHealthState("0");
                                        break;
                                    case "unknown":
                                        hostSensorEntity.setHealthState("1");
                                        break;
                                    case "yellow":
                                        hostSensorEntity.setHealthState("2");
                                        break;
                                    case "red":
                                        hostSensorEntity.setHealthState("3");
                                        break;
                                }

                                if (!hostSensorEntityExist) {
                                    entity.getMappedHostSensors().add(hostSensorEntity);
                                    EntitiesStorage.getHostSensorEntities().add(hostSensorEntity);
                                    postMBeanRegister(this.mBeanServer, hostSensorEntity, domainName, "hostsensor");
                                }
                            }
                        }
                    }
                }
                if (dps != null && oc.getObj().getType().equals("VirtualMachine")) {
                    VirtualMachineEntity entity = null;
                    String entityId = oc.getObj().getValue();
                    boolean entityExist = false;
                    for (VirtualMachineEntity e : EntitiesStorage.getVirtualMachineEntities()) {
                        if (e.getId().equals(entityId)) {
                            entityExist = true;
                            entity = e;
                            break;
                        }
                    }
                    if (!entityExist) entity = new VirtualMachineEntity();
                    entity.setObjectReference(oc.getObj());
                    entity.setId(entityId);
                    entity.setLastDataCollectionTime(dataCollectionTime);
                    for (DynamicProperty dp : dps) {
                        path = dp.getName();
                        if (path.equals("name")) {
                            name = (String) dp.getVal();
                            entity.setName(name + " (" + entityId + ")");
                        }
                        if (path.equals("guest.ipAddress")) {
                            entity.setIpAddress((String) dp.getVal());
                        }
                        if (path.equals("resourcePool")) {
                            objectReference = (ManagedObjectReference) dp.getVal();
                            entity.setResourcePool(objectReference);
                            entity.setResourcePoolId(objectReference.getValue());
                        }
                        if (path.equals("overallStatus")) {
                            status = (ManagedEntityStatus) dp.getVal();
                            switch (status) {
                                case GREEN:
                                    entity.setOverallStatus("0");
                                    break;
                                case GRAY:
                                    entity.setOverallStatus("1");
                                    break;
                                case YELLOW:
                                    entity.setOverallStatus("2");
                                    break;
                                case RED:
                                    entity.setOverallStatus("3");
                                    break;
                            }
                        }
                        if (path.equals("runtime.connectionState")) {
                            vmConnStatus = (VirtualMachineConnectionState) dp.getVal();
                            switch (vmConnStatus) {
                                case CONNECTED:
                                    entity.setConnectionStatus("0");
                                    break;
                                case DISCONNECTED:
                                    entity.setConnectionStatus("1");
                                    break;
                                case ORPHANED:
                                    entity.setConnectionStatus("2");
                                    break;
                                case INACCESSIBLE:
                                    entity.setConnectionStatus("3");
                                    break;
                                case INVALID:
                                    entity.setConnectionStatus("4");
                                    break;
                            }
                        }
                        if (path.equals("runtime.powerState")) {
                            vmPowerStatus = (VirtualMachinePowerState) dp.getVal();
                            switch (vmPowerStatus) {
                                case POWERED_OFF:
                                    entity.setPowerStatus("0");
                                    break;
                                case POWERED_ON:
                                    entity.setPowerStatus("1");
                                    break;
                                case SUSPENDED:
                                    entity.setPowerStatus("2");
                                    break;
                            }
                        }
                        if (path.equals("summary.config.numCpu")) {
                            num = (int) dp.getVal();
                            entity.setNumCpu(num);
                        }
                        if (path.equals("summary.config.memorySizeMB")) {
                            num = (int) dp.getVal();
                            entity.setMemorySizeMB(num);
                        }
                        if (path.equals("summary.quickStats.balloonedMemory")) {
                            num = (int) dp.getVal();
                            entity.setBalloonedMemoryMB(num);
                        }
                        if (path.equals("summary.quickStats.swappedMemory")) {
                            num = (int) dp.getVal();
                            entity.setSwappedMemoryMB(num);
                        }
                    }
                    if (!entityExist) {
                        EntitiesStorage.getVirtualMachineEntities().add(entity);
                        postMBeanRegister(this.mBeanServer, entity, domainName, "virtualmachine");
                    }

                    for (DynamicProperty dp : dps) {
                        path = dp.getName();
                        if (path.equals("config.hardware.device")) {
                            Map<Integer, String> scsiControllersMap = new HashMap<>();
                            virtualDevices = (ArrayOfVirtualDevice) dp.getVal();

                            virtualDevices.getVirtualDevice().forEach(e -> {
                                if (e instanceof VirtualSCSIController) {
                                    VirtualSCSIController virtualSCSIController = (VirtualSCSIController) e;
                                    int controllerKey = virtualSCSIController.getKey();
                                    String controllerName = virtualSCSIController.getDeviceInfo().getLabel().replace("SCSI controller ", "scsi");
                                    scsiControllersMap.put(controllerKey, controllerName);
                                }
                            });

                            for (VirtualDevice e : virtualDevices.getVirtualDevice()) {
                                if (e instanceof VirtualDisk) {
                                    VirtualDiskEntity virtualDiskEntity = null;
                                    VirtualDisk virtualDisk = (VirtualDisk) e;
                                    int controllerKey = virtualDisk.getControllerKey();
                                    //System.out.println(Integer.toString(controllerKey)+"-");
                                    int virtualDiskKey = virtualDisk.getKey();
                                    //System.out.println(Integer.toString(virtualDiskKey)+"--");
                                    int virtualDiskUnitNumber = virtualDisk.getUnitNumber();
                                    //System.out.println(Integer.toString(virtualDiskUnitNumber)+"---");
                                    String controllerName = scsiControllersMap.get(controllerKey);//
                                    String virtualDiskInstance = controllerName + ":" + virtualDiskUnitNumber + "~";
                                    //System.out.println(virtualDiskInstance+"----");
                                    String virtualDiskName = virtualDisk.getDeviceInfo().getLabel();//
                                    //System.out.println(virtualDiskName+"-----");
                                    boolean virtualDiskEntityExist = false;
                                    if (entityExist) {
                                        for (VirtualDiskEntity vd : entity.getMappedVirtualDisks()) {
                                            if (vd.getAdditionalId().equals(Integer.toString(virtualDiskKey))) {
                                                virtualDiskEntityExist = true;
                                                virtualDiskEntity = vd;
                                                break;
                                            }
                                        }
                                    }
                                    if (!virtualDiskEntityExist) virtualDiskEntity = new VirtualDiskEntity();


                                    virtualDiskEntity.setId(entity.getId());
                                    virtualDiskEntity.setAdditionalId(Integer.toString(virtualDiskKey));
                                    virtualDiskEntity.setMappedVM(entity);
                                    virtualDiskEntity.setInstance(virtualDiskInstance);
                                    virtualDiskEntity.setCapacityMB(virtualDisk.getCapacityInBytes() / 1024 / 1024);
                                    virtualDiskEntity.setLastDataCollectionTime(dataCollectionTime);
                                    virtualDiskEntity.setName(virtualDiskName + " (" + entity.getId() + "" + virtualDiskKey + ")");

                                    if (!virtualDiskEntityExist) {
                                        entity.getMappedVirtualDisks().add(virtualDiskEntity);
                                        EntitiesStorage.getVirtualDiskEntities().add(virtualDiskEntity);
                                        postMBeanRegister(this.mBeanServer, virtualDiskEntity, domainName, "virtualdisk");
                                    }
                                }
                            }
                        }
                    }
                }
                if (dps != null && oc.getObj().getType().equals("ResourcePool")) {
                    ResourcePoolEntity entity = null;
                    String entityId = oc.getObj().getValue();
                    boolean entityExist = false;
                    for (ResourcePoolEntity e : EntitiesStorage.getResourcePoolEntities()) {
                        if (e.getId().equals(entityId)) {
                            entityExist = true;
                            entity = e;
                            entity.getMappedVMs().clear();
                            entity.getChildResourcePools().clear();
                            break;
                        }
                    }
                    if (!entityExist) entity = new ResourcePoolEntity();
                    entity.setObjectReference(oc.getObj());
                    entity.setId(entityId);
                    entity.setLastDataCollectionTime(dataCollectionTime);
                    for (DynamicProperty dp : dps) {
                        path = dp.getName();
                        if (path.equals("name")) {
                            name = (String) dp.getVal();
                            entity.setName(name + " (" + entityId + ")");
                        }
                        if (path.equals("parent")) {
                            objectReference = (ManagedObjectReference) dp.getVal();
                            entity.setParentResourcePool(objectReference);
                            entity.setParentResourcePoolId(objectReference.getValue());
                        }
                        if (path.equals("owner")) {
                            objectReference = (ManagedObjectReference) dp.getVal();
                            entity.setComputeResource(objectReference);
                            entity.setComputeResourceId(objectReference.getValue());
                        }
                        if (path.equals("overallStatus")) {
                            status = (ManagedEntityStatus) dp.getVal();
                            switch (status) {
                                case GREEN:
                                    entity.setOverallStatus("0");
                                    break;
                                case GRAY:
                                    entity.setOverallStatus("1");
                                    break;
                                case YELLOW:
                                    entity.setOverallStatus("2");
                                    break;
                                case RED:
                                    entity.setOverallStatus("3");
                                    break;
                            }
                        }
                        if (path.equals("vm")) {
                            arrayOfManagedObjects = (ArrayOfManagedObjectReference) dp.getVal();
                            entity.setNumVMs(arrayOfManagedObjects.getManagedObjectReference().size());
                        }
                        if (path.equals("config.cpuAllocation.limit")) {
                            numLong = (long) dp.getVal();
                            if (numLong > 0) entity.setCpuAllocationLimitMHz(numLong);
                        }
                        if (path.equals("config.cpuAllocation.reservation")) {
                            numLong = (long) dp.getVal();
                            if (numLong > 0) entity.setCpuAllocationReservationMHz(numLong);
                        }
                        if (path.equals("config.memoryAllocation.limit")) {
                            numLong = (long) dp.getVal();
                            if (numLong > 0) entity.setMemoryAllocationLimitMB(numLong);
                        }
                        if (path.equals("config.memoryAllocation.reservation")) {
                            numLong = (long) dp.getVal();
                            if (numLong > 0) entity.setMemoryAllocationReservationMB(numLong);
                        }
                        if (path.equals("summary.quickStats.overallCpuUsage")) {
                            numLong = (long) dp.getVal();
                            entity.setOverallCpuUsageMHz(numLong);
                        }
                        if (path.equals("summary.quickStats.guestMemoryUsage")) {
                            numLong = (long) dp.getVal();
                            entity.setGuestMemoryUsageMB(numLong);
                        }
                    }
                    if (!entityExist) {
                        EntitiesStorage.getResourcePoolEntities().add(entity);
                        postMBeanRegister(this.mBeanServer, entity, domainName, "resourcepool");
                    }
                }
                if (dps != null && oc.getObj().getType().equals("ComputeResource")) {
                    ComputeResourceEntity entity = null;
                    String entityId = oc.getObj().getValue();
                    boolean entityExist = false;
                    for (ComputeResourceEntity e : EntitiesStorage.getComputeResourceEntities()) {
                        if (e.getId().equals(entityId)) {
                            entityExist = true;
                            entity = e;
                            break;
                        }
                    }
                    if (!entityExist) entity = new ComputeResourceEntity();
                    entity.setObjectReference(oc.getObj());
                    entity.setId(entityId);
                    entity.setLastDataCollectionTime(dataCollectionTime);
                    for (DynamicProperty dp : dps) {
                        path = dp.getName();
                        if (path.equals("name")) {
                            name = (String) dp.getVal();
                            entity.setName(name);
                        }
                        if (path.equals("resourcePool")) {
                            objectReference = (ManagedObjectReference) dp.getVal();
                            entity.setRootResourcePool(objectReference);
                        }
                        if (path.equals("overallStatus")) {
                            status = (ManagedEntityStatus) dp.getVal();
                            switch (status) {
                                case GREEN:
                                    entity.setOverallStatus("0");
                                    break;
                                case GRAY:
                                    entity.setOverallStatus("1");
                                    break;
                                case YELLOW:
                                    entity.setOverallStatus("2");
                                    break;
                                case RED:
                                    entity.setOverallStatus("3");
                                    break;
                            }
                        }
                        if (path.equals("summary.numCpuCores")) {
                            numShort = (short) dp.getVal();
                            entity.setNumCpuCores(numShort);
                        }
                        if (path.equals("summary.numEffectiveHosts")) {
                            num = (int) dp.getVal();
                            entity.setNumEffectiveHosts(num);
                        }
                        if (path.equals("summary.numHosts")) {
                            num = (int) dp.getVal();
                            entity.setNumHosts(num);
                        }
                        if (path.equals("summary.effectiveCpu")) {
                            num = (int) dp.getVal();
                            entity.setEffectiveCpuMHz(num);
                        }
                        if (path.equals("summary.totalCpu")) {
                            num = (int) dp.getVal();
                            entity.setTotalCpuMHz(num);
                        }
                        if (path.equals("summary.effectiveMemory")) {
                            numLong = (long) dp.getVal();
                            entity.setEffectiveMemoryMB(numLong);
                        }
                        if (path.equals("summary.totalMemory")) {
                            numLong = (long) dp.getVal() / 1024 / 1024;
                            entity.setTotalMemoryMB(numLong);
                        }
                    }
                    if (!entityExist) {
                        EntitiesStorage.getComputeResourceEntities().add(entity);
                        postMBeanRegister(this.mBeanServer, entity, domainName, "computeresource");
                    }
                }
                if (dps != null && oc.getObj().getType().equals("ClusterComputeResource")) {
                    ClusterComputeResourceEntity entity = null;
                    String entityId = oc.getObj().getValue();
                    boolean entityExist = false;
                    for (ClusterComputeResourceEntity e : EntitiesStorage.getClusterComputeResourceEntities()) {
                        if (e.getId().equals(entityId)) {
                            entityExist = true;
                            entity = e;
                            break;
                        }
                    }
                    if (!entityExist) entity = new ClusterComputeResourceEntity();
                    entity.setObjectReference(oc.getObj());
                    entity.setId(entityId);
                    entity.setLastDataCollectionTime(dataCollectionTime);
                    for (DynamicProperty dp : dps) {
                        path = dp.getName();
                        if (path.equals("name")) {
                            name = (String) dp.getVal();
                            entity.setName(name + " (" + entityId + ")");//
                        }
                        if (path.equals("resourcePool")) {
                            objectReference = (ManagedObjectReference) dp.getVal();
                            entity.setRootResourcePool(objectReference);
                        }
                        if (path.equals("overallStatus")) {
                            status = (ManagedEntityStatus) dp.getVal();
                            switch (status) {
                                case GREEN:
                                    entity.setOverallStatus("0");
                                    break;
                                case GRAY:
                                    entity.setOverallStatus("1");
                                    break;
                                case YELLOW:
                                    entity.setOverallStatus("2");
                                    break;
                                case RED:
                                    entity.setOverallStatus("3");
                                    break;
                            }
                        }

                        if (path.equals("summary.numCpuCores")) {
                            numShort = (short) dp.getVal();
                            entity.setNumCpuCores(numShort);
                        }
                        if (path.equals("summary.numEffectiveHosts")) {
                            num = (int) dp.getVal();
                            entity.setNumEffectiveHosts(num);
                        }
                        if (path.equals("summary.numHosts")) {
                            num = (int) dp.getVal();
                            entity.setNumHosts(num);
                        }
                        if (path.equals("summary.effectiveCpu")) {
                            num = (int) dp.getVal();
                            entity.setEffectiveCpuMHz(num);
                        }
                        if (path.equals("summary.totalCpu")) {
                            num = (int) dp.getVal();
                            entity.setTotalCpuMHz(num);
                        }
                        if (path.equals("summary.effectiveMemory")) {
                            numLong = (long) dp.getVal();
                            entity.setEffectiveMemoryMB(numLong);
                        }
                        if (path.equals("summary.totalMemory")) {
                            numLong = (long) dp.getVal() / 1024 / 1024;
                            entity.setTotalMemoryMB(numLong);
                        }
                    }
                    if (!entityExist) {
                        EntitiesStorage.getClusterComputeResourceEntities().add(entity);
                        postMBeanRegister(this.mBeanServer, entity, domainName, "clustercomputeresource");
                    }
                }
                if (dps != null && oc.getObj().getType().equals("VirtualApp")) {
                    VirtualAppEntity entity = null;
                    String entityId = oc.getObj().getValue();
                    boolean entityExist = false;
                    for (VirtualAppEntity e : EntitiesStorage.getVirtualAppEntities()) {
                        if (e.getId().equals(entityId)) {
                            entityExist = true;
                            entity = e;
                            entity.setRoot(false);
                            entity.getChildVirtualApps().clear();
                            break;
                        }
                    }
                    if (!entityExist) entity = new VirtualAppEntity();
                    entity.setObjectReference(oc.getObj());
                    entity.setId(entityId);
                    entity.setLastDataCollectionTime(dataCollectionTime);
                    for (DynamicProperty dp : dps) {
                        path = dp.getName();
                        if (path.equals("name")) {
                            name = (String) dp.getVal();
                            entity.setName(name);
                        }
                        if (path.equals("parent")) {
                            objectReference = (ManagedObjectReference) dp.getVal();
                            entity.setParent(objectReference);
                            if (objectReference.getType().equals("ResourcePool")) {
                                entity.setOwnerResourcePool(objectReference);
                                entity.setOwnerResourcePoolId(objectReference.getValue());
                                entity.setRoot(true);
                            }
                        }
                    }
                    if (!entityExist) {
                        EntitiesStorage.getVirtualAppEntities().add(entity);
                    }
                }
            }
        }
    }

    private void setResourcePoolIdToVMsInVApp() {
        List<VirtualAppEntity> virtualAppEntities = EntitiesStorage.getVirtualAppEntities();
        List<VirtualMachineEntity> virtualMachineEntities = EntitiesStorage.getVirtualMachineEntities();

        for (VirtualAppEntity virtualApp : virtualAppEntities) {
            String virtualAppId = virtualApp.getObjectReference().getValue();

            virtualAppEntities.forEach(e -> {
                if (e.getParent() != null) {
                    if (virtualAppId.equals(e.getParent().getValue())) virtualApp.getChildVirtualApps().add(e);
                }
            });
        }

        for (VirtualAppEntity virtualApp : virtualAppEntities) {
            if (virtualApp.isRoot()) {
                List<VirtualAppEntity> allChildVirtualApps = new ArrayList<>();
                this.fillAllChildVirtualApps(virtualApp, allChildVirtualApps);
                allChildVirtualApps.forEach(e -> {
                    e.setOwnerResourcePool(virtualApp.getOwnerResourcePool());
                    e.setOwnerResourcePoolId(virtualApp.getOwnerResourcePoolId());
                });
            }
        }

        for (VirtualAppEntity virtualApp : virtualAppEntities) {
            String virtualAppId = virtualApp.getObjectReference().getValue();

            virtualMachineEntities.forEach(e -> {
                if (e.getResourcePool() != null) {
                    if (e.getResourcePool().getValue().equals(virtualAppId)) {
                        e.setResourcePool(virtualApp.getOwnerResourcePool());
                        e.setResourcePoolId(virtualApp.getOwnerResourcePoolId());
                    }
                }
            });
        }
    }

    private void fillAllChildVirtualApps(VirtualAppEntity currentVirtualApp, List<VirtualAppEntity> resultList) {
        if (!currentVirtualApp.getChildVirtualApps().isEmpty()) {
            resultList.addAll(currentVirtualApp.getChildVirtualApps());
            currentVirtualApp.getChildVirtualApps().forEach((e) -> this.fillAllChildVirtualApps(e, resultList));
        }
    }

    void getMetrics(List<ManagedObjectReference> entities, Map<String, String> counters, BiConsumer<String, Supplier<Object>> action) throws RuntimeFaultFaultMsg {
        List<PerfEntityMetricBase> retrievedStats = Utils.getRealTimeStats(
                this.methods,
                this.perfMgr,
                this.countersIdMap,
                counters,
                "csv",
                entities);

        for (PerfEntityMetricBase singleEntityPerfStats : retrievedStats) {
            PerfEntityMetricCSV entityStatsCsv = (PerfEntityMetricCSV) singleEntityPerfStats;
            List<PerfMetricSeriesCSV> metricsValues = entityStatsCsv.getValue();

            if (!metricsValues.isEmpty()) {
                for (PerfMetricSeriesCSV csv : metricsValues) {
                    PerfCounterInfo pci = this.countersInfoMap.get(csv.getId().getCounterId());
                    String counterName = pci.getGroupInfo().getKey() + "."
                            + pci.getNameInfo().getKey() + "."
                            + pci.getRollupType();

                    action.accept(counterName, csv::getValue);
                }
            }
        }
    }

    private void measureDatastores() throws RuntimeFaultFaultMsg {
        List<DatastoreEntity> datastoreEntities = EntitiesStorage.getDatastoreEntities();

        for (DatastoreEntity entity : datastoreEntities) {
            Map<String, String> countersHV = this.metrics.getDSCountersHV();
            countersHV.entrySet().removeIf(e -> !this.countersIdMap.containsKey(e.getKey()));

            if (!countersHV.isEmpty() && !entity.getMappedHosts().isEmpty()) {
                List<ManagedObjectReference> entities = new ArrayList<>(entity.getMappedHosts());
                List<PerfEntityMetricBase> retrievedStats = Utils.getRealTimeStats(this.methods,
                        this.perfMgr,
                        this.countersIdMap,
                        countersHV,
                        "csv",
                        entities);

                for (PerfEntityMetricBase singleEntityPerfStats : retrievedStats) {
                    PerfEntityMetricCSV entityStatsCsv = (PerfEntityMetricCSV) singleEntityPerfStats;
                    List<PerfMetricSeriesCSV> metricsValues = entityStatsCsv.getValue();

                    if (!metricsValues.isEmpty()) {
                        for (PerfMetricSeriesCSV csv : metricsValues) {
                            PerfCounterInfo pci = this.countersInfoMap.get(csv.getId().getCounterId());
                            String counterName = pci.getGroupInfo().getKey() + "."
                                    + pci.getNameInfo().getKey() + "."
                                    + pci.getRollupType();
                            String instanceName = "/" + csv.getId().getInstance() + "/";

                            if (counterName.equals("datastore.datastoreIops.AVERAGE") && entity.getUrl().contains(instanceName)) {
                                entity.getDatastoreIops().add(Long.parseLong(csv.getValue()));
                            }
                        }
                    }
                }
            }

            Map<String, String> countersVM = this.metrics.getDSCountersVM();
            countersVM.entrySet().removeIf(e -> !this.countersIdMap.containsKey(e.getKey()));

            if (!countersVM.isEmpty() && !entity.getMappedVMs().isEmpty()) {
                List<ManagedObjectReference> entities = new ArrayList<>(entity.getMappedVMs());
                List<PerfEntityMetricBase> retrievedStats = Utils.getRealTimeStats(this.methods,
                        this.perfMgr,
                        this.countersIdMap,
                        countersVM,
                        "csv",
                        entities);

                for (PerfEntityMetricBase singleEntityPerfStats : retrievedStats) {
                    PerfEntityMetricCSV entityStatsCsv = (PerfEntityMetricCSV) singleEntityPerfStats;
                    List<PerfMetricSeriesCSV> metricsValues = entityStatsCsv.getValue();

                    if (!metricsValues.isEmpty()) {
                        for (PerfMetricSeriesCSV csv : metricsValues) {
                            PerfCounterInfo pci = this.countersInfoMap.get(csv.getId().getCounterId());
                            String counterName = pci.getGroupInfo().getKey() + "."
                                    + pci.getNameInfo().getKey() + "."
                                    + pci.getRollupType();
                            String instanceName = "/" + csv.getId().getInstance() + "/";

                            if (counterName.equals("datastore.totalReadLatency.AVERAGE") && entity.getUrl().contains(instanceName)) {
                                entity.getTotalReadLatencyMS().add(Long.parseLong(csv.getValue()));
                            }
                            if (counterName.equals("datastore.totalWriteLatency.AVERAGE") && entity.getUrl().contains(instanceName)) {
                                entity.getTotalWriteLatencyMS().add(Long.parseLong(csv.getValue()));
                            }
                            if (counterName.equals("datastore.numberReadAveraged.AVERAGE") &&
                                    entity.getUrl().contains(instanceName) &&
                                    Long.parseLong(csv.getValue()) >= 0) {
                                entity.getNumberRead().add(Long.parseLong(csv.getValue()));
                            }
                            if (counterName.equals("datastore.numberWriteAveraged.AVERAGE") &&
                                    entity.getUrl().contains(instanceName) &&
                                    Long.parseLong(csv.getValue()) >= 0) {
                                entity.getNumberWrite().add(Long.parseLong(csv.getValue()));
                            }
                            if (counterName.equals("datastore.read.AVERAGE") &&
                                    entity.getUrl().contains(instanceName) &&
                                    Long.parseLong(csv.getValue()) >= 0) {
                                entity.getReadRateBps().add(Long.parseLong(csv.getValue()) * 1024);
                            }
                            if (counterName.equals("datastore.write.AVERAGE") &&
                                    entity.getUrl().contains(instanceName) &&
                                    Long.parseLong(csv.getValue()) >= 0) {
                                entity.getWriteRateBps().add(Long.parseLong(csv.getValue()) * 1024);
                            }
                        }
                    }
                }
            }
        }

        datastoreEntities.forEach(DatastoreEntity::fillMeasureData);
    }

    private void measureHostSystems() throws RuntimeFaultFaultMsg {
        List<HostSystemEntity> hostSystemEntities = EntitiesStorage.getHostSystemEntities();

        Map<String, String> counters = this.metrics.getHSCounters();
        counters.entrySet().removeIf(e -> !this.countersIdMap.containsKey(e.getKey()));

        if (!counters.isEmpty() && !hostSystemEntities.isEmpty()) {
            List<ManagedObjectReference> entities = new ArrayList<>();
            hostSystemEntities.forEach(e -> entities.add(e.getObjectReference()));
            List<PerfEntityMetricBase> retrievedStats = Utils.getRealTimeStats(this.methods,
                    this.perfMgr,
                    this.countersIdMap,
                    counters,
                    "csv",
                    entities);

            for (PerfEntityMetricBase singleEntityPerfStats : retrievedStats) {
                PerfEntityMetricCSV entityStatsCsv = (PerfEntityMetricCSV) singleEntityPerfStats;
                List<PerfMetricSeriesCSV> metricsValues = entityStatsCsv.getValue();

                if (!metricsValues.isEmpty()) {
                    String entityId = entityStatsCsv.getEntity().getValue();
                    HostSystemEntity entity = null;
                    for (HostSystemEntity e : hostSystemEntities) {
                        if (e.getId().equals(entityId)) entity = e;
                    }

                    if (entity != null) {
                        for (PerfMetricSeriesCSV csv : metricsValues) {
                            PerfCounterInfo pci = this.countersInfoMap.get(csv.getId().getCounterId());
                            String counterName = pci.getGroupInfo().getKey() + "."
                                    + pci.getNameInfo().getKey() + "."
                                    + pci.getRollupType();

                            if (counterName.equals("cpu.usage.AVERAGE")) {
                                entity.setCpuUsagePercent(Double.parseDouble(csv.getValue()) / 100);
                            }
                            if (counterName.equals("mem.usage.AVERAGE")) {
                                entity.setMemoryUsagePercent(Double.parseDouble(csv.getValue()) / 100);
                            }
                            if (counterName.equals("mem.consumed.AVERAGE")) {
                                entity.setMemoryUsageAbsoluteMB(Double.parseDouble(csv.getValue()) / 1024);
                            }
                            if (counterName.equals("net.received.AVERAGE")) {
                                entity.setNetworkReceivedKbps(Long.parseLong(csv.getValue()));
                            }
                            if (counterName.equals("net.transmitted.AVERAGE")) {
                                entity.setNetworkTransmittedKbps(Long.parseLong(csv.getValue()));
                            }
                        }
                    }
                }
            }
        }
    }

    private void measureVirtualMachines() throws RuntimeFaultFaultMsg {
        List<VirtualMachineEntity> virtualMachineEntities = EntitiesStorage.getVirtualMachineEntities();
        List<VirtualDiskEntity> virtualDiskEntities = EntitiesStorage.getVirtualDiskEntities();
        List<ResourcePoolEntity> resourcePoolEntities = EntitiesStorage.getResourcePoolEntities();

        Map<String, String> counters = this.metrics.getVMCounters();
        counters.entrySet().removeIf(e -> !this.countersIdMap.containsKey(e.getKey()));

        if (!counters.isEmpty() && !virtualMachineEntities.isEmpty()) {
            List<ManagedObjectReference> entities = new ArrayList<>();
            virtualMachineEntities.forEach(e -> entities.add(e.getObjectReference()));
            List<PerfEntityMetricBase> retrievedStats = Utils.getRealTimeStats(this.methods,
                    this.perfMgr,
                    this.countersIdMap,
                    counters,
                    "csv",
                    entities);

            for (PerfEntityMetricBase singleEntityPerfStats : retrievedStats) {
                PerfEntityMetricCSV entityStatsCsv = (PerfEntityMetricCSV) singleEntityPerfStats;
                List<PerfMetricSeriesCSV> metricsValues = entityStatsCsv.getValue();

                if (!metricsValues.isEmpty()) {
                    String entityId = entityStatsCsv.getEntity().getValue();
                    VirtualMachineEntity entity = null;
                    for (VirtualMachineEntity e : virtualMachineEntities) {
                        if (e.getId().equals(entityId)) entity = e;
                    }

                    if (entity != null) {
                        List<VirtualDiskEntity> virtualDisks = entity.getMappedVirtualDisks();
                        for (PerfMetricSeriesCSV csv : metricsValues) {
                            PerfCounterInfo pci = this.countersInfoMap.get(csv.getId().getCounterId());
                            String counterName = pci.getGroupInfo().getKey() + "."
                                    + pci.getNameInfo().getKey() + "."
                                    + pci.getRollupType();
                            String instanceName = csv.getId().getInstance();

                            if (counterName.equals("cpu.usage.AVERAGE")) {
                                entity.setCpuUsagePercent(Double.parseDouble(csv.getValue()) / 100);
                            }
                            if (counterName.equals("cpu.ready.SUMMATION")) {
                                entity.setCpuReadyPercent((Double.parseDouble(csv.getValue()) / (20 * 1000)) * 100);
                            }
                            if (counterName.equals("mem.usage.AVERAGE")) {
                                entity.setMemoryUsagePercent(Double.parseDouble(csv.getValue()) / 100);
                            }
                            if (counterName.equals("mem.active.AVERAGE")) {
                                entity.setMemoryUsageAbsoluteMB(Double.parseDouble(csv.getValue()) / 1024);
                            }
                            if (counterName.equals("mem.consumed.AVERAGE")) {
                                entity.setMemoryConsumedMB(Double.parseDouble(csv.getValue()) / 1024);
                            }
                            if (counterName.equals("mem.overhead.AVERAGE")) {
                                entity.setMemoryOverheadMB(Double.parseDouble(csv.getValue()) / 1024);
                            }
                            if (counterName.equals("net.received.AVERAGE")) {
                                entity.setNetworkReceivedKbps(Long.parseLong(csv.getValue()));
                            }
                            if (counterName.equals("net.transmitted.AVERAGE")) {
                                entity.setNetworkTransmittedKbps(Long.parseLong(csv.getValue()));
                            }
                            if (counterName.equals("virtualDisk.totalReadLatency.AVERAGE")) {
                                virtualDisks.forEach(e -> {
                                    if (instanceName.equals(e.getInstance()))
                                        e.setTotalReadLatencyMS(Long.parseLong(csv.getValue()));
                                });
                            }
                            if (counterName.equals("virtualDisk.totalWriteLatency.AVERAGE")) {
                                virtualDisks.forEach(e -> {
                                    if (instanceName.equals(e.getInstance()))
                                        e.setTotalWriteLatencyMS(Long.parseLong(csv.getValue()));
                                });
                            }
                            if (counterName.equals("virtualDisk.numberReadAveraged.AVERAGE")) {
                                virtualDisks.forEach(e -> {
                                    if (instanceName.equals(e.getInstance()) && Long.parseLong(csv.getValue()) >= 0) {
                                        e.setNumberReadAveraged(Long.parseLong(csv.getValue()));
                                    }
                                });
                            }
                            if (counterName.equals("virtualDisk.numberWriteAveraged.AVERAGE")) {
                                virtualDisks.forEach(e -> {
                                    if (instanceName.equals(e.getInstance()) && Long.parseLong(csv.getValue()) >= 0) {
                                        e.setNumberWriteAveraged(Long.parseLong(csv.getValue()));
                                    }
                                });
                            }
                            if (counterName.equals("virtualDisk.read.AVERAGE")) {
                                virtualDisks.forEach(e -> {
                                    if (instanceName.equals(e.getInstance()) && Long.parseLong(csv.getValue()) >= 0) {
                                        e.setReadRateBps(Long.parseLong(csv.getValue()) * 1024);
                                    }
                                });
                            }
                            if (counterName.equals("virtualDisk.write.AVERAGE")) {
                                virtualDisks.forEach(e -> {
                                    if (instanceName.equals(e.getInstance()) && Long.parseLong(csv.getValue()) >= 0) {
                                        e.setWriteRateBps(Long.parseLong(csv.getValue()) * 1024);
                                    }
                                });
                            }
                        }
                    }
                }
            }
        }

        virtualMachineEntities.forEach(VirtualMachineEntity::fillMeasureData);
        virtualDiskEntities.forEach(VirtualDiskEntity::fillMeasureData);

        virtualMachineEntities.forEach(vm -> {
            AbstractEntity resourcePool = Utils.getEntityById(vm.getResourcePoolId(), "", resourcePoolEntities);
            if (resourcePool != null) vm.setResourcePoolName(resourcePool.getName());
        });
    }

    private void measureResourcePools() {
        List<ResourcePoolEntity> resourcePoolEntities = EntitiesStorage.getResourcePoolEntities();
        List<VirtualMachineEntity> virtualMachineEntities = EntitiesStorage.getVirtualMachineEntities();
        List<AbstractEntity> computeResourceEntities = new ArrayList<>();
        computeResourceEntities.addAll(EntitiesStorage.getComputeResourceEntities());
        computeResourceEntities.addAll(EntitiesStorage.getClusterComputeResourceEntities());

        for (ResourcePoolEntity resourcePool : resourcePoolEntities) {
            String resourcePoolId = resourcePool.getObjectReference().getValue();

            virtualMachineEntities.forEach(e -> {
                if (e.getResourcePool() != null) {
                    if (resourcePoolId.equals(e.getResourcePool().getValue())) resourcePool.getMappedVMs().add(e);
                }
            });

            resourcePoolEntities.forEach(e -> {
                if (e.getParentResourcePool() != null) {
                    if (resourcePoolId.equals(e.getParentResourcePool().getValue()))
                        resourcePool.getChildResourcePools().add(e);
                }
            });

            List<Double> cpuReadyPercent = new ArrayList<>();
            resourcePool.getMappedVMs().forEach(e -> cpuReadyPercent.add(e.getCpuReadyPercent()));
            if (!cpuReadyPercent.isEmpty()) {
                resourcePool.setCpuReadyMaxPercent(cpuReadyPercent.stream().reduce(Double::max).get());
                resourcePool.setCpuReadyAvgPercent(cpuReadyPercent.stream().collect(Collectors.averagingDouble((e) -> e)));
            }
        }

        resourcePoolEntities.forEach(ResourcePoolEntity::fillMeasureData);

        resourcePoolEntities.forEach(rp -> {
            AbstractEntity parentResourcePool = Utils.getEntityById(rp.getParentResourcePoolId(), "", resourcePoolEntities);
            AbstractEntity computeResource = Utils.getEntityById(rp.getComputeResourceId(), "", computeResourceEntities);
            if (parentResourcePool != null) rp.setParentResourcePoolName(parentResourcePool.getName());
            if (computeResource != null) rp.setComputeResourceName(computeResource.getName());
            /*1111*/

            if (computeResource != null) rp.setText("Hello text");


        });
    }

    private void measureComputeResources() {
        List<ComputeResourceEntity> computeResourceEntities = EntitiesStorage.getComputeResourceEntities();
        List<ResourcePoolEntity> resourcePoolEntities = EntitiesStorage.getResourcePoolEntities();

        for (ComputeResourceEntity computeResource : computeResourceEntities) {
            String rootResourcePoolId = computeResource.getRootResourcePool().getValue();

            resourcePoolEntities.forEach(e -> {
                if (e.getId().equals(rootResourcePoolId)) {
                    computeResource.setNumVM(e.getNumVM());
                    computeResource.setOverallCpuUsageMHz(e.getOverallCpuUsageMHz());
                    computeResource.setGuestMemoryUsageMB(e.getGuestMemoryUsageMB());
                    computeResource.setCpuReadyMaxPercent(e.getCpuReadyMaxPercent());
                    computeResource.setCpuReadyAvgPercent(e.getCpuReadyAvgPercent());
                }
            });
        }

        computeResourceEntities.forEach(ComputeResourceEntity::fillMeasureData);
    }

    private void measureClusterComputeResources() {
        List<ClusterComputeResourceEntity> clusterComputeResourceEntities = EntitiesStorage.getClusterComputeResourceEntities();
        List<ResourcePoolEntity> resourcePoolEntities = EntitiesStorage.getResourcePoolEntities();

/*
        Map<String, String> counters = this.metrics.getCCRCounters();
        counters.entrySet().removeIf(e -> !this.countersIdMap.containsKey(e.getKey()));
        List<ManagedObjectReference> entities = clusterComputeResourceEntities.stream().map(AbstractEntity::getObjectReference).collect(Collectors.toList());
        try {
            getMetrics(entities, counters, (s, value) -> {
                LOGGER.info("`hE3BY2mG`"+ value.get().toString());
            });
        } catch (RuntimeFaultFaultMsg e) {
            throw new RuntimeException(e);
        }
*/


        for (ClusterComputeResourceEntity clusterComputeResource : clusterComputeResourceEntities) {
            String rootResourcePoolId = clusterComputeResource.getRootResourcePool().getValue();

            resourcePoolEntities.forEach(e -> {
                if (e.getId().equals(rootResourcePoolId)) {
                    clusterComputeResource.setNumVM(e.getNumVM());
                    clusterComputeResource.setOverallCpuUsageMHz(e.getOverallCpuUsageMHz());
                    clusterComputeResource.setGuestMemoryUsageMB(e.getGuestMemoryUsageMB());
                    clusterComputeResource.setCpuReadyMaxPercent(e.getCpuReadyMaxPercent());
                    clusterComputeResource.setCpuReadyAvgPercent(e.getCpuReadyAvgPercent());
                }
            });
        }

        clusterComputeResourceEntities.forEach(ClusterComputeResourceEntity::fillMeasureData);
    }
}
