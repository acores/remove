package com.me.vmware;

import com.vmware.vim25.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import javax.xml.ws.BindingProvider;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class ServerConnection {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServerConnection.class);
    private final String serverIp;
    private final String userName;
    private final String password;
    private final String url;
    private final AtomicBoolean connectionUp = new AtomicBoolean(false);
    // Variables of the following types for access to the API methods
    // and to the vSphere inventory.
    // -- ManagedObjectReference for the ServiceInstance on the Server
    // -- VimService for access to the vSphere Web service
    // -- VimPortType for access to methods
    // -- ServiceContent for access to managed object services
    private final ManagedObjectReference serviceInstance;
    private final VimService webService;
    private final VimPortType methods;
    private ServiceContent serviceContent;

    public ServerConnection(String serverIp, String userName, String password) {
        this.serverIp = serverIp;
        this.userName = userName;
        this.password = password;
        this.url = "https://" + serverIp + "/sdk";
        System.out.println(this.url);
        // Set up the manufactured managed object reference for the ServiceInstance
        this.serviceInstance = new ManagedObjectReference();

        this.serviceInstance.setType("ServiceInstance");
        this.serviceInstance.setValue("ServiceInstance");
        // Create a VimService object to obtain a VimPort binding provider.
        // The BindingProvider provides access to the protocol fields
        // in request/response messages. Retrieve the request context
        // which will be used for processing message requests.
        this.webService = new VimService();
        this.methods = this.webService.getVimPort();
        Map<String, Object> context = ((BindingProvider) this.methods).getRequestContext();
        // Store the Server URL in the request context and specify true
        // to maintain the connection between the client and server.
        // The client API will include the Server's HTTP cookie in its
        // requests to maintain the session. If you do not set this to true,
        // the Server will start a new session with each request.
        context.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, this.url);
        context.put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
    }

    public void connect() throws Exception {
        if (!this.connectionUp.get()) {
            ServerConnection.trustAllX509Certificates();
            // Retrieve the ServiceContent object and login
            this.serviceContent = this.methods.retrieveServiceContent(this.serviceInstance);
            this.methods.login(this.serviceContent.getSessionManager(), this.userName, this.password,null);
            this.connectionUp.set(true);
        }
    }

    public void disconnect() {
        if (this.connectionUp.get()) {
            try {
                this.connectionUp.set(false);
                // close the connection
                this.methods.logout(this.serviceContent.getSessionManager());
                LOGGER.info("Disconnected from vCenter/ESX server successfully");
            } catch (Exception e) {
                LOGGER.warn("Can't disconnect from vCenter/ESX server. " + e.getMessage());
            }
        }
    }

    public VimPortType getMethods() {
        return methods;
    }

    public ServiceContent getServiceContent() {
        return serviceContent;
    }

    public boolean getConnectionUp() {
        return connectionUp.get();
    }

    private static void trustAllX509Certificates() throws NoSuchAlgorithmException, KeyManagementException {
        // Authentication is handled by using a TrustManager and supplying
        // a host name verifier method.

        // Declare a host name verifier that will automatically enable
        // the connection. The host name verifier is invoked during
        // the SSL handshake.
        HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String urlHostName, SSLSession session) { return true; }
        };
        // Create the trust manager.
        TrustManager[] trustAllCerts = new TrustManager[1];
        TrustManager tm = new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {}

            @Override
            public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {}

            @Override
            public X509Certificate[] getAcceptedIssuers() { return null; }
        };
        trustAllCerts[0] = tm;
        // Create the SSL context
        SSLContext sc = SSLContext.getInstance("SSL");
        // Create the session context
        SSLSessionContext sslsc = sc.getServerSessionContext();
        // Initialize the contexts; the session context takes the trust manager.
        sslsc.setSessionTimeout(0);
        sc.init(null, trustAllCerts, null);
        // Use the default socket factory to create the socket for the secure connection
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        // Set the default host name verifier to enable the connection.
        HttpsURLConnection.setDefaultHostnameVerifier(hv);
    }
}
