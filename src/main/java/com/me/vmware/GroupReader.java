package com.me.vmware;

import com.me.vmware.entities.AbstractEntity;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.util.*;
import java.util.stream.Collectors;

public class GroupReader {

    public static LinkedHashMap<String, String> extractProperties(ObjectName objectName) {
        String str = objectName.toString().replaceFirst(objectName.getDomain() + ":", "");

        LinkedHashMap<String, String> collect = new LinkedHashMap<>();

        Arrays.stream(str.split(",")).map(s -> s.split("=")).forEach(s -> collect.put(s[0], s[1]));

        return collect;
    }

    public static List<String> extractPathFromGroup(AbstractEntity entity, String domain) throws MalformedObjectNameException {

        String flat = toFlat(entity.getGroup());

        ObjectName name = new ObjectName(domain + ":" + flat + ",name=" + entity.getName());

        return extractPropertiesToList(name);
    }

    public static List<String> extractPathFromGroup(String group, String domain, String name) throws MalformedObjectNameException {
        String flat = toFlat(group);

        ObjectName oname = new ObjectName(domain + ":" + flat + ",name=" + name);

        return extractPropertiesToList(oname);
    }

    public static List<String> extractPropertiesToList(ObjectName objectName) {
        String str = objectName.toString().replaceFirst(objectName.getDomain() + ":", "");

        List<String> collect = Arrays.stream(str.split(","))
                .map(s -> s.split("=")[1])
                .collect(Collectors.toList());
        collect.remove(collect.size()-1);
        return collect;
    }

    public static String toFlat(String group) {
        String[] split = group.split("\\|");

        for (int x = 0; x < split.length; x++) {
            String s = split[x];
            split[x] = s.hashCode() + "=" + s;
        }

        return String.join(",", split);
    }
}
