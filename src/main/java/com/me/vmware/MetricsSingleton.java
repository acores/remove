package com.me.vmware;

import com.vmware.vim25.PropertySpec;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class MetricsSingleton {

    private static MetricsSingleton INSTANCE;
    private List<String> entitiesList;
    private PropertySpec pSpecDC;
    private PropertySpec pSpecDS;
    private PropertySpec pSpecHS;
    private PropertySpec pSpecVM;
    private PropertySpec pSpecRP;
    private PropertySpec pSpecCR;
    private PropertySpec pSpecCCR;
    private PropertySpec pSpecVA;
    private Map<String, String> DSCountersHV;
    private Map<String, String> DSCountersVM;
    private Map<String, String> HSCounters;
    private Map<String, String> VMCounters;
    private Map<String, String> CCRCounters;

    private MetricsSingleton() {
        this.fillEntitiesList();
        this.fillPSpecDC();
        this.fillPSpecDS();
        this.fillPSpecHS();
        this.fillPSpecVM();
        this.fillPSpecRP();
        this.fillPSpecCR();
        this.fillPSpecCCR();
        this.fillPSpecVA();
        this.fillDSCountersHV();
        this.fillDSCountersVM();
        this.fillHSCounters();
        this.fillVMCounters();
        this.fillCCRCounters();

    }

    public static MetricsSingleton getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new MetricsSingleton();
        }

        return INSTANCE;
    }

    private void fillEntitiesList() {
        this.entitiesList = Arrays.asList("Datacenter",
                "Datastore",
                "HostSystem",
                "VirtualMachine",
                "ResourcePool",
                "ComputeResource",
                "ClusterComputeResource",
                "VirtualApp");
    }

    private void fillPSpecDC() {
        this.pSpecDC = new PropertySpec();
        this.pSpecDC.setType("Datacenter");
        this.pSpecDC.getPathSet().add("name");
        this.pSpecDC.getPathSet().add("overallStatus");
    }

    private void fillPSpecDS() {
        this.pSpecDS = new PropertySpec();
        this.pSpecDS.setType("Datastore");
        this.pSpecDS.getPathSet().add("name");
        this.pSpecDS.getPathSet().add("host");
        this.pSpecDS.getPathSet().add("vm");
        this.pSpecDS.getPathSet().add("summary.url");
        this.pSpecDS.getPathSet().add("overallStatus");
        this.pSpecDS.getPathSet().add("summary.accessible");
        this.pSpecDS.getPathSet().add("summary.capacity");
        this.pSpecDS.getPathSet().add("summary.freeSpace");
    }

    private void fillPSpecHS() {
        this.pSpecHS = new PropertySpec();
        this.pSpecHS.setType("HostSystem");
        this.pSpecHS.getPathSet().add("name");
        this.pSpecHS.getPathSet().add("overallStatus");
        this.pSpecHS.getPathSet().add("runtime.connectionState");
        this.pSpecHS.getPathSet().add("runtime.powerState");
        this.pSpecHS.getPathSet().add("summary.hardware.numCpuCores");
        this.pSpecHS.getPathSet().add("summary.hardware.memorySize");
        this.pSpecHS.getPathSet().add("runtime.healthSystemRuntime.systemHealthInfo.numericSensorInfo");
    }

    private void fillPSpecVM() {
        this.pSpecVM = new PropertySpec();
        this.pSpecVM.setType("VirtualMachine");
        this.pSpecVM.getPathSet().add("name");
        this.pSpecVM.getPathSet().add("resourcePool");
        this.pSpecVM.getPathSet().add("overallStatus");
        this.pSpecVM.getPathSet().add("runtime.connectionState");
        this.pSpecVM.getPathSet().add("runtime.powerState");
        this.pSpecVM.getPathSet().add("summary.config.numCpu");
        this.pSpecVM.getPathSet().add("summary.config.memorySizeMB");
        this.pSpecVM.getPathSet().add("summary.quickStats.balloonedMemory");
        this.pSpecVM.getPathSet().add("summary.quickStats.swappedMemory");
        this.pSpecVM.getPathSet().add("config.hardware.device");
    }

    private void fillPSpecRP() {
        this.pSpecRP = new PropertySpec();
        this.pSpecRP.setType("ResourcePool");
        this.pSpecRP.getPathSet().add("name");
        this.pSpecRP.getPathSet().add("parent");
        this.pSpecRP.getPathSet().add("owner");
        this.pSpecRP.getPathSet().add("overallStatus");
        this.pSpecRP.getPathSet().add("vm");
        this.pSpecRP.getPathSet().add("config.cpuAllocation.limit");
        this.pSpecRP.getPathSet().add("config.cpuAllocation.reservation");
        this.pSpecRP.getPathSet().add("config.memoryAllocation.limit");
        this.pSpecRP.getPathSet().add("config.memoryAllocation.reservation");
        this.pSpecRP.getPathSet().add("summary.quickStats.overallCpuUsage");
        this.pSpecRP.getPathSet().add("summary.quickStats.guestMemoryUsage");
    }

    private void fillPSpecCR() {
        this.pSpecCR = new PropertySpec();
        this.pSpecCR.setType("ComputeResource");
        this.pSpecCR.getPathSet().add("name");
        this.pSpecCR.getPathSet().add("resourcePool");
        this.pSpecCR.getPathSet().add("overallStatus");
        this.pSpecCR.getPathSet().add("summary.numCpuCores");
        this.pSpecCR.getPathSet().add("summary.numEffectiveHosts");
        this.pSpecCR.getPathSet().add("summary.numHosts");
        this.pSpecCR.getPathSet().add("summary.effectiveCpu");
        this.pSpecCR.getPathSet().add("summary.totalCpu");
        this.pSpecCR.getPathSet().add("summary.effectiveMemory");
        this.pSpecCR.getPathSet().add("summary.totalMemory");
    }

    private void fillPSpecCCR() {
        this.pSpecCCR = new PropertySpec();
        this.pSpecCCR.setType("ClusterComputeResource");
        this.pSpecCCR.getPathSet().add("name");
        this.pSpecCCR.getPathSet().add("resourcePool");
        this.pSpecCCR.getPathSet().add("overallStatus");
        this.pSpecCCR.getPathSet().add("summary.numCpuCores");
        this.pSpecCCR.getPathSet().add("summary.numEffectiveHosts");
        this.pSpecCCR.getPathSet().add("summary.numHosts");
        this.pSpecCCR.getPathSet().add("summary.effectiveCpu");
        this.pSpecCCR.getPathSet().add("summary.totalCpu");
        this.pSpecCCR.getPathSet().add("summary.effectiveMemory");
        this.pSpecCCR.getPathSet().add("summary.totalMemory");
    }

    private void fillPSpecVA() {
        this.pSpecVA = new PropertySpec();
        this.pSpecVA.setType("VirtualApp");
        this.pSpecVA.getPathSet().add("name");
        this.pSpecVA.getPathSet().add("parent");
    }

    private void fillDSCountersHV() {
        this.DSCountersHV = new HashMap<>();
        this.DSCountersHV.put("datastore.datastoreIops.AVERAGE", "*");
    }

    private void fillDSCountersVM() {
        this.DSCountersVM = new HashMap<>();
        this.DSCountersVM.put("datastore.totalReadLatency.AVERAGE", "*");
        this.DSCountersVM.put("datastore.totalWriteLatency.AVERAGE", "*");
        this.DSCountersVM.put("datastore.numberReadAveraged.AVERAGE", "*");
        this.DSCountersVM.put("datastore.numberWriteAveraged.AVERAGE", "*");
        this.DSCountersVM.put("datastore.read.AVERAGE", "*");
        this.DSCountersVM.put("datastore.write.AVERAGE", "*");
    }

    private void fillHSCounters() {
        this.HSCounters = new HashMap<>();
        this.HSCounters.put("cpu.usage.AVERAGE", "");
        this.HSCounters.put("mem.usage.AVERAGE", "");
        this.HSCounters.put("mem.consumed.AVERAGE", "");
        this.HSCounters.put("net.received.AVERAGE", "");
        this.HSCounters.put("net.transmitted.AVERAGE", "");
    }

    private void fillCCRCounters() {
        this.CCRCounters = new HashMap<>();
        this.CCRCounters.put("cpu.usage.AVERAGE", "");
        this.CCRCounters.put("mem.usage.AVERAGE", "");
    }

    private void fillVMCounters() {
        this.VMCounters = new HashMap<>();
        this.VMCounters.put("cpu.usage.AVERAGE", "");
        this.VMCounters.put("cpu.ready.SUMMATION", "");
        this.VMCounters.put("mem.usage.AVERAGE", "");
        this.VMCounters.put("mem.active.AVERAGE", "");
        this.VMCounters.put("mem.consumed.AVERAGE", "");
        this.VMCounters.put("mem.overhead.AVERAGE", "");
        this.VMCounters.put("net.received.AVERAGE", "");
        this.VMCounters.put("net.transmitted.AVERAGE", "");
        this.VMCounters.put("virtualDisk.totalReadLatency.AVERAGE", "*");
        this.VMCounters.put("virtualDisk.totalWriteLatency.AVERAGE", "*");
        this.VMCounters.put("virtualDisk.numberReadAveraged.AVERAGE", "*");
        this.VMCounters.put("virtualDisk.numberWriteAveraged.AVERAGE", "*");
        this.VMCounters.put("virtualDisk.read.AVERAGE", "*");
        this.VMCounters.put("virtualDisk.write.AVERAGE", "*");
    }

    public List<String> getEntitiesList() { return entitiesList; }
    public PropertySpec getPSpecDC() { return pSpecDC; }
    public PropertySpec getPSpecDS() { return pSpecDS; }
    public PropertySpec getPSpecHS() { return pSpecHS; }
    public PropertySpec getPSpecVM() { return pSpecVM; }
    public PropertySpec getPSpecRP() { return pSpecRP; }
    public PropertySpec getPSpecCR() { return pSpecCR; }
    public PropertySpec getPSpecCCR() { return pSpecCCR; }
    public PropertySpec getPSpecVA() { return pSpecVA; }
    public Map<String, String> getDSCountersHV() { return DSCountersHV; }
    public Map<String, String> getDSCountersVM() { return DSCountersVM; }
    public Map<String, String> getHSCounters() { return HSCounters; }
    public Map<String, String> getVMCounters() { return VMCounters; }

    public Map<String, String> getCCRCounters() {
        return CCRCounters;
    }
}
