package com.me.vmware.config.reader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.me.vmware.VMWareJMX;
import com.me.vmware.config.ConnectionsConfig;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.nio.file.Path;
import java.util.UUID;

import static com.me.vmware.AppConfig.APP_SETTINGS_PATH;
import static com.me.vmware.VMWareJMX.getAppConfig;

public class JsonConfigReader {
    public JsonConfigReader() {
    }

    public @Nullable ConnectionsConfig fromString(@NotNull String source) {
        return getAppConfig().getGson().fromJson(source, ConnectionsConfig.class);
    }

    public @Nullable ConnectionsConfig fromStream(@NotNull InputStream inputStream) {
        return getAppConfig().getGson().fromJson(new InputStreamReader(inputStream), ConnectionsConfig.class);
    }

    public @Nullable String toJsonString(@NotNull ConnectionsConfig config) {
        return getAppConfig().getGson().toJson(config);
    }

    public @NotNull String toPrettyJsonString(@NotNull ConnectionsConfig config) {
        return getAppConfig().getPrettyGson().toJson(config);
    }

    public static boolean saveConfig(File path, ConnectionsConfig connectionsConfig) {
        try (FileWriter writer = new FileWriter(path)) {
            String jsonString = new JsonConfigReader().toPrettyJsonString(connectionsConfig);
            writer.write(jsonString);

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static boolean saveJson(File path, Object o, boolean pretty) {
        try (FileWriter writer = new FileWriter(path)) {
            String jsonString = pretty ? getAppConfig().getPrettyGson().toJson(o) : getAppConfig().getGson().toJson(o);
            writer.write(jsonString);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean saveString(File path, String content) throws IOException {
        try (FileWriter writer = new FileWriter(path)) {
            String s = content.replace("\n", System.lineSeparator());
            writer.write(s);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static File randomJsonFile(String prefix) {
        String rndName = prefix + UUID.randomUUID().toString().substring(0, 6) + ".json";
        return APP_SETTINGS_PATH.resolve(rndName).toFile();
    }

    public static File randomFile(String prefix, String extension, String... path) {
        String rndName = prefix + UUID.randomUUID().toString().substring(0, 6) + extension;
        Path resolved = APP_SETTINGS_PATH;
        for (String s : path) {
            resolved = resolved.resolve(s);
        }

        resolved.toFile().mkdirs();

        return new File(resolved.toFile(), rndName);
    }
}
