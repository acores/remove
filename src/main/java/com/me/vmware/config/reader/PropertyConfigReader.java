package com.me.vmware.config.reader;

import com.me.vmware.config.ConnectionsConfig;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.me.vmware.config.ConnectionsConfig.ConnectionInfo;

public class PropertyConfigReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyConfigReader.class);

    @Nullable
    public ConnectionsConfig readFromStream(@NotNull InputStream stream) {
        Properties properties = loadProperties(stream);
        if (properties == null)
            return null;

        ConnectionsConfig connectionsConfig = new ConnectionsConfig(new ArrayList<>(), new ArrayList<>());

        if (!readReconnectProperties(properties, connectionsConfig)) {
            return null;
        }
        if (!readActiveConnections(properties, connectionsConfig)) {
            return null;
        }

        return connectionsConfig;
    }

    @Nullable
    private Properties loadProperties(InputStream stream) {
        try (InputStream input = stream) {
            Properties props = new Properties();
            props.load(input);
            return props;
        } catch (IOException e) {
            LOGGER.error("Can't load properties. " + e.getMessage());
        }

        return null;
    }

    @SuppressWarnings("ConstantConditions")
    private static boolean readReconnectProperties(Properties props, ConnectionsConfig ConnectionsConfig) {

        LOGGER.info("Read reconnect properties...");

        JavaPropertyReader clearIntervalSec = new JavaPropertyReader("clearIntervalSec");
        JavaPropertyReader updateIntervalSec = new JavaPropertyReader("updateIntervalSec");
        JavaPropertyReader updateBeforeExitIntervalSec = new JavaPropertyReader("updateBeforeExitIntervalSec");
        JavaPropertyReader numberRetries = new JavaPropertyReader("numberRetries");

        AtomicBoolean corrupted = new AtomicBoolean(false);


        Stream.of(clearIntervalSec, updateIntervalSec, updateBeforeExitIntervalSec, numberRetries).forEach(x -> {
            x.read(props);

            if (!x.hasValue()) {
                LOGGER.error("Could not find property {} in config.properties.", x.getKey());
                corrupted.set(true);
            }
        });

        if (corrupted.get()) {
            LOGGER.error("Could not read connection manager properties. config.properties is corrupted.");
            return false;
        }

        try {
            ConnectionsConfig.clearIntervalSec = Integer.parseInt(clearIntervalSec.getValue());
            ConnectionsConfig.updateIntervalSec = Integer.parseInt(updateIntervalSec.getValue());
            ConnectionsConfig.updateBeforeExitIntervalSec = Integer.parseInt(updateBeforeExitIntervalSec.getValue());
            ConnectionsConfig.numberRetries = Integer.parseInt(numberRetries.getValue());
        } catch (NumberFormatException | DateTimeParseException e) {
            LOGGER.error("Error while parse connection settings", e);
            return false;
        }

        return true;
    }

    private static Optional<ConnectionInfo> readBlackServerConnection(String connectionName, Properties p) {
        LOGGER.info("Read connection: {}", connectionName);

        JavaPropertyReader serverIp = new JavaPropertyReader("serverIp");
        JavaPropertyReader userName = new JavaPropertyReader("userName");
        JavaPropertyReader password = new JavaPropertyReader("password");
        JavaPropertyReader domainName = new JavaPropertyReader("domainName");
        JavaPropertyReader onError = new JavaPropertyReader("onError");

        AtomicBoolean corrupted = new AtomicBoolean(false);

        Stream.of(serverIp, userName, password, domainName, onError).forEach(x -> {
            x.read(connectionName, p);

            if (!x.hasValue()) {
                LOGGER.error("Could not find property {} in config.properties.", x.getKey());
                corrupted.set(true);
            }
        });

        if (corrupted.get()) {
            return Optional.empty();
        }

        return Optional.of(new ConnectionInfo(
                connectionName,
                domainName.getValue(),
                serverIp.getValue(),
                userName.getValue(),
                password.getValue(),
                onError.getValue()
        ));
    }

    private static boolean readActiveConnections(Properties props, ConnectionsConfig ConnectionsConfig) {
        LOGGER.info("Read connections...");

        //Получаем соединения
        String connectionsString = props.getProperty("connections");
        if (connectionsString == null) {
            LOGGER.error("Could not find property 'connections' in config.properties.");
            return false;
        }

        List<String> connections = Arrays.stream(connectionsString.split(","))
                .map(String::trim)
                .collect(Collectors.toList());

        if (connections.isEmpty()) {
            LOGGER.error("Connections is empty config.properties: {}", connections);
            return false;
        }

        LOGGER.info("Founded connections: {}", connections);

        for (String connection : connections) {
            Optional<ConnectionInfo> blackServerConnection = readBlackServerConnection(connection, props);
            if (!blackServerConnection.isPresent()) {
                LOGGER.error("Could not read connection {} properties. config.properties is corrupted.", connection);
                return false;
            }

            ConnectionsConfig.connections.add(blackServerConnection.get());
            ConnectionsConfig.activeConnections = connections;
        }
        return true;
    }

    public static class JavaPropertyReader {
        String key;
        String value;
        public JavaPropertyReader(String key) {
            this.key = key;
        }
        public void read(String path, Properties p) {
            this.key = path + "." + key;
            this.value = p.getProperty(key);
        }

        public void read(Properties p) {
            this.value = p.getProperty(key);
        }

        public boolean hasValue() {
            return value != null;
        }

        @Nullable
        public String getValue() {
            return value;
        }

        public String getKey() {
            return key;
        }
    }
}
