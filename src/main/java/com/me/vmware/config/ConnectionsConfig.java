package com.me.vmware.config;

import java.util.ArrayList;
import java.util.List;

public class ConnectionsConfig {
    public int clearIntervalSec;
    public int updateIntervalSec;
    public int updateBeforeExitIntervalSec;
    public int numberRetries;

    public List<String> activeConnections;
    public List<ConnectionInfo> connections;

    public ConnectionsConfig(List<String> activeConnections, List<ConnectionInfo> connections) {
        this.activeConnections = activeConnections;
        this.connections = connections;
    }

    public boolean equalByIntervals(ConnectionsConfig config) {
        return this.clearIntervalSec == config.clearIntervalSec &&
                this.updateIntervalSec == config.updateIntervalSec &&
                this.updateBeforeExitIntervalSec == config.updateBeforeExitIntervalSec;
    }

    public static class ConnectionInfo {
        public ConnectionInfo(String connection, String domainName, String serverIp, String userName, String password, String onError) {
            this.connection = connection;
            this.domainName = domainName;
            this.serverIp = serverIp;
            this.userName = userName;
            this.password = password;
            this.onError = onError;
        }

        public String connection;
        public String domainName;
        public String serverIp;
        public String userName;
        public String password;
        public String onError;
    }
}
