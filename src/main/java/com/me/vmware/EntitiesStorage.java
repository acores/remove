package com.me.vmware;

import com.me.vmware.entities.*;

import java.util.ArrayList;
import java.util.List;

public class EntitiesStorage {
    private static List<DatacenterEntity> datacenterEntities = new ArrayList<>();
    private static List<ComputeResourceEntity> computeResourceEntities = new ArrayList<>();
    private static List<ClusterComputeResourceEntity> clusterComputeResourceEntities = new ArrayList<>();
    private static List<DatastoreEntity> datastoreEntities = new ArrayList<>();
    private static List<ResourcePoolEntity> resourcePoolEntities = new ArrayList<>();
    private static List<HostSystemEntity> hostSystemEntities = new ArrayList<>();
    private static List<HostSensorEntity> hostSensorEntities = new ArrayList<>();
    private static List<VirtualMachineEntity> virtualMachineEntities = new ArrayList<>();
    private static List<VirtualDiskEntity> virtualDiskEntities = new ArrayList<>();
    private static List<VirtualAppEntity> virtualAppEntities = new ArrayList<>();

    public static List<AbstractEntity> getAllEntities() {
        List<AbstractEntity> allEntities = new ArrayList<>();
        allEntities.addAll(datacenterEntities);
        allEntities.addAll(computeResourceEntities);
        allEntities.addAll(clusterComputeResourceEntities);
        allEntities.addAll(datastoreEntities);
        allEntities.addAll(resourcePoolEntities);
        allEntities.addAll(hostSystemEntities);
        allEntities.addAll(hostSensorEntities);
        allEntities.addAll(virtualMachineEntities);
        allEntities.addAll(virtualDiskEntities);
        allEntities.addAll(virtualAppEntities);

        return allEntities;
    }
    public static List<DatacenterEntity> getDatacenterEntities() {
        return datacenterEntities;
    }

    public static List<ComputeResourceEntity> getComputeResourceEntities() { return computeResourceEntities; }

    public static List<ClusterComputeResourceEntity> getClusterComputeResourceEntities() { return clusterComputeResourceEntities; }

    public static List<DatastoreEntity> getDatastoreEntities() {
        return datastoreEntities;
    }

    public static List<ResourcePoolEntity> getResourcePoolEntities() { return resourcePoolEntities; }

    public static List<HostSystemEntity> getHostSystemEntities() {
        return hostSystemEntities;
    }

    public static List<HostSensorEntity> getHostSensorEntities() { return hostSensorEntities; }

    public static List<VirtualMachineEntity> getVirtualMachineEntities() { return virtualMachineEntities; }

    public static List<VirtualDiskEntity> getVirtualDiskEntities() { return virtualDiskEntities; }

    public static List<VirtualAppEntity> getVirtualAppEntities() { return virtualAppEntities; }

    public static void clearStorage() {
        datacenterEntities.clear();
        computeResourceEntities.clear();
        clusterComputeResourceEntities.clear();
        datastoreEntities.clear();
        resourcePoolEntities.clear();
        hostSystemEntities.clear();
        hostSensorEntities.clear();
        virtualMachineEntities.clear();
        virtualDiskEntities.clear();
        virtualAppEntities.clear();
    }

}
