package com.me.vmware;

import com.me.vmware.config.reader.JsonConfigReader;

import java.io.File;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AppUtils {
    public static int generateId(Deque<Integer> ids) {
        ids.addLast(ids.size());

        return ids.getLast();
    }
    public static List<String> listDuplicates(List<String> list) {
        Set<String> elements = new HashSet<String>();
        return list.stream()
                .filter(n -> !elements.add(n))
                .collect(Collectors.toList());
    }
}
